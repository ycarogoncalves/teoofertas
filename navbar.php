<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <i class="fas fa-search"></i>
            </button>
            <a class="navbar-brand refresh" href="index.php"><i class="fas fa-sync"></i></a>
            <a class="navbar-brand img-icone" href="index.php"><img src='images/BLACKPRICE80x80.png'></a>
            
            
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="row">
                <div id="searchBar" class="col-md-10 col-sm-10 col-xs-12">
                    <form class="navbar-form" role="search" method="GET" id="search-form" name="search-form" action="index.php">
                        <div id="containerSearch" class="input-group">
                            <?php
                            if( !isset($_SESSION['pesquisa']) ){
                                $_SESSION['pesquisa'] = '';
                            }
                            ?>
                            <input autocomplete="off" type="text" class="form-control" placeholder="BUSCAR OFERTAS" list="produto" name="buscar" id="buscar"  value="<?php echo $_SESSION['pesquisa']; ?>">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                  
        </div>
    </div>
    <!-- /.container -->
</nav>
