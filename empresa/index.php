<?php

session_start();
$idUser = $_SESSION['idUser'];

include('../conf/confbd.php');

include('authSession.php');

include('head.php');
?>


<body>

	<?php include('navbar.php'); ?>

    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Minhas promoções ativas</h2>
            </div>

        <?php 

        	$datahoje = date('Y-m-d');

            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

		 	// capturar id do supermercado
         	$SQLSelect = "SELECT supermercado_idsupermercado FROM `supermercado_tem_usuario` where usuario_idusuario = '$idUser'";
            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                   
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $idsupermercado = $dadosEncontrados['supermercado_idsupermercado'];	
                 }
             }

            // instrução SQL básica 
            $SQLSelect = "SELECT idpublicaOferta,status,precoOferta,dataInicial,dataFinal,date_format(dataFinal,'%Y/%m/%d') as dataTermino,desc_promocao,banner_promocao,nomeSupermercado FROM `publicaOferta`,`supermercado` WHERE supermercado_idsupermercado=supermercado.idsupermercado and (status=1 or status=0) and idsupermercado = $idsupermercado and dataInicial <= '$datahoje' and dataFinal >= '$datahoje';";

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $barras = $dadosEncontrados['barras'];
                    $desc_promocao = $dadosEncontrados['desc_promocao'];
                    $banner_promocao = $dadosEncontrados['banner_promocao'];
                    $id = $dadosEncontrados['idpublicaOferta'];
                    $precoOferta = $dadosEncontrados['precoOferta'];
                    $dataFinal = $dadosEncontrados['dataTermino'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $status = $dadosEncontrados['status'];
                    $precoOferta = number_format($precoOferta,2,",",".");

                    
                    if($status==0){
                        $hidden="hiddenProduct";
                        $pendente="Aguardando aprovação";
                    }
                    else{
                        $hidden="";
                        $pendente="";
                    }

                    /*
					if (!getimagesize($imagemProduto)) {
					    $imagemProduto = 'images/notfound.png';
					} 
					*/
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6 $hidden' >
                            <img class='blackItem' src='../images/BLACKPRICE80x80.png' alt=''>
                            <i class='aguardandoAprovacao'>$pendente</i>
                            <a href='portfolio-item.php?id=$id'>
                                <img class='img-responsive img-portfolio img-hover' src='$banner_promocao' alt=''>
                                <span class='productDescription'>".substr($desc_promocao, 0,30)."</span>
                                <p class='productPrice'>R$ $precoOferta</p>
                                <p class='productSupermercado'>$nomeSupermercado</p>

                                <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>

                                <p>
                                    <div id='contador' class='contador_$id'></div>
                                    <script type='text/javascript'>
                                      $('.contador_$id').countdown('$dataFinal', function(event) {
                                        $(this).html(event.strftime('%D dias %H:%M:%S'));
                                      });
                                    </script>                    
                                </p>

                            </a>
                        </div>
                       ";           
                }
            }
            else{
                echo'<div class="starter-template">';
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
    ?> 
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Black Price 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
    <!-- jQuery -->


    <?php  include('footer.php'); ?>

</body>

</html>
