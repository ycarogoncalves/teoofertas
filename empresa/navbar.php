 <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#menu-toggle" class="btn button_menu" aria-label="Left Align"id="menu-toggle">
                    <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
                </a>
                <a class="navbar-brand" href="index.php"><img src='../images/BLACKPRICE80x80.png'></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				    <div id="searchBar" class="col-md-6">
		          	<!--<form class="navbar-form" role="search" method="post" id="search-form" name="search-form" action="resultadoBusca.php">
		              <div id="containerSearch" class="input-group">
                    <input autocomplete="off" onkeyup="loadProduto(this.value);" type="text" class="form-control" placeholder="Digite o nome do produto" list="produto" name="query" id="query">

                      <datalist name="produto" id="produto"></datalist>

                      <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
                </form>-->
              </div>

              <?php if(!empty($_SESSION['auth'])){
              ?>
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="menuAdmin"><?php $arrayNome = explode(" ",  $_SESSION['nomeUsuario']); $nomeUsuario = $arrayNome[0]; echo $nomeUsuario;?></span><span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="alteraDadosPessoais.php"><span class="glyphicon glyphicon-pencil"></span>  Editar dados</a></li>
                    <li><a href="alteraSenha.php"><span class="glyphicon glyphicon-pencil"></span>  Alterar senha</a></li>
                    <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                  </ul>
                </li>
              </ul>
              <?php
              }
              else{
              ?>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" role="button" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-user"></span> Entrar</a>
                    </li>
                </ul>
              <?
              }
              ?>

              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bullhorn" aria-hidden="true"></i> <span class="menuAdmin">Promoções</span><span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="cadPromo.php"><span class="glyphicon glyphicon-plus"></span> Cadastrar</a></li>
                    <li><a href="listaPromo.php"><span class="glyphicon glyphicon-search"></span> Pesquisar</a></li>
                  </ul>
                </li>
              </ul>

           </div>
        <!-- /.container -->
    </nav>

    <?php
        require_once("../conf/confBD.php");

        $conexao = conn_mysql();
        $sql = "select *from categoria order by descricaoCategoria asc";
        $operacao = $conexao->prepare($sql);
        $pesquisar = $operacao->execute();
        $resultado_cats = $operacao->fetchAll();
    ?>

    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <span>Navegação</span>
                </li>
                <?php
                    if(count($resultado_cats)>0){
                        foreach($resultado_cats as $categoria){?>
                            <li>
                                <a href="index.php?cat=<?=$categoria['idcategoria']?>">
                                    <i class="fa <?=$categoria['icone']?>" aria-hidden="true"></i>
                                    <?=$categoria['descricaoCategoria']?>
                                </a>
                            </li>
                    <?  }
                    }?>
            </ul>
        </div>
    </div>
