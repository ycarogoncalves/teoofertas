<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Black Price</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link href="../css/simple-sidebar.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/blackprice.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script src="../js/fileinput.js" type="text/javascript"></script>
    <script src="../js/fileinput_locale_pt.js" type="text/javascript"></script>   
    <script src="../js/jquery.js" type="text/javascript"></script>     
    <script src="../js/jquery.countdown.js"></script>

    <link rel="stylesheet" href="../jquery-ui/jquery-ui.css">
    <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
    <script src="../jquery-ui/jquery-ui.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

    <script type="text/javascript">
    $(function() {
        var dates = $('#from, #to').datepicker({
            dateFormat: 'dd/mm/yy',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior',
            defaultDate: "+w",
            changeMonth: true,
            numberOfMonths: 2,
            onSelect: function(selectedDate) {
                var option = this.id == "from" ? "minDate" : "maxDate";
                var instance = $(this).data("datepicker");
                var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });
    </script>

 <script language="javascript">    
 
function MascaraMoeda(objTextBox, SeparadorMilesimo, SeparadorDecimal, e){    
    var sep = 0;    
    var key = '';    
    var i = j = 0;    
    var len = len2 = 0;    
    var strCheck = '0123456789';    
    var aux = aux2 = '';    
    var whichCode = (window.Event) ? e.which : e.keyCode;    
    if (whichCode == 13 || whichCode == 8) return true;    
    key = String.fromCharCode(whichCode); // Valor para o código da Chave    
    if (strCheck.indexOf(key) == -1) return false; // Chave inválida    
    len = objTextBox.value.length;    
    for(i = 0; i < len; i++)    
        if ((objTextBox.value.charAt(i) != '0') && (objTextBox.value.charAt(i) != SeparadorDecimal)) break;    
    aux = '';    
    for(; i < len; i++)    
        if (strCheck.indexOf(objTextBox.value.charAt(i))!=-1) aux += objTextBox.value.charAt(i);    
    aux += key;    
    len = aux.length;    
    if (len == 0) objTextBox.value = '';    
    if (len == 1) objTextBox.value = '0'+ SeparadorDecimal + '0' + aux;    
    if (len == 2) objTextBox.value = '0'+ SeparadorDecimal + aux;    
    if (len > 2) {    
        aux2 = '';    
        for (j = 0, i = len - 3; i >= 0; i--) {    
            if (j == 3) {    
                aux2 += SeparadorMilesimo;    
                j = 0;    
            }    
            aux2 += aux.charAt(i);    
            j++;    
        }    
        objTextBox.value = '';    
        len2 = aux2.length;    
        for (i = len2 - 1; i >= 0; i--)    
        objTextBox.value += aux2.charAt(i);    
        objTextBox.value += SeparadorDecimal + aux.substr(len - 2, len);    
    }    
    return false;    
}    
</script>    

</head>