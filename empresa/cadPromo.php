<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Cadastrar Promoção</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="gravar_promo.php" enctype="multipart/form-data">


  <div class="form-group">
    <label for="desc_promocao">Descrição</label>
    <input type="text" class="form-control" name="desc_promocao" id="desc_promocao" placeholder="Descrição da promoção" autofocus>
  </div>

  <div class="form-group">
    <label for="from">Data de Início</label>
    <input type="text" class="form-control" name="inicio" id="from" placeholder="Data de Início" required>
  </div>


  <div class="form-group">
    <label for="from">Data de Término</label>
    <input type="text" class="form-control" name="fim" id="to" placeholder="Data de Término" required>
  </div>

  <div class="form-group">
      <label for="endereco">Preço</label>
      <input type="text" class="form-control" name="preco" id="preco" placeholder="Preço" onKeyPress="return(MascaraMoeda(this,'','.',event))">
  </div>

  <div class="form-group">
      <label class="control-label">Banner da promoção</label>
      <input id="file-0a" name="banner_promocao" class="file" type="file">
  </div>

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>
</div>

<?php include ('footer.php'); ?>

  </body>
</html>
