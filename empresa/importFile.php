<?php

session_start();

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
    
<div class="col-md-6">


<h2 class="titleH2">Importação de produtos</h2>

<p>Este procedimento pode demorar alguns minutos</p>

<div id="area"></div>

<?php require_once("navbar.php"); ?>

<div id="hideDiv">
 
<form id="formCad" method="post" action="carregarImportacao.php" enctype="multipart/form-data">

  <input type="hidden" name="MAX_FILE_SIZE" value="200971520" />

 <div class="form-group">
	  		<label class="control-label">Escolha o arquivo</label>
			<input id="file-0a" name="arquivo" class="file" type="file">
</div>	 
 

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="carregar" id="carregar">Carregar novos produtos</button> 
  </div>

</form>

</div>

</div>
</div>




<script type="text/javascript">


$( "#carregar" ).click(function() {
  
  $("#hideDiv").css("display", "none");

  $("#area").html("<img src='../images/loading.gif' width='200' height='200' />");
});


jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            endereco: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                              
            telefones: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
             horarioFuncionamento: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },  
            imagemSupermercado: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                       
          
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
