<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Cadastrar Produto</h2>

<?php require_once("navbar.php"); ?>


<form id="formCad" method="post" action="gravar_produto.php">

  <div class="form-group">
    <label for="nome">Código de barras</label>
    <input type="text" class="form-control" name="barras" id="barras" placeholder="Código de barras">
  </div>

  <div class="form-group">
    <label for="nome">Descrição</label>
    <input type="text" class="form-control" name="descricao" id="descricao" placeholder="Descrição">
  </div>

    <div class="form-group">
    <label for="nome">URL da Imagem</label>
    <input type="text" class="form-control" name="imagem" id="imagem" placeholder="URL da Imagem">
  </div>

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>
</div>


<script type="text/javascript">

jQuery(function($){
          
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            imagem: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            descricao: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                                              
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
    
  </body>
</html>
