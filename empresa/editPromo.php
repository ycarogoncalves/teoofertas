<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

   $idPromo = $_POST["editar"];
     
    // instancia objeto PDO, conectando no mysql
    $conexao = conn_mysql();    

    if(isset($_POST['excluir'])){

      try{
          $SQLUpdate = "UPDATE publicaOferta set status=2 WHERE idpublicaOferta='".$_POST['excluir']."';";
          $operacao = $conexao->prepare($SQLUpdate);            
          $update = $operacao->execute();

          if($update){
                  $SQLUpdate = "delete from rel_listaDeCompras_publicaOferta WHERE id_publicaoferta='".$_POST['excluir']."';";
                  $operacao = $conexao->prepare($SQLUpdate);            
                  $update = $operacao->execute();

                  echo '<script type="text/javascript">alert("Oferta excluída com sucesso.")</script>';
                  echo "<script>location.href='index.php';</script>";   
                  exit;   
          }
          else{
                  echo '<script type="text/javascript">alert("Erro ao excluir oferta.")</script>';
                  echo "<script>location.href='index.php';</script>";
                  exit;         
          }      
      } //try
      catch (PDOException $e)
      {
          echo "Erro!: " . $e->getMessage() . "<br>";
          die();
      }

    }

             
    try{


    // instrução SQL básica 
    $SQLSelect = "SELECT status,idpublicaOferta,desc_promocao,banner_promocao,precoOferta,date_format(dataInicial,'%d/%m/%Y') as dataInicial,dataFinal,date_format(dataFinal,'%d/%m/%Y') as dataTermino,nomeSupermercado FROM `publicaOferta`,`supermercado` WHERE supermercado_idsupermercado=supermercado.idsupermercado and idpublicaOferta = '".$idPromo."';";
  
    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if (count($resultados)>0){  
        foreach($resultados as $dadosEncontrados){ 
           $desc_promocao=$dadosEncontrados['desc_promocao']; 
           $precoOferta=$dadosEncontrados['precoOferta'];                     
           $dataInicial=$dadosEncontrados['dataInicial'];
           $dataFinal=$dadosEncontrados['dataTermino'];
           $banner_promocao=$dadosEncontrados['banner_promocao'];
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Editar Promoção</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="update_promo.php" enctype="multipart/form-data">

  <input type="hidden" name="idPromo" value="<?php echo $idPromo; ?>">

  <div class="form-group">
    <label for="desc_promocao">Descrição</label>
    <input type="text" class="form-control" name="desc_promocao" id="desc_promocao" placeholder="Descrição da promoção" value="<?php echo $desc_promocao; ?>">
  </div>

  <div class="form-group">
    <label for="from">Data de Início</label>
    <input type="text" class="form-control" name="inicio" id="from" placeholder="Data de Início" value="<?php echo $dataInicial; ?>" required>
  </div>


  <div class="form-group">
    <label for="from">Data de Término</label>
    <input type="text" class="form-control" name="fim" id="to" placeholder="Data de Término"  value="<?php echo $dataFinal; ?>" required>
  </div>

  <div class="form-group">
      <label for="endereco">Preço</label>
      <input type="text" class="form-control" name="preco" id="preco" placeholder="Preço" value="<?php echo $precoOferta; ?>" onKeyPress="return(MascaraMoeda(this,'','.',event))" autofocus>
  </div>
  
  <img src="<?php echo "$banner_promocao"; ?>" alt="" />
     <div class="form-group">
            <label class="control-label">Banner</label>
          <input id="file-0a" name="banner_promocao" class="file" type="file">
    </div>   

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>
</div>

<?php include ('footer.php'); ?>

  </body>
</html>
