<?php

session_start();

$idusuario= $_SESSION['idUser'];

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Editar Produto</h2>

<?php require_once("navbar.php"); ?>
 
   <?php 
      // instancia objeto PDO, conectando no mysql

    $id=$_GET['id'];
    $conexao = conn_mysql();      


    try{


      // instrução SQL básica 
      $SQLSelect = "SELECT supermercado_idsupermercado FROM `supermercado_tem_usuario` where usuario_idusuario=$idusuario";
    
      //prepara a execução da sentença
      $operacao = $conexao->prepare($SQLSelect);    
          
      $pesquisar = $operacao->execute();
      
      //captura TODOS os resultados obtidos
      $resultados = $operacao->fetchAll();

      // se há resultados, os escreve em uma tabela
      if (count($resultados)>0){  
          foreach($resultados as $dadosEncontrados){ 
             $idsupermercado=$dadosEncontrados['supermercado_idsupermercado'];                     
          }
      }


    // instrução SQL básica 
    $SQLSelect = "SELECT idsupermercado_tem_produto,supermercado.nomeSupermercado as nomeSupermercado,precoProduto,produto.descricaoProduto,produto.imagemProduto,produto.barras,produto.idproduto FROM `supermercado_tem_produto`,produto,supermercado WHERE supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado and supermercado_tem_produto.produto_idproduto=produto.idproduto and supermercado.idsupermercado=$idsupermercado and idsupermercado_tem_produto=$id;";
  
    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if (count($resultados)>0){  
        foreach($resultados as $dadosEncontrados){ 
                    $id = $dadosEncontrados['idsupermercado_tem_produto'];
                    $barras = $dadosEncontrados['barras'];
                    $descricaoProduto = $dadosEncontrados['descricaoProduto'];
                    $imagemProduto = $dadosEncontrados['imagemProduto'];
                    $precoProduto = $dadosEncontrados['precoProduto'];
                    $precoProduto = number_format($precoProduto,2,",",".");
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  

<form id="formCad" method="post" action="update_produto.php">

<input type="hidden" name="id" value="<?php echo $id; ?>">

  <div class="form-group">
    <label for="nome">Código de barras</label>
    <input type="text" class="form-control" name="barras" id="barras" placeholder="Código de barras" value="<?php echo $barras; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="nome">Descrição</label>
    <input type="text" class="form-control" name="descricao" id="descricao" placeholder="Descrição" value="<?php echo $descricaoProduto; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="nome">Preço</label>
    <input type="text" class="form-control" onKeyPress="return(MascaraMoeda(this,'','.',event))" name="preco" id="preco" placeholder="Preço"> (Último preço: R$ <?php echo $precoProduto; ?>)
  </div>


  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Atualizar</button> 
  </div>

</form>

</div>
</div>


<script type="text/javascript">

jQuery(function($){
          
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            imagem: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            descricao: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                                              
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
    
  </body>
</html>
