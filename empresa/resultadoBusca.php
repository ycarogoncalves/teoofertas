<?php

session_start();

$idusuario= $_SESSION['idUser'];


include('../conf/confbd.php');

include('authSession.php');

$produto = $_POST['query'];

$_SESSION['pesquisa']= $produto;
            
?>

    <!DOCTYPE html>
<html lang="pt-br">

<?php include('head.php'); ?>

<body>
    <?php include('navbar.php'); ?>

    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Produtos</h2>
            </div>


        <?php 

            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            $i=1;
            try{
                $SQLSelect = "SELECT * FROM `atualizacao`;"; 
                $operacao = $conexao->prepare($SQLSelect);     
                $pesquisar = $operacao->execute();
                $resultados = $operacao->fetchAll();
                $count = $operacao->rowCount();
                if (count($resultados)>0){  
                                foreach($resultados as $dadosEncontrados){
                                    $dataAtualizacao=$dadosEncontrados['dataAtualizacao'];
                                    $idsupermercado=$dadosEncontrados['supermercado_idsupermercado'];

                                    if($i<$count){
                                        $atualizacao .="(idsupermercado=$idsupermercado and dataAtualizacao='$dataAtualizacao') or ";
                                    }
                                    else{
                                        $atualizacao .="(idsupermercado=$idsupermercado and dataAtualizacao='$dataAtualizacao')";
                                    }   
                                    $i++;
                                }
                }
            } //try
            catch (PDOException $e)
            {
            // caso ocorra uma exceção, exibe na tela
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
            }


            // instrução SQL básica 
            $SQLSelect = "SELECT supermercado_idsupermercado FROM `supermercado_tem_usuario` where usuario_idusuario=$idusuario";
          
            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);    
                
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){ 
                   $idsupermercado=$dadosEncontrados['supermercado_idsupermercado'];                     
                }
            }

            // instrução SQL básica 
            $SQLSelect = "SELECT idsupermercado_tem_produto,supermercado.nomeSupermercado as nomeSupermercado,precoProduto,produto.descricaoProduto,produto.imagemProduto,produto.barras,produto.idproduto FROM `supermercado_tem_produto`,produto,supermercado WHERE ($atualizacao) and supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado and supermercado_tem_produto.produto_idproduto=produto.idproduto and produto.descricaoProduto like '$produto%' and supermercado.idsupermercado=$idsupermercado order by barras;";

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();


            // ALGORITMO COMPARAR E CALCULAR MENOR PREÇO
            $i=1;
            $j=0;
            $menorPreco=100000;
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $barras[$i] = $dadosEncontrados['barras'];
                    $preco[$i] = $dadosEncontrados['precoProduto'];

                    if($barras[$i]==$barras[$i-1]){
                    	if($preco[$i-1]<$menorPreco){
                    		$menorPreco=$preco[$i-1];
                    		if($preco[$i]<$menorPreco){
                    			$menorPreco=$preco[$i];
                    			$menorPrecoPos[$j]=$i;
                    		}
                    		else{
                    			$menorPrecoPos[$j]=$i-1;
                    		}
                    	}
                    }else{
                    	$j++;
                    	$menorPreco=100000;
                    }
                    $i++;
                }
             }
            // se há resultados, os escreve em uma tabela
            $i=1;
            $j=1;
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $id = $dadosEncontrados['idsupermercado_tem_produto'];
                    $barras = $dadosEncontrados['barras'];
                    $descricaoProduto = $dadosEncontrados['descricaoProduto'];
                    $imagemProduto = $dadosEncontrados['imagemProduto'];
                    $precoProduto = $dadosEncontrados['precoProduto'];
                    $precoProduto = number_format($precoProduto,2,",",".");

                    /*
                    if (!getimagesize($imagemProduto)) {
                        $imagemProduto = 'images/notfound.png';
                    } 
                    */


                    if(!empty($_SESSION['auth'])){
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6'>
                            <span class='addProduto'><i class='fa fa-pencil'></i></span>
                            <img class='blackItem' src='../images/BLACKPRICE80x80.png' alt=''>
                            <a href='editarProduto.php?id=$id'>
                                <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                <span class='productDescription'>".substr($descricaoProduto, 0,38)."</span>
                                <span class='productDescription'>$barras</span>                               
                                <p class='productPrice'> $precoProduto</p>
                                <p class='productSupermercado'></p>
                            </a>
                        </div>
                       ";   

                     }else{
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6'>
                            <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                            <a href='portfolio-item.php?id=$id'>
                                <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                <span class='productDescription'>".substr($descricaoProduto, 0,36)."</span>
                                <p class='productPrice'>R$ $precoProduto</p>
                                <p class='productSupermercado'>$nomeSupermercado</p>
                            </a>
                        </div>
                       ";
                     }    
                     $i++;          
                }
            }
            else{
                echo'<div class="starter-template">';
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
    ?>  
                                        
        </div>
        <!-- /.row -->

        <hr>

       <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Supermercados participantes</h2>
            </div>

            <div class="portfolio-supermercado col-md-4 col-sm-6">
                <a href="#">
                    <img class="img-responsive img-portfolio img-hover" src="../images/supermercados/tiateca.png" alt="">
                </a>
            </div>

            <div class="portfolio-supermercado col-md-4 col-sm-6">
                <a href="#">
                    <img class="img-responsive img-portfolio img-hover" src="../images/supermercados/epaplus.png" alt="">
                </a>
            </div>

            <div class="portfolio-supermercado col-md-4 col-sm-6">
                <a href="#">
                    <img class="img-responsive img-portfolio img-hover" src="../images/supermercados/sevia.png" alt="">
                </a>
            </div>
         </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Black Price 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <?php include('footer.php'); ?>    
</body>

</html>
