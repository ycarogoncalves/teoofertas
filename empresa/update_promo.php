<?php 

session_start();
$idUser = $_SESSION['idUser'];

    require_once("../conf/confbd.php");

function converteData($data, $se, $ss){
    return implode($ss, array_reverse(explode($se, $data)));
}

    $idPromo= $_POST ["idPromo"];
    $inicio= $_POST ["inicio"];
    $fim= $_POST ["fim"];
    $preco= htmlspecialchars($_POST ["preco"]);
    $desc_promocao= htmlspecialchars($_POST ["desc_promocao"]);
    $dataInicial = converteData($inicio,'/','-');
    $dataFinal = converteData($fim,'/','-');

    $conexao = conn_mysql();  


    try{
      if(!empty($_FILES['banner_promocao']['name'])){

          $uploaddir = '../images/promocoes/';

          $Imagem = $_FILES['banner_promocao']['name'] . date('l jS \of F Y h:i:s A');

          $tipo = explode(".", $_FILES['banner_promocao']['name']);

          $uploadfile = $uploaddir . md5($Imagem) . '.' . $tipo[1];
        
          if (move_uploaded_file($_FILES['banner_promocao']['tmp_name'], $uploadfile)) {
            $SQLUpdate = "UPDATE `publicaOferta` set precoOferta='$preco',desc_promocao='$desc_promocao',banner_promocao='$uploadfile',dataInicial='$dataInicial',dataFinal='$dataFinal' where idpublicaOferta=$idPromo;";
           
            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLUpdate);    
        
            $update = $operacao->execute();
          }
      }
      else{
        // instrução SQL básica 
        $SQLUpdate = "UPDATE `publicaOferta` set precoOferta='$preco',desc_promocao='$desc_promocao',dataInicial='$dataInicial',dataFinal='$dataFinal' where idpublicaOferta=$idPromo;";

        //prepara a execução da sentença
        $operacao = $conexao->prepare($SQLUpdate);    
            
        $update = $operacao->execute();
      }

      if ($update){

            $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
            $operacao = $conexao->prepare($SQLLogs);            
            $inserirLog = $operacao->execute(array($SQLUpdate,$idUser));          
                
        echo '<script language="javascript">';
        echo utf8_decode('alert("Promoção alterada com sucesso.")');
        echo '</script>';
        $href = '<script language="javascript">location.href="listaPromo.php";</script>';
        echo $href;
         }
       else {
          echo '<script language="javascript">';
          echo utf8_decode('alert("Erro ao alterar promoção.")');
          echo '</script>';
        $href = '<script language="javascript">location.href="listaPromo.php";</script>';
        echo $href;
       }  

  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  
