<?php

session_start();
$idUser = $_SESSION['idUser'];

unset($_SESSION['arrayProdutosCadastrar']);

require_once("authSession.php");
require_once("../conf/confbd.php");

try
{
	if(count($_POST) < 1){
		header("Location: acessoNegado.php");
		die();
	}
	else{

try{
	// instancia objeto PDO, conectando no mysql
		$conexao = conn_mysql();

		 // capturar id do supermercado
	     $SQLSelect = "SELECT supermercado_idsupermercado FROM `supermercado_tem_usuario` where usuario_idusuario=$idUser;";
	        //prepara a execução da sentença
	        $operacao = $conexao->prepare($SQLSelect);      
	               
	        $pesquisar = $operacao->execute();
	        
	        //captura TODOS os resultados obtidos
	        $resultados = $operacao->fetchAll();

	        // se há resultados, os escreve em uma tabela
	        if (count($resultados)>0){  
	            foreach($resultados as $dadosEncontrados){
	                $idsupermercado = $dadosEncontrados['supermercado_idsupermercado'];	
	             }
	         }

	        $dataAtual=date("Y-m-d");

			$uploaddir = 'arquivos/';

			$tipo = explode(".", $_FILES['arquivo']['name']);

			$nomeArquivo = md5($idsupermercado.$uploaddir.$_FILES['arquivo']['name'].$dataAtual.'#blackPrice');

			$uploadfile = $uploaddir . $nomeArquivo . '.' . $tipo[1];

			if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $uploadfile)) {

				$arquivoSupermercado = fopen("$uploadfile", "r");

				$cont=0;
				while(!feof($arquivoSupermercado)) {

					$linha=fgets($arquivoSupermercado);
				  	$arrayArquivoSupermercado = explode(';',$linha);
				  	$barras=$arrayArquivoSupermercado[0];
				  	$preco=$arrayArquivoSupermercado[1];
				  	$preco= rand(2, 10) . '.' . rand(10, 99);
				  	$dataAtual=date("Y-m-d");

					 // capturar id do supermercado
				     $SQLSelect = "SELECT idproduto FROM `produto` where barras=$barras;";

				        //prepara a execução da sentença
				        $operacao = $conexao->prepare($SQLSelect);      
				               
				        $pesquisar = $operacao->execute();
				        
				        //captura TODOS os resultados obtidos
				        $resultados = $operacao->fetchAll();

				        // se há resultados, os escreve em uma tabela
				        if (count($resultados)>0){  
				            foreach($resultados as $dadosEncontrados){
				                $idproduto = $dadosEncontrados['idproduto'];	
				             }

						     $SQLInsert = 'INSERT INTO `supermercado_tem_produto` (`supermercado_idsupermercado`,`produto_idproduto`,`precoProduto`,`dataAtualizacao`) VALUES (?,?,?,?)';
							 $operacao = $conexao->prepare($SQLInsert);					  
							 $inserir = $operacao->execute(array($idsupermercado,$idproduto,$preco,$dataAtual));
		            $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
		            $operacao = $conexao->prepare($SQLLogs);            
		            $inserirLog = $operacao->execute(array($SQLUpdate,$idUser));  
							 if($inserir){
							 	$cont++;
							 	$qtdeProdutos=$cont;
							 }
				        }else{
				        	$arrayProdutosCadastrar[]=$barras;
				        }
					}
					fclose($arquivoSupermercado);

				    // instrução SQL básica 
				    $SQLUpdate = "UPDATE `atualizacao` set dataAtualizacao='$dataAtual',quantidade=$cont where supermercado_idsupermercado=$idsupermercado;";
					//prepara a execução da sentença
				    $operacao = $conexao->prepare($SQLUpdate);    
				    $update = $operacao->execute();

		            $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
		            $operacao = $conexao->prepare($SQLLogs);            
		            $inserirLog = $operacao->execute(array($SQLUpdate,$idUser));  

				    $SQLSelect = "SELECT * FROM `atualizacao` where supermercado_idsupermercado=$idsupermercado;";
	   		        $operacao = $conexao->prepare($SQLSelect);      
				    $pesquisar = $operacao->execute();
				    $resultados = $operacao->fetchAll();
				    if (count($resultados)==0){  
						 $SQLInsert = "INSERT INTO `atualizacao` (`supermercado_idsupermercado`,`quantidade`,`dataAtualizacao`) VALUES ($idsupermercado,$cont,'$dataAtual')";
						 $operacao = $conexao->prepare($SQLInsert);					  
						 $inserir = $operacao->execute();	

			            $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
			            $operacao = $conexao->prepare($SQLLogs);            
			            $inserirLog = $operacao->execute(array($SQLUpdate,$idUser));  	
	
					}			 	    	
			 
			}
			
			$tam = sizeof($arrayProdutosCadastrar);
			if($tam>0){
				$_SESSION['arrayProdutosCadastrar']=$arrayProdutosCadastrar;
				$href = '<script language="javascript">location.href="pendencias.php";</script>';
			}else{
				$href = '<script language="javascript">location.href="index.php";</script>';				
			}

			if ($cont > 0){
				echo '<script language="javascript">';
				echo utf8_decode('alert("Produtos importados com sucesso.")');
				echo '</script>';
				echo $href;
   			 }
			 else {
	  			echo '<script language="javascript">';
	   			echo utf8_decode('alert("Erro ao importar produtos.")');
	   			echo '</script>';
				$href = '<script language="javascript">location.href="index.php";</script>';
				echo $href;
			 }			
		
	} //try
		catch (PDOException $e)
		{
		    echo "Erro!: " . $e->getMessage() . "<br>";
		    die();
		}

	}

} //try
catch (PDOException $e)
{
	// caso ocorra uma exceÃ§Ã£o, exibe na tela
	echo "Erro!: " . $e->getMessage() . "<br>";
	die();
}

?>

