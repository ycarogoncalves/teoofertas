<?php

session_start();
$idUser=$_SESSION['idUser'];

//print_r($_POST);

$dataHoje=date('Y-m-d');

require_once("conf/confbd.php");

	$idproduto=$_POST['idOferta'];

	// instancia objeto PDO, conectando no mysql
	$conexao = conn_mysql();

	$SQLSelect = "SELECT idlistaDeCompras from listaDeCompras where usuario_idusuario=$idUser;";   

	// $resultados = mysqli_query($conexao,$SQLSelect);

	$operacao = $conexao->prepare($SQLSelect);      
	$pesquisar = $operacao->execute();
	$resultados = $operacao->fetchAll();

	if(isset($_POST['add'])){
		try{

			if ( !empty($resultados) ) {  
				foreach($resultados as $dadosEncontrados){
					$idlistaDeCompras=$dadosEncontrados['idlistaDeCompras'];
				}
			}
			else{
				//criando a lista
				$SQLInsert = 'INSERT INTO `listaDeCompras` (`dataCriacaoLista`,`usuario_idusuario`) VALUES (?,?)';	  

				// $lastIdlistaDeCompras = mysqli_query($conexao,$SQLInsert);
				$operacao = $conexao->prepare($SQLInsert);					  
				$inserir = $operacao->execute(array($dataHoje,$idUser));
				$lastIdlistaDeCompras = $conexao->lastInsertId(); 

				$idlistaDeCompras=$lastIdlistaDeCompras;
			}

			//inserindo ofertas na lista
			$SQLInsert = 'INSERT INTO `rel_listaDeCompras_publicaOferta` (`id_listadecompras`,`id_publicaoferta`) VALUES (?,?)';
			
			$operacao = $conexao->prepare($SQLInsert);					  
			$inserir = $operacao->execute(array($idlistaDeCompras,$idproduto));
		}
		catch (PDOException $e)
		{
			echo "Erro!: " . $e->getMessage() . "<br>";
			die();
		}	
	}
	else{
		try{
				// instancia objeto PDO, conectando no mysql
			$conexao = conn_mysql();

			if (count($resultados)>0){  
				foreach($resultados as $dadosEncontrados){
					$idlistaDeCompras=$dadosEncontrados['idlistaDeCompras'];
					$SQLDelete = "DELETE FROM `rel_listaDeCompras_publicaOferta` where id_listadecompras=$idlistaDeCompras and id_publicaoferta=$idproduto;";
					$operacao = $conexao->prepare($SQLDelete);					  
					$delete = $operacao->execute();
				}
			}

		} //try
		catch (PDOException $e)
		{
			echo "Erro!: " . $e->getMessage() . "<br>";
			die();
		}	
	}
	$conexao = null;

?>

