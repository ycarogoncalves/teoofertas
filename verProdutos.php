<?php

session_start();

include('conf/confbd.php');

?>
       <?php 
            
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $SQLSelect = "
                   SELECT 
                        publicaOferta.*,
                        date_format(dataFinal,'%d/%m/%Y') as dataFinalformat,
                        nomeSupermercado,
                        idsupermercado,
                        id_categoria,
                        imagemSupermercado,
                        mapa,
                        endereco,
                        horarioFuncionamento,
                        telefone
                    FROM 
                        `publicaOferta`
                        inner join supermercado on idsupermercado=supermercado_idsupermercado 
                        inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                    WHERE 
                        status= 1 
                        and idpublicaOferta = $id;"; 
                        $oferta=1;
               
            }
            elseif(isset($_GET['idproduto'])){
                $id = $_GET['idproduto'];
                $SQLSelect = "SELECT *,precoProduto as preco from produto p 
                                right join supermercado_tem_produto sp on p.idproduto=sp.produto_idproduto 
                                left join publicaOferta po on p.idproduto=po.produto_idproduto
                                where  idsupermercado_tem_produto = $id;";  
                                $oferta=0;               
            }

            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // $resultados = mysqli_query($conexao,$SQLSelect);

            // se há resultados, os escreve em uma tabela
            if ( !empty($resultados) ){  
                foreach($resultados as $dadosEncontrados){
                    $idProdutoGraph = $dadosEncontrados['idpublicaOferta'];
                    $idSupGraph = $dadosEncontrados['idsupermercado'];
                    $descricaoProduto = $dadosEncontrados['desc_promocao'];
                    $imagemProduto = $dadosEncontrados['banner_promocao'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $precoOferta =  $dadosEncontrados['precoOferta'];
                    $precoOferta = number_format($precoOferta,2,",",".");
                    $idproduto = $dadosEncontrados['idpublicaOferta'];
                    $mapa = $dadosEncontrados['mapa'];
                    $id_categoria = $dadosEncontrados['id_categoria'];
                    $imagemSupermercado=$dadosEncontrados['imagemSupermercado'];
                    $dataFinalformat=$dadosEncontrados['dataFinalformat'];
                    $endereco=$dadosEncontrados['endereco'];
                    $horarioFuncionamento=$dadosEncontrados['horarioFuncionamento'];
                    $telefone=$dadosEncontrados['telefone'];
                    /*
                    if (!getimagesize($imagemProduto)) {
                        $imagemProduto = 'images/notfound.png';
                    } 
                    */
                }
                $SQLDelete = "update `publicaOferta` set qtde_cliques=qtde_cliques+1 where idpublicaOferta = $idproduto;";
                $operacao = $conexao->prepare($SQLDelete);                    
                $delete = $operacao->execute();
                
            }
            else{
                echo'<div class="starter-template">';
                echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
    ?> 
    

<script>
  fbq('track', 'ViewContent');
</script>

<body>
    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Item Row -->
        <div class="row">
            <div class="col-md-3">
                <img class="img-responsive imagemDestaqueProduto" src="<?=str_replace('../','',$imagemProduto); ?>" alt="">
            </div>
            <div class="col-md-3 col-xs-12">
                <h3><?php echo $descricaoProduto; ?> </h3>
                <p class="productSupermercado"><?php echo $nomeSupermercado; ?></p>
                <p class="productPrice">R$ <?php echo $precoOferta; ?>

             <!-- Facebook Share Button Functionaliy    -->
            <script>function fbs_click() {
            u="https://www.teoofertas.com.br/share.php?id=<?=$idproduto?>";
            t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');return false;}
            </script>

            <a rel="nofollow" class="_49vh _2pi7" href="https://www.teoofertas.com.br/share.php?id=<?=$idproduto?>" onclick="return fbs_click()" target="_blank"><img src="images/facebook.png" alt="Share" /></a>
   
                <br>

                <strong>
                <p>
                <?php echo $endereco; ?> <br>
                Funcionamento: <?php echo $horarioFuncionamento; ?> <br>
                Contato: <?php echo $telefone; ?>
                </p>
                </strong>

                <div class="col-md-5 col-xs-4">
                    <img class="img-responsive imagemDestaqueMarca" src="<?=$imagemSupermercado?>" alt="">
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <?php
                if(strlen($mapa)>0){
                    ?>
                    <div class="row">
                         <?=$mapa; ?>
                        <!-- /.row -->
                    </div>
                    <?php 
                }
                ?>
            </div>            
        </div>
    </div>
    <!-- /.container -->
</body>
</html>
