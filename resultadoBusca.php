<?php

session_start();

include('conf/confbd.php');

$idUser = $_SESSION['idUser'];

include('authSession.php');

$produto = $_POST['query'];
if(empty($produto)){
    $produto = $_GET['query'];
}
            
?>

<!DOCTYPE html>
<html lang="pt-br">

<?php include('head.php'); ?>

<body>
    <?php include('navbar.php'); ?>

     <!-- Carrega Produtos -->
    <div id="carregaProdutos">

    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Produtos em destaque</h2>
            </div>


        <?php 



            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    


        //LISTA DE COMPRAR DO USUÁRIO
        try{
            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            $SQLSelect = "SELECT idlistaDeCompras FROM `listaDeCompras` WHERE usuario_idusuario=$idUser order by dataCriacaoLista desc limit 1;";   
            $operacao = $conexao->prepare($SQLSelect);      
            $pesquisar = $operacao->execute();
            $resultados = $operacao->fetchAll();

            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                        $idlistaDeCompras=$dadosEncontrados['idlistaDeCompras'];
                    }
            }

                try{
                    // instancia objeto PDO, conectando no mysql
                    $conexao = conn_mysql();    

                    $SQLSelect = "SELECT * FROM `listaDeCompras_em_supermercado` WHERE `idlistaDeCompras`=$idlistaDeCompras;";   
                  //  echo $SQLSelect;
                    $operacao = $conexao->prepare($SQLSelect);      
                    $pesquisar = $operacao->execute();
                    $resultados = $operacao->fetchAll();

                    if (count($resultados)>0){  
                        foreach($resultados as $dadosEncontrados){
                                $minhaListaDeCompras[]=$dadosEncontrados['idsupermercado_tem_produto'];
                            }
                    }


                }
                catch (PDOException $e)
                {
                    echo "Erro!: " . $e->getMessage() . "<br>";
                    die();
                } 

        }
        catch (PDOException $e)
        {
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
        } 
        // FIM
        
            /*
            // instrução SQL básica 
            $SQLSelect = "SELECT sum(quantidade) as limite FROM `atualizacao`,`supermercado` WHERE supermercado.idsupermercado=atualizacao.supermercado_idsupermercado;";

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $limite = $dadosEncontrados['limite'];
                }
            }
            */



            $i=1;
            try{
                $SQLSelect = "SELECT * FROM `atualizacao`;"; 
                $operacao = $conexao->prepare($SQLSelect);     
                $pesquisar = $operacao->execute();
                $resultados = $operacao->fetchAll();
                $count = $operacao->rowCount();
                if (count($resultados)>0){  
                                foreach($resultados as $dadosEncontrados){
                                    $dataAtualizacao=$dadosEncontrados['dataAtualizacao'];
                                    $idsupermercado=$dadosEncontrados['supermercado_idsupermercado'];

                                    if($i<$count){
                                        $atualizacao .="(idsupermercado=$idsupermercado and dataAtualizacao='$dataAtualizacao') or ";
                                    }
                                    else{
                                        $atualizacao .="(idsupermercado=$idsupermercado and dataAtualizacao='$dataAtualizacao')";
                                    }   
                                    $i++;
                                }
                }
            } //try
            catch (PDOException $e)
            {
            // caso ocorra uma exceção, exibe na tela
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
            }



            try{


            // instrução SQL básica 
           /* $SQLSelect = "SELECT *,precoProduto as preco from produto p 
                                right join supermercado_tem_produto sp on p.idproduto=sp.produto_idproduto 
                                left join publicaOferta po on p.idproduto=po.produto_idproduto 
                                where p.descricaoProduto like '$produto%' order by barras,precoProduto;";
            */
            $SQLSelect = "SELECT *, if(precoOferta is not null,'1','0') as tipo from produto inner join supermercado_tem_produto on produto.idproduto=supermercado_tem_produto.produto_idproduto inner join supermercado on supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado left join publicaOferta on produto.idproduto=publicaOferta.produto_idproduto and publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado where produto.descricaoProduto like '$produto%' and ($atualizacao) order by barras,precoProduto limit 30;";   

          //  echo $SQLSelect;

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();
            $count = $operacao->rowCount();


            // ALGORITMO COMPARAR E CALCULAR MENOR PREÇO
            $i=1;
            $j=1;
            $menorPreco=100000;

            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){

                        $tipo = $dadosEncontrados['tipo'];

                        // Se é promoção
                        if($tipo==1){
                            $dadosEncontrados['precoProduto']=$dadosEncontrados['precoOferta'];
                        }

                        $preco[0]=100000;
                        $barras[0] = $dadosEncontrados['barras'];
                        $barras[$i] = $dadosEncontrados['barras'];
                        $preco[$i] = $dadosEncontrados['precoProduto'];

                        if($barras[$i]==$barras[$i-1]){

                        	if($preco[$i-1]<=$menorPreco){

                        		$menorPreco=$preco[$i-1];

                        		if($preco[$i]<=$menorPreco){
              
                        			$menorPreco=$preco[$i];
                        			$menorPrecoPos[$j]=$i;

                        		}
                        		else{
                        			$menorPrecoPos[$j]=$i-1;
                        		}

                        	}elseif($preco[$i]<=$menorPreco){

                                    $menorPreco=$preco[$i];
                                    $menorPrecoPos[$j]=$i;
                            }
                        }else{
                        	$j++;
                        	$menorPreco=100000;
                        }
                        $i++;
                }
             }

            // se há resultados, os escreve em uma tabela
            $i=1;
            $j=1;


            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $tipo = $dadosEncontrados['tipo'];
                	$id = $dadosEncontrados['idsupermercado_tem_produto'];
                    $barras = $dadosEncontrados['barras'];
                    $descricaoProduto = $dadosEncontrados['descricaoProduto'];
                    $imagemProduto = $dadosEncontrados['imagemProduto'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $idOferta = $dadosEncontrados['idpublicaOferta'];
                    $idProSup = $dadosEncontrados['idsupermercado_tem_produto'];

                    if (in_array($idProSup, $minhaListaDeCompras, true)) {
                        $classLista = "fa-trash";
                    }else{
                        $classLista = "fa-cart-plus";
                    }


                    // Se é promoção
                    if($tipo==1){
                        $dadosEncontrados['precoProduto']=$dadosEncontrados['precoOferta'];
                        $href="id";
                        $id=$idOferta;
                    }
                    else{
                       $href="idproduto"; 
                    }
                    $precoProduto =  $dadosEncontrados['precoProduto']; 
                   
                    $precoProduto = number_format($precoProduto,2,",",".");

                    /*
                    if (!getimagesize($imagemProduto)) {
                        $imagemProduto = 'images/notfound.png';
                    } 
                    */

                    if($menorPrecoPos[$j]==$i){
                    	$classBlackItem="blackItemOn";
                    	$j++;
                    }
                    else{
                    	$classBlackItem="blackItem";
                    }

                    if(!empty($_SESSION['auth'])){
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6'>
                            <span onclick='addtoCart(\"$idProSup\")'; class='addProduto'><i id='item_$idProSup' class='fa $classLista'></i></span>                            <img class='$classBlackItem' src='images/BLACKPRICE80x80.png' alt=''>
                            <a href='portfolio-item.php?$href=$id'>
                                <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                <span class='productDescription'>".substr($descricaoProduto, 0,38)."</span>
                                <p class='productPrice'>R$ $precoProduto</p>
                                <p class='productSupermercado'>$nomeSupermercado</p>
                            </a>
                        </div>
                       ";   

                     }else{
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6'>
                            <img class='$classBlackItem' src='images/BLACKPRICE80x80.png' alt=''>
                            <a href='portfolio-item.php?$href=$id'>
                                <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                <span class='productDescription'>".substr($descricaoProduto, 0,36)."</span>
                                <p class='productPrice'>R$ $precoProduto</p>
                                <p class='productSupermercado'>$nomeSupermercado</p>
                            </a>
                        </div>
                       ";
                     }    
                     $i++;          
                }
            }
            else{
                echo'<div class="starter-template">';
                echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
    ?>  
                                        
        </div>
        <!-- /.row -->

        <hr>

       <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Supermercados participantes</h2>
            </div>

<?php

           // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            $SQLSelect="SELECT * from supermercado where lixeira = 0;";

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $idsupermercado = $dadosEncontrados['idsupermercado'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $imagemSupermercado = $dadosEncontrados['imagemSupermercado'];
                    $imagemSupermercado=str_replace("..", ".", $imagemSupermercado);
                    $lat = $dadosEncontrados['lat'];
                    $lgt = $dadosEncontrados['lgt'];
                    $telefone = $dadosEncontrados['telefone'];
                    $endereco = $dadosEncontrados['endereco'];
                    $horarioFuncionamento = $dadosEncontrados['horarioFuncionamento'];         

               echo    "<div class='portfolio-supermercado col-md-4 col-sm-6'>
                        <a href='supermercado.php?id=$idsupermercado'>
                            <img class='img-responsive img-portfolio img-hover' src='$imagemSupermercado' alt=''>
                        </a>
                        </div>";

                }

            }
            else{
                echo'<div class="starter-template">';
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
?>

         </div>

        <hr>

    </div>
    <!-- /.container -->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

    <?php include('footer.php'); ?>    
</div>
</body>

</html>
