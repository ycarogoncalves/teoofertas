<?php

session_start();

/*if($_SESSION['tipo']==1 ||$_SESSION['tipo']==2){
    
   $_SESSION = array();  //Limpa o vetor de sessão
   session_destroy();       // Destruímos a sessão em si

   header("Location:./acessoNegado.php");
  die();    
}*/
include('conf/confbd.php');

$com_categoria = '';
$outra_categoria='';
if(isset($_GET['cat'])){
    $categoria = $_GET['cat'];
    if(!is_numeric($categoria))
        header('Location:index.php');

    $conexao = conn_mysql();    

    $SQLSelect = "select * from categoria where idcategoria=? and lixeira=0";
    
    // $categoria_escolhida = mysqli_query($conexao,$SQLSelect);
    
    $operacao = $conexao->prepare($SQLSelect);
    $pesquisar = $operacao->execute(array($categoria));
    $categoria_escolhida = $operacao->fetch();

    $conexao = null;

    $com_categoria = 'and id_categoria='.$categoria;
    $outra_categoria = 'and id_categoria<>'.$categoria;
} else {
    $categoria_escolhida = array(
        'idcategoria' => 25,
        'descricaoCategoria' => 'Supermercados',
        'icone' => 'fa-shopping-basket'
    );
}

if( !empty($_SESSION['idUser']) ) {
    $idUser = $_SESSION['idUser'];
} else {
    $idUser = '';
}

$datahoje=date('Y-m-d');

?>

<!DOCTYPE html>
<html lang="pt-br">

<?php include('head.php'); ?>

<body>
    <style>
        .msg{
            text-align:center;
            font-size:13px;
            color:#F00;
            display:inline-block;
            margin-top:30px;
            width:100%;
        }


        #loadingGif{
            display: none;
        }

    </style>
	<?php include('navbar2.php'); ?>

    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('images/banner/banner01.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('images/banner/banner02.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('images/banner/banner03.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('images/banner/banner04.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>            
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    
    <!-- Page Content -->
    <div class="container">

        <div class='col-lg-4' style="margin-left: -14px;  margin-top: 20px;">
            <label for="supermercados">Selecione</label>
            <select class="form-control" id="sup"> 
            <option value="">Todos Supermercados</option>
                <?php

                    $conexao = conn_mysql();  
                    $SQLEmpresa = "SELECT idsupermercado,nomeSupermercado from supermercado,publicaOferta where publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado and supermercado.lixeira=0 group by idsupermercado";  
                    // $resultadoEmpresa = mysqli_query($conexao,$SQLEmpresa);

                    $operacaoEmpresa = $conexao->prepare($SQLEmpresa);
                    $pesquisarEmpresa = $operacaoEmpresa->execute();
                    $resultadoEmpresa = $operacaoEmpresa->fetchAll();
                    foreach($resultadoEmpresa as $empresa){
                        $idsupermercado=$empresa['idsupermercado'];
                        $nomeSupermercado=$empresa['nomeSupermercado'];
                        echo "<option value='$idsupermercado'>$nomeSupermercado</option>";
                    }          
                ?>                      
            </select>
        </div>

        <spam id='loadingGif'>Carregando ofertas, aguarde...<img src='images/ajax-loader.gif' alt='' /></spam>

        <?php
        if(!isset($_GET['buscar'])){
            ?>
            <!--top 6 promocoes encerradas-->
            <div id='topProduto' class='row'>
                <?php
                    
                    $conexao = conn_mysql();    

                    try{
                        $SQLSelect = "
                            SELECT *,
                                if(precoOferta is not null,'1','0') as tipo,
                                date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                                nomeSupermercado,
                                idpublicaOferta,
                                imagemSupermercado
                            from 
                                publicaOferta 
                                inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                                inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                            where 
                                DATEDIFF(dataFinal,'$datahoje') >= 0
                                and DATEDIFF(dataFinal,dataInicial) <= 3
                                and status= 1 
                                group by idpublicaOferta
                                $com_categoria
                                order by qtde_cliques desc
                                ;";   

                        // $resultados = mysqli_query($conexao,$SQLSelect);
                                
                        $operacao = $conexao->prepare($SQLSelect);
                        $pesquisar = $operacao->execute();
                        $resultados = $operacao->fetchAll();


                        if (!empty($resultados)){
                            ?>
                            <div class='col-lg-12'>
                                <h3 class='page-header'>Ofertas do dia (<?=date('d/m/Y')?>)</h3>
                            </div>
                            <?php
                            foreach($resultados as $promocoes){
                                ?>
                                <div class='portfolio-item col-md-4 col-xs-6'>
                                    <img class='blackItem' src='<?=str_replace('../','',$promocoes['imagemSupermercado']);?>' alt=''>
                                    <!--<img class='marcaDagua' src='images/BLACKPRICE80x80.png' alt=''>-->
                                    <img onclick="loadModal('<?=$promocoes['idpublicaOferta']?>')" class='img-responsive img-portfolio img-hover' data-toggle="modal" data-target="#modalProdutos" src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt=''>
                                    <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                    <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                    <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>
                                </div>
                                <?php      
                            }
                        }
                    }
                    catch (PDOException $e)
                    {
                        // caso ocorra uma exceção, exibe na tela
                        echo "Erro!: " . $e->getMessage() . "<br>";
                        die();
                    }

                    $conexao = NULL;
                    
                ?>
            </div>
            <?php
        }
        ?>

        <div id='produtosOferta' class='row'>
            <?php
                


                if(isset($_GET['buscar'])){
                    $query=$_GET['buscar'];
                    $buscarTotal = "and desc_promocao like '%$query%'";
                    $buscarNome = "desc_promocao like '%$query%' and";
                }else{
                    $buscarNome = "";
                    $buscarTotal = "";
                }

                try{

                    $conexao = conn_mysql();  

                    $total = "SELECT sum(status) as totalPromoAtivas from  publicaOferta where status = 1 $buscarTotal
                            and dataFinal >= '$datahoje' 
                            and status= 1 
                            and dataInicial <= '$datahoje'
                            ";   
                    // $resultadoTotal = mysqli_query($conexao,$total);

                    $operacaototal = $conexao->prepare($total);
                    $pesquisarTotal = $operacaototal->execute();
                    $resultadoTotal = $operacaototal->fetchAll();
                        foreach($resultadoTotal as $total){
                            $totalPromoAtivas=$total['totalPromoAtivas'];
                        }

                    $SQLSelect = "
                        SELECT *,
                            if(precoOferta is not null,'1','0') as tipo,
                            date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                            nomeSupermercado,
                            DATEDIFF('$datahoje', dataFinal) as datediff
                        from 
                            publicaOferta 
                            inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                            inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                        where 
                            $buscarNome
                            dataFinal >= '$datahoje' 
                            and status= 1 
                            and dataInicial <= '$datahoje'
                            $com_categoria
                            group by idpublicaOferta
                            order by qtde_cliques,precoOferta
                            ";   

                    // $resultados = mysqli_query($conexao,$SQLSelect);

                    $operacao = $conexao->prepare($SQLSelect);
                    $pesquisar = $operacao->execute();
                    $resultados = $operacao->fetchAll();

                     $SQLSelect = "
                        SELECT 
                            idlistaDeCompras,
                            id_publicaoferta 
                        FROM 
                            `listaDeCompras`
                            inner join rel_listaDeCompras_publicaOferta on id_listadecompras=idlistaDeCompras
                        WHERE 
                            usuario_idusuario=$idUser;";   

                    // $resultadoslista = mysqli_query($conexao,$SQLSelect);

                    $operacaoLista = $conexao->prepare($SQLSelect);      
                    $pesquisarLista = $operacaoLista->execute();
                    $resultadoslista = $operacaoLista->fetchAll();

                    $minhaListaDeCompras = array();
                    
                    if ( !empty($resultadoslista) ){  
                        foreach($resultadoslista as $listaCompras){
                            $minhaListaDeCompras[]=$listaCompras['id_publicaoferta'];
                        }
                    }
                    $promocoes_listadas = array();

                    if ( !empty($resultados) ){
                        ?>

                        <div class='col-lg-12'>
                              <h3 class='page-header'>Promoções <?=$categoria_escolhida['descricaoCategoria']?> (<?=$totalPromoAtivas?>) </h3>
                        </div>

                        <?php
                        
                        //if(!empty($_SESSION['auth'])){
                        foreach($resultados as $promocoes){
                            array_push($promocoes_listadas,$promocoes['idpublicaOferta']);
                            if (in_array($promocoes['idpublicaOferta'], $minhaListaDeCompras)) {
                                $classLista = "fa-star";
                            }else{
                                $classLista = "fa-star-o";
                            }
                            ?>
                            <div class='portfolio-item col-md-4 col-xs-6' >
                                <!--<span title="Adicionar aos meus favoritos" onclick="addtoCart('<?=$promocoes['idpublicaOferta']?>');" class='addProduto'><i id="item_<?=$promocoes['idpublicaOferta']?>" class="fa <?=$classLista?>"></i></span>-->

                                <span class='addProduto'> 
                                <?php
                                if(isset($_GET['buscar'])){
                                ?>
                                    <div class="fb-share-button" data-href="https://www.teoofertas.com.br/share.php?id=<?=$promocoes['idpublicaOferta']?>" data-layout="button" data-size="small" data-mobile-iframe="false"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.teoofertas.com.br/"></a></div>
                                <?php
                                }
                                ?>
                                </span>

                                <img class='blackItem' src='<?=str_replace('../','',$promocoes['imagemSupermercado']);?>' alt=''>
                                <!--<img class='marcaDagua' src='images/BLACKPRICE80x80.png' alt=''>-->


                                    <img onclick="loadModal('<?=$promocoes['idpublicaOferta']?>')" class='img-responsive img-portfolio img-hover' data-toggle="modal" data-target="#modalProdutos" src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt='<?=$promocoes['desc_promocao']?>'>

                                    <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                    <?php
                                        if(!empty($_SESSION['auth'])){ 
                                            ?>
                                            <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?>
                                            </p>
                                            <?php  
                                        } else {
                                            ?>
                                            <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                            <?php  
                                        }
                                    ?>
                                    <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>

                                    <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>

                                    <p>
                                    <?php 
                                    if($promocoes['datediff']==0){ 
                                        ?>
                                        <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'>ACABA HOJE!</div>
                                        <?php 
                                    }else { 
                                        ?>
                                        <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'></div>
                                        <script type='text/javascript'>
                                        $('.contador_<?=$promocoes['idpublicaOferta']?>').countdown('<?=$promocoes['dataFinal']?>', function(event) {
                                            $(this).html(event.strftime('%D dias %H:%M:%S'));
                                        });
                                        </script>        
                                        <?php 
                                    } 
                                    ?>            
                                    </p>
                            </div>
                        <?php      
                        }
                    } else {
                        ?>
                        <div>
                            <p class="msg">Nenhuma oferta encontrada!</p>
                        </div>
                        <?php  
                    }
                }
                catch (PDOException $e)
                {
                    echo "Erro!: " . $e->getMessage() . "<br>";
                    die();
                }

                $conexao = NULL;
            ?>
        </div>

        <!--top 3 de promoções de outras categorias-->
        <?php


            /*if(isset($_GET['cat'])){?>
                <div id='topProduto' class='row'>
                    <div class='col-lg-12'>
                        <h3 class='page-header'>Promoções ativas de outras categorias</h3>
                    </div>
                    <?php
                        //if(!empty($_SESSION['auth'])){
                        $conexao = conn_mysql();    
                        try{
                            $promocoes_in = '';
                            $promocoes_in = implode($promocoes_listadas,',');
                            if(strlen($promocoes_in)>0)
                                $promocoes_in = "and idpublicaOferta not in($promocoes_in)";
                        
                            $SQLSelect = "
                                SELECT *,
                                    if(precoOferta is not null,'1','0') as tipo,
                                    date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                                    nomeSupermercado
                                from 
                                    publicaOferta 
                                    inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                                    inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                                where 
                                    dataFinal > '$datahoje' 
                                    and status= 1 
                                    and dataInicial <= '$datahoje'
                                    $promocoes_in
                                    group by idpublicaOferta
                                    limit 3;";   

                            $operacao = $conexao->prepare($SQLSelect);
                            $pesquisar = $operacao->execute();
                            $resultados = $operacao->fetchAll();
                            
                            if (count($resultados)>0){
                                foreach($resultados as $promocoes){
                                    if (in_array($promocoes['idpublicaOferta'], $minhaListaDeCompras)) {
                                        $classLista = "fa-star";
                                    }else
                                        $classLista = "fa-star-o";
                                ?>
                                    <div class='portfolio-item col-md-4 col-xs-6 $hidden' >
                                        <span title="Adicionar aos meus favoritos" onclick="addtoCart('<?=$promocoes['idpublicaOferta']?>');" class='addProduto'><i id="item_<?=$promocoes['idpublicaOferta']?>" class="fa <?=$classLista?>"></i></span>
                                        <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                                        <a href="share.php?cat=<?=$promocoes['id_categoria']?>&id=<?=$promocoes['idpublicaOferta']?>" style="outline:none;">
                                            <img class='img-responsive img-portfolio img-hover' src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt=''>
                                            <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                            <?php
                                                if(!empty($_SESSION['auth'])){?>
                                                    <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                            <?  }
                                                else{?>
                                                    <p class='productPrice'>Faça seu login.</p>
                                            <?  }?>
                                            <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>

                                            <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>

                                            <p>
                                                <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'></div>
                                                <script type='text/javascript'>
                                                $('.contador_<?=$promocoes['idpublicaOferta']?>').countdown('<?=$promocoes['dataFinal']?>', function(event) {
                                                    $(this).html(event.strftime('%D dias %H:%M:%S'));
                                                });
                                                </script>                    
                                            </p>

                                        </a>
                                    </div>
                        <?      
                                }
                            }
                            else{?>
                                <div>
                                    <p class="msg">No momento, não há promoções ativas para a categoria escolhida.</p>
                                </div>
                        <?  }
                        }
                        catch (PDOException $e)
                        {
                            // caso ocorra uma exceção, exibe na tela
                            echo "Erro!: " . $e->getMessage() . "<br>";
                            die();
                        }

                        $conexao = NULL;*/
                        /*}
                        else{?>
                            <div>
                                <p class="msg">É necessário efetuar o login para visualizar as promoções.</p>
                            </div>
                    <?  }*/
                   /* ?>
                </div>
        <? }*/?>

            <div>
                <p class="msg">Atenção! O Teó Ofertas foi desenvolvido para ajudar você consumidor a encontrar mais facilmente as orfertas disponíveis nos supermercados de Teófilo Otoni e região. As ofertas possuem data de expiração ou até durar o estoque, as imagens contidas no sistema são meramente ilustrativas. Em caso de dúvidas entre em contato conosco: siamelsoftwares@gmail.com</p>
                <p><a href="http://www.siamelsoftwares.com.br/contato/"><img src="images/logo.png" alt="SiamelSoftware"></a></p>
            </div>

      <hr>

    <?php  include('footer.php'); ?>

    </div>
    <!-- /.container -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>    

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })

        $( "#sup" ).change(function() {
          var sup = $( "#sup" ).val();

          $('#loadingGif').fadeIn();
          $('#topProduto').fadeOut();
          //$('#produtosOferta').load('loadOfertas.php?sup='+sup);

            $( "#produtosOferta" ).load('loadOfertas.php?sup='+sup, function() {
              $('#loadingGif').fadeOut();
            });

        });

        function loadModal(id){
            $("#bodyModal").load('verProdutos.php?id='+id);
        }

    </script>
        
</div>

</body>

</html>
