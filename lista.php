<?php

session_start();
$idUser = $_SESSION['idUser'];

include('conf/confbd.php');

?>

<!DOCTYPE html>
<html lang="pt-br">

<?php include('head.php'); ?>

<script>
  fbq('track', 'AddToCart');
</script>


<body>
	<?php include('navbar.php'); ?>

  
    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Minha lista de compras</h2>
            </div>

        <?php 

            $datahoje = date('Y-m-d');


            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{
                $SQLSelect = "SELECT idlistaDeCompras from listaDeCompras where usuario_idusuario=$idUser order by idlistaDeCompras desc limit 1;";   

                $operacao = $conexao->prepare($SQLSelect);      
                $pesquisar = $operacao->execute();
                $resultados = $operacao->fetchAll();

                if (count($resultados)>0){  
                    foreach($resultados as $dadosEncontrados){
                        $idlistaDeCompras=$dadosEncontrados['idlistaDeCompras'];
                        }
                }
            }
            catch (PDOException $e)
            {
                echo "Erro!: " . $e->getMessage() . "<br>";
                die();
            }                           

            try{

            $i=1;
            try{
                $SQLSelect = "SELECT * FROM `atualizacao`;"; 
                $operacao = $conexao->prepare($SQLSelect);     
                $pesquisar = $operacao->execute();
                $resultados = $operacao->fetchAll();
                $count = $operacao->rowCount();
                if (count($resultados)>0){  
                                foreach($resultados as $dadosEncontrados){
                                    $dataAtualizacao=$dadosEncontrados['dataAtualizacao'];
                                    $idsupermercado=$dadosEncontrados['supermercado_idsupermercado'];

                                    if($i<$count){
                                        $atualizacao .="(idsupermercado=$idsupermercado and dataAtualizacao='$dataAtualizacao') or ";
                                    }
                                    else{
                                        $atualizacao .="(idsupermercado=$idsupermercado and dataAtualizacao='$dataAtualizacao')";
                                    }   
                                    $i++;
                                }
                }
            } //try
            catch (PDOException $e)
            {
            // caso ocorra uma exceção, exibe na tela
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
            }


            // instrução SQL básica 
            /*$SQLSelect = "SELECT status,idpublicaOferta,idproduto,precoOferta,dataInicial,dataFinal,date_format(dataFinal,'%Y/%m/%d') as dataTermino,barras,descricaoProduto,imagemProduto,nomeSupermercado FROM `publicaOferta`,`produto`,`supermercado` WHERE supermercado_idsupermercado=supermercado.idsupermercado and produto_idproduto=produto.idproduto and dataFinal >= '$datahoje' and status= 1 and dataInicial <= '$datahoje' order by precoOferta,dataFinal;";*/

            $SQLSelect="SELECT *,DATEDIFF(dataFinal,'$datahoje') AS promoValida, if(precoOferta is not null,'1','0') as tipo,date_format(dataFinal,'%Y/%m/%d') as dataTermino from produto

            inner join supermercado_tem_produto on produto.idproduto=supermercado_tem_produto.produto_idproduto

            inner join supermercado on supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado

            left join publicaOferta on produto.idproduto=publicaOferta.produto_idproduto and publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado

            left JOIN listaDeCompras_em_supermercado on listaDeCompras_em_supermercado.idsupermercado_tem_produto=supermercado_tem_produto.idsupermercado_tem_produto

            where idlistaDeCompras=$idlistaDeCompras order by nomeSupermercado,precoOferta,dataFinal;";

        

            //echo $SQLSelect;

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                $existeProdutos=1;
                foreach($resultados as $dadosEncontrados) {
                    $id=$dadosEncontrados['produto_idproduto'];
                    $barras = $dadosEncontrados['barras'];
                    $descricaoProduto = $dadosEncontrados['descricaoProduto'];
                    $imagemProduto = $dadosEncontrados['imagemProduto'];
                    $idProduto = $dadosEncontrados['idsupermercado_tem_produto'];
                    $precoOferta = $dadosEncontrados['precoOferta'];
                    $precoProduto = $dadosEncontrados['precoProduto'];
                    $promoValida = $dadosEncontrados['promoValida'];
                    $tipo = $dadosEncontrados['tipo'];
                    $idsupermercado = $dadosEncontrados['idsupermercado'];

                    $listaIdProdutos[]=$idProduto;

                    if($promoValida==0 && $tipo==1) { 
                            $SQLDelete = "DELETE FROM `listaDeCompras_em_supermercado` where idlistaDeCompras=$idlistaDeCompras and idsupermercado_tem_produto=$idProduto;";
                            $operacao = $conexao->prepare($SQLDelete);                    
                            $delete = $operacao->execute();


                            $SQLDelete = "DELETE FROM `publicaOferta` where produto_idproduto=$id and supermercado_idsupermercado=$idsupermercado;";
                            $operacao = $conexao->prepare($SQLDelete);                    
                            $delete = $operacao->execute();


                            $SQLSelect="SELECT idsupermercado_tem_produto from supermercado_tem_produto where produto_idproduto=$id order by precoProduto limit 1;";

                            $operacao = $conexao->prepare($SQLSelect);      
                            $pesquisar = $operacao->execute();
                            $resultados = $operacao->fetchAll();
                            // se há resultados, os escreve em uma tabela
                            if (count($resultados)>0){  
                                foreach($resultados as $dadosEncontrados) {
                                       $idsupermercado_tem_produto=$dadosEncontrados['idsupermercado_tem_produto'];
                                }
                                $SQLInsert = 'INSERT INTO `listaDeCompras_em_supermercado` (`idlistaDeCompras`,`idsupermercado_tem_produto`) VALUES (?,?)';   
                                $operacao = $conexao->prepare($SQLInsert);                    
                                $inserir = $operacao->execute(array($idlistaDeCompras,$idsupermercado_tem_produto));     
                            }

                            $tipo=0;   

                            //echo "<script>location.href='lista.php';</script>";  
                            //exit;
                    }elseif($promoValida==NULL){
                        $tipo=0;   
                    }
                    else{
                        $precoProduto = $dadosEncontrados['precoOferta'];
                        $tipo=1;
                    }

                    $dataFinal = $dadosEncontrados['dataTermino'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $status = $dadosEncontrados['status'];
                    $precoProduto = number_format($precoProduto,2,",",".");

                    /*
					if (!getimagesize($imagemProduto)) {
					    $imagemProduto = 'images/notfound.png';
					} 
					*/
                    $arrayNomeSupermercado=explode(" ",$nomeSupermercado);
                    $nomeSupermercado=$arrayNomeSupermercado[0];

                    if(!empty($_SESSION['auth'])){
                        if($tipo==0) {
                                echo "
                                    <div class='portfolio-item col-md-4 col-sm-6' >
                                        <span onclick='addtoCart(\"$idProduto\")'; class='addProduto'><i id='item_$idProduto' class='fa fa-trash'></i></span>
                                        <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                                        <a href='portfolio-item.php?idproduto=$idProduto'>
                                            <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                            <span class='productDescription'>".substr($descricaoProduto, 0,38)."</span>
                                            <p class='productPrice'>R$ $precoProduto ($nomeSupermercado)</p>
                                            <p class='productSupermercado'>$nomeSupermercado</p>

                                        </a>
                                    </div>
                                   ";    
                        }            
                        else{
                                echo "
                                <div class='portfolio-item col-md-4 col-sm-6' >
                                    <span onclick='addtoCart(\"$idProduto\")'; class='addProduto'><i id='item_$idProduto' class='fa fa-trash'></i></span>
                                    <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                                    <a href='portfolio-item.php?idproduto=$idProduto'>
                                        <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                        <span class='productDescription'>".substr($descricaoProduto, 0,38)."</span>
                                        <p class='productPrice'>R$ $precoProduto ($nomeSupermercado)</p>
                                        <p class='productSupermercado'>$nomeSupermercado</p>


                                        <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>

                                        <p>
                                            <div id='contador' class='contador_$idProduto'></div>
                                            <script type='text/javascript'>
                                              $('.contador_$idProduto').countdown('$dataFinal', function(event) {
                                                $(this).html(event.strftime('%D dias %H:%M:%S'));
                                              });
                                            </script>                    
                                        </p>


                                    </a>
                                </div>
                               ";    

                        }
                    }
                }
            }
            else{
                $existeProdutos=0;
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $l=0;
        $qtdeProdutosEmLista=sizeof($listaIdProdutos);
        foreach ($listaIdProdutos as $idProduto) {
            $l++;
            if($l<$qtdeProdutosEmLista){
                $produtos.="`idsupermercado_tem_produto` = $idProduto or ";
            }else{
                $produtos.="`idsupermercado_tem_produto` = $idProduto";
            }

        }

        try{

            // Total da lista
             $SQLSelect = "SELECT *, if(precoOferta is not null,'1','0') as tipo from produto inner join supermercado_tem_produto on produto.idproduto=supermercado_tem_produto.produto_idproduto inner join supermercado on supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado left join publicaOferta on produto.idproduto=publicaOferta.produto_idproduto and publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado where ($produtos)";   

            //$SQLSelect = "SELECT sum(precoProduto) as totalLista FROM `supermercado_tem_produto` WHERE ($produtos);";   
           // echo $SQLSelect;

            $operacao = $conexao->prepare($SQLSelect);      
            $pesquisar = $operacao->execute();
            $resultados = $operacao->fetchAll();

            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $tipo = $dadosEncontrados['tipo'];

                    // Se é promoção
                    if($tipo==1){
                        $dadosEncontrados['precoProduto']=$dadosEncontrados['precoOferta'];
                    }
                    $totalLista+=$dadosEncontrados['precoProduto'];
                    }
            }
        }
        catch (PDOException $e)
        {
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
        }  

        try{
            $SQLSelect = "SELECT produto_idproduto FROM `supermercado_tem_produto` WHERE ($produtos);";   
            //echo $SQLSelect;

            $operacao = $conexao->prepare($SQLSelect);      
            $pesquisar = $operacao->execute();
            $resultados = $operacao->fetchAll();

            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                        $arrayIdProdutos[]=$dadosEncontrados['produto_idproduto'];
                    }
            }
        }
        catch (PDOException $e)
        {
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
        }  

        //print_r($arrayIdProdutos);

        $arrayIdProdutos = array_filter($arrayIdProdutos);

        $l=0;
        $qtdeProdutosEmArray=sizeof($arrayIdProdutos);
        foreach ($arrayIdProdutos as $idProduto) {          
            $l++;
            if($l<$qtdeProdutosEmArray){
                $idprodutos.="`idproduto` = $idProduto or ";
            }else{
                $idprodutos.="`idproduto` = $idProduto";
            }

        }        


        try{


            $SQLSelect = "SELECT idsupermercado FROM `supermercado` WHERE lixeira=0;";   
           // echo $SQLSelect;

            $operacao = $conexao->prepare($SQLSelect);      
            $pesquisar = $operacao->execute();
            $resultados = $operacao->fetchAll();

            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                        $idsupermercado=$dadosEncontrados['idsupermercado'];

                        try{
                        //$SQLSelect = "SELECT *, if(precoOferta is not null,'1','0') as tipo from produto inner join supermercado_tem_produto on produto.idproduto=supermercado_tem_produto.produto_idproduto inner join supermercado on supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado left join publicaOferta on produto.idproduto=publicaOferta.produto_idproduto and publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado WHERE ($idprodutos) and supermercado.idsupermercado=supermercado_tem_produto.supermercado_idsupermercado and supermercado_tem_produto.supermercado_idsupermercado=$idsupermercado and atualizacao.supermercado_idsupermercado=supermercado.idsupermercado and supermercado_tem_produto.dataAtualizacao=atualizacao.dataAtualizacao;";

                        $SQLSelect = "SELECT *, if(precoOferta is not null,'1','0') as tipo from produto inner join supermercado_tem_produto on produto.idproduto=supermercado_tem_produto.produto_idproduto inner join supermercado on supermercado_tem_produto.supermercado_idsupermercado=supermercado.idsupermercado left join publicaOferta on produto.idproduto=publicaOferta.produto_idproduto and publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado left join atualizacao on  atualizacao.supermercado_idsupermercado=supermercado.idsupermercado WHERE ($idprodutos) and supermercado.idsupermercado=supermercado_tem_produto.supermercado_idsupermercado and supermercado_tem_produto.supermercado_idsupermercado=$idsupermercado  and supermercado_tem_produto.dataAtualizacao=atualizacao.dataAtualizacao;";

                       // echo $SQLSelect;

                            $operacao = $conexao->prepare($SQLSelect);      
                            $pesquisar = $operacao->execute();
                            $resultados = $operacao->fetchAll();

                            if (count($resultados)>0){  
                                foreach($resultados as $dadosEncontrados){
                                    $tipo = $dadosEncontrados['tipo'];

                                    // Se é promoção
                                    if($tipo==1){
                                        $dadosEncontrados['precoProduto']=$dadosEncontrados['precoOferta'];
                                    }
                                    $total+=$dadosEncontrados['precoProduto'];

                                    $sup=$dadosEncontrados['nomeSupermercado'];  
                                    $idsupermercado=$dadosEncontrados['idsupermercado'];
                                    }

                            }
                        }
                        catch (PDOException $e)
                        {
                            echo "Erro!: " . $e->getMessage() . "<br>";
                            die();
                        }  
                        
                        $idsup[$sup]=$idsupermercado;
                        $arrayTotais[$sup]=$total;  
                        $total=0;

                       // print_r($arrayTotais);
                    }
            }
        }
        catch (PDOException $e)
        {
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
        }  

        $totais=asort($arrayTotais);

        foreach($arrayTotais as $supMenorPreco => $totalCompra) {
            $sup=$supMenorPreco;
            $total=$totalCompra;
            break;        
        }

        $idSupMenorPreco=$idsup[$sup];
       
        if($totalLista<$total){
            $diff=$total-$totalLista;
            $diff = number_format($diff,2,",",".");
          //  $frase="Comprando todos os produtos da forma que escolheu, você irá economizar R$ $diff";
        }
        elseif($total<$totalLista){
             $diff=$totalLista-$total;
            $diff = number_format($diff,2,",",".");
           // $frase="Comprando todos os produtos no supermercado $sup, você irá economizar R$ $diff";
        }
        else{
          //  $frase="";
        }

       // echo $totalLista . ' ' . $total;

    ?> 
        </div>
        <!-- /.row -->

        <hr>



        <?php if($existeProdutos==1){

            echo "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal'>Quero uma compra única</button>";
            
            }else{
             
             echo "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#myModal' disabled>Quero uma compra única</button>";
               
             } ?>
        

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Quero uma compra única</h3>
              </div>
              <div class="modal-body">
                <?php

                    try{

                    echo "<h4>Sua compra ficaria mais barata comprando no <strong>$sup</strong></h4>";

                    $SQLSelect="SELECT * from supermercado where idsupermercado=$idSupMenorPreco;";

                    //prepara a execução da sentença
                    $operacao = $conexao->prepare($SQLSelect);      
                            
                    $pesquisar = $operacao->execute();
                    
                    //captura TODOS os resultados obtidos
                    $resultados = $operacao->fetchAll();

                    // se há resultados, os escreve em uma tabela
                        if (count($resultados)>0){  
                            foreach($resultados as $dadosEncontrados){
                                $idsupermercado = $dadosEncontrados['idsupermercado'];
                                $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                                $imagemSupermercado = $dadosEncontrados['imagemSupermercado'];
                                $lat = $dadosEncontrados['lat'];
                                $lgt = $dadosEncontrados['lgt'];
                                $telefone = $dadosEncontrados['telefone'];
                                $endereco = $dadosEncontrados['endereco'];
                                $horarioFuncionamento = $dadosEncontrados['horarioFuncionamento'];         
                            }

                        }
                        else{
                            echo'<div class="starter-template">';
                            echo'</div>';
                        }
                    } //try
                    catch (PDOException $e)
                    {
                    // caso ocorra uma exceção, exibe na tela
                    echo "Erro!: " . $e->getMessage() . "<br>";
                    die();
                    }

                    $conexao = NULL;

                    echo "
                            <div class='row'>
                                <!-- Map Column -->
                                <div class='col-md-8'>
                                    <!-- Embedded Google Map -->
                                    <iframe width='100%' height='400px' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://maps.google.com/maps?hl=pt-br&amp;ie=UTF8&amp;ll=$lat,$lgt&amp;t=m&amp;z=16&amp;output=embed'></iframe>
                                </div>
                                <!-- Contact Details Column -->
                                <div class='col-md-4'>
                                    <h3>$nomeSupermercado</h3>
                                    <p>
                                        $endereco<br>
                                    </p>
                                    <p><i class='fa fa-phone'></i> 
                                        <abbr title='Phone'>Telefone</abbr>: $telefone</p>
                                    <p><i class='fa fa-clock-o'></i> 
                                        <abbr title='Hours'>Aberto</abbr>: $horarioFuncionamento</p>
                                    <p><img class='img-responsive' style='margin:0 auto;width:60%;' src='images/BLACKPRICE.png' alt=''></p>
                                </div>
                            </div>
                          
                    ";

                ?>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Entendi</button>
              </div>
            </div>

          </div>
        </div>



        <!-- Footer -->
    <?php  include('footer.php'); ?>

    </div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>



</body>

</html>
