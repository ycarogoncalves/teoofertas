<?php

session_start();

require_once("authSession.php");

require_once("conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Alterar senha pessoal</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="update_senha.php">

<div class="form-group">
  <label for="nome">Senha atual</label>
  <input type="text" class="form-control" name="senhaAtual" id="senhaAtual" placeholder="Senha atual">
</div>


<div class="form-group">
    <label for="senha1">Nova senha</label>
    <input type="password" class="form-control" name="senha1" id="senha1" placeholder="Nova senha">
</div>

<div class="form-group">
    <label for="senha2">Confirmar senha</label>
    <input type="password" class="form-control" name="senha2" id="senha2" placeholder="Confirmar senha">
</div>


  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>


  

</div>

  <?php  include('footer.php'); ?>
  
<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
         
       }
    });
});


</script>


  </body>
</html>
