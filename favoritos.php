<?php

session_start();
$idUser = $_SESSION['idUser'];

include('conf/confbd.php');

?>

<!DOCTYPE html>
<html lang="pt-br">

<?php include('head.php'); ?>

<body>
	<?php include('navbar.php'); ?>

  
    <!-- Page Content -->
    <div class="container">

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Meus favoritos</h2>
            </div>

            <?php 

                $datahoje = date('Y-m-d');


                // instancia objeto PDO, conectando no mysql
                // $conexao = conn_mysql();    

                try{
                    $SQLSelect = "SELECT idlistaDeCompras from listaDeCompras where usuario_idusuario=$idUser order by idlistaDeCompras desc limit 1;";   

                    $resultados = mysqli_query($conexao,$SQLSelect);

                    // $operacao = $conexao->prepare($SQLSelect);      
                    // $pesquisar = $operacao->execute();
                    // $resultados = $operacao->fetchAll();

                    if ( !empty($resultados) ){  
                        foreach($resultados as $dadosEncontrados){
                            $idlistaDeCompras=$dadosEncontrados['idlistaDeCompras'];
                        }
                        /*deletando todos os favoritos em que as ofertas ja encerraram*/
                        $SQLSelect = "SELECT *from rel_listaDeCompras_publicaOferta where id_listadecompras=".$idlistaDeCompras.";";  

                        $resultadosBusca = mysqli_query($conexao,$SQLSelect);

                        // $operacaoBusca = $conexao->prepare($SQLSelect);      
                        // $pesquisarBusca = $operacaoBusca->execute();
                        // $resultadosBusca = $operacaoBusca->fetchAll();
                        
                        if ( !empty($resultadosBusca) ){ 
                            foreach($resultadosBusca as $result){
                                $SQLSelect = "SELECT *from publicaOferta where idpublicaOferta=".$result['id_publicaoferta']." and status=0;";   
                                $resultadosBusca2 = mysqli_query($conexao,$SQLSelect);
                                // $operacaoBusca2 = $conexao->prepare($SQLSelect);      
                                // $pesquisarBusca2 = $operacaoBusca2->execute();
                                // $resultadosBusca2 = $operacaoBusca2->fetchAll();
                                
                                if ( !empty($resultadosBusca2) ){
                                    $SQLDelete = "delete from `rel_listaDeCompras_publicaOferta` where id=".$result['id'].";";

                                    $delete = mysqli_query($conexao,$SQLSelect);
                                    
                                    // $operacaoDel = $conexao->prepare($SQLDelete);                    
                                    // $delete = $operacaoDel->execute();
                                }
                            }
                        }
                        /*fim*/
                    }
                }
                catch (PDOException $e)
                {
                    echo "Erro!: " . $e->getMessage() . "<br>";
                    die();
                } 
                                        
                
                $SQLSelect = "
                    SELECT 
                        publicaOferta.*,
                        supermercado.*
                    from 
                        listaDeCompras 
                        inner join rel_listaDeCompras_publicaOferta on id_listadecompras=idlistaDeCompras
                        inner join publicaOferta on id_publicaoferta=idpublicaOferta
                        inner join supermercado on idsupermercado=supermercado_idsupermercado
                    where 
                        listaDeCompras.usuario_idusuario=$idUser 
                        order by idlistaDeCompras desc;";   

                $operacao = $conexao->prepare($SQLSelect);      
                $pesquisar = $operacao->execute();
                $resultados = $operacao->fetchAll();

                $conexao = null;

                $conexao = conn_mysql();   
                
                $existeProdutos=0;
                if(count($resultados)>0){
                    $existeProdutos=1;
                    foreach($resultados as $promocoes){?>
                        <div class="blocos_favoritos">
                            <div class='portfolio-item col-md-4 col-sm-6' >
                                <span title="Adicionar aos meus favoritos" onclick="addtoCart('<?=$promocoes['idpublicaOferta']?>');" class='addProduto'><i id="item_<?=$promocoes['idpublicaOferta']?>" class="fa fa-star"></i></span>
                                <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                                <a href='portfolio-item.php?cat=<?=$categoria?>&id=<?=$promocoes['idpublicaOferta']?>' style="outline:none;">
                                    <img class='img-responsive img-portfolio img-hover' src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt=''>
                                    <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                    <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                    <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>

                                    <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>

                                    <p>
                                        <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'></div>
                                        <script type='text/javascript'>
                                        $('.contador_<?=$promocoes['idpublicaOferta']?>').countdown('<?=$promocoes['dataFinal']?>', function(event) {
                                            $(this).html(event.strftime('%D dias %H:%M:%S'));
                                        });
                                        </script>                    
                                    </p>

                                </a>
                            </div>
                            <br />
                            <button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target="#myModal<?=$promocoes['idpublicaOferta']?>">Eu quero</button>
                        </div>
                        <!-- Modal -->
                        <div id="myModal<?=$promocoes['idpublicaOferta']?>" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h3 class="modal-title">Eu quero</h3>
                                </div>
                                <div class="modal-body">
                                    <?php   
                                        $idsupermercado = $promocoes['idsupermercado'];
                                        $nomeSupermercado = $promocoes['nomeSupermercado'];
                                        $imagemSupermercado = $promocoes['imagemSupermercado'];
                                        $lat = $promocoes['lat'];
                                        $lgt = $promocoes['lgt'];
                                        $telefone = $promocoes['telefone'];
                                        $endereco = $promocoes['endereco'];
                                        $horarioFuncionamento = $promocoes['horarioFuncionamento'];         
                                    
                                        ?>
                                        <div class='row'>
                                            <!-- Map Column -->
                                            <div class='col-md-8'>
                                                <!-- Embedded Google Map -->
                                                <iframe width='100%' height='400px' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='http://maps.google.com/maps?hl=pt-br&amp;ie=UTF8&amp;ll=<?=$lat?>,<?=$lgt?>&amp;t=m&amp;z=16&amp;output=embed'></iframe>
                                            </div>
                                            <!-- Contact Details Column -->
                                            <div class='col-md-4'>
                                                <h3><?=$nomeSupermercado?></h3>
                                                <p>
                                                    <?=$endereco?><br>
                                                </p>
                                                <p><i class='fa fa-phone'></i> 
                                                    <abbr title='Phone'>Telefone</abbr>: <?=$telefone?></p>
                                                <p><i class='fa fa-clock-o'></i> 
                                                    <abbr title='Hours'>Aberto</abbr>: <?=$horarioFuncionamento?></p>
                                                <p><img class='img-responsive' style='margin:0 auto;width:60%;' src='images/BLACKPRICE.png' alt=''></p>
                                            </div>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendi</button>
                                </div>
                                </div>

                            </div>
                        </div>
                <?
                    }
                    $conexao = NULL;
                }
                ?>
        </div>
        <!-- /.row -->

        <hr>  
        


        <!-- Footer -->

    <?php  include('footer.php'); ?>

    </div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
