<?php

session_start();

include('conf/confbd.php');

?>
       <?php 
            
            if(isset($_GET['id'])){
                $id = $_GET['id'];
                $SQLSelect = "
                   SELECT 
                        publicaOferta.*,
                        date_format(dataFinal,'%d/%m/%Y') as dataFinalformat,
                        nomeSupermercado,
                        idsupermercado,
                        id_categoria,
                        imagemSupermercado,
                        mapa
                    FROM 
                        `publicaOferta`
                        inner join supermercado on idsupermercado=supermercado_idsupermercado 
                        inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                    WHERE 
                        status= 1 
                        and idpublicaOferta = $id;"; 
                        $oferta=1;
               
            }
            elseif(isset($_GET['idproduto'])){
                $id = $_GET['idproduto'];
                $SQLSelect = "SELECT *,precoProduto as preco from produto p 
                                right join supermercado_tem_produto sp on p.idproduto=sp.produto_idproduto 
                                left join publicaOferta po on p.idproduto=po.produto_idproduto
                                where  idsupermercado_tem_produto = $id;";  
                                $oferta=0;               
            }

            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $idProdutoGraph = $dadosEncontrados['idpublicaOferta'];
                    $idSupGraph = $dadosEncontrados['idsupermercado'];
                    $descricaoProduto = $dadosEncontrados['desc_promocao'];
                    $imagemProduto = $dadosEncontrados['banner_promocao'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $precoOferta =  $dadosEncontrados['precoOferta'];
                    $precoOferta = number_format($precoOferta,2,",",".");
                    $idproduto = $dadosEncontrados['idpublicaOferta'];
                    $mapa = $dadosEncontrados['mapa'];
                    $id_categoria = $dadosEncontrados['id_categoria'];
                    $imagemSupermercado=$dadosEncontrados['imagemSupermercado'];
                    $dataFinalformat=$dadosEncontrados['dataFinalformat'];
                    /*
                    if (!getimagesize($imagemProduto)) {
                        $imagemProduto = 'images/notfound.png';
                    } 
                    */
                }
                $SQLDelete = "update `publicaOferta` set qtde_cliques=qtde_cliques+1 where idpublicaOferta = $idproduto;";
                $operacao = $conexao->prepare($SQLDelete);                    
                $delete = $operacao->execute();
                
            }
            else{
                echo'<div class="starter-template">';
                echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
    ?> 

<script type="text/javascript">
    window.location.href="index.php";
</script>
    
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:url"           content="https://www.teoofertas.com.br/portfolio-item.php?id=<?=$id?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Encontrei no Teó Ofertas! 😍" />
    <meta property="og:description"   content="<?=$descricaoProduto?> em oferta no <?=$nomeSupermercado?> por apenas R$ <?=$precoOferta?>. A oferta válida até <?=$dataFinalformat?> ou enquanto durar o estoque. Corre lá!" />
    <meta property="og:image"         content="https://www.teoofertas.com.br/images/<?=$imagemProduto?>" />
    <meta property="fb:app_id"        content="174842606397662" />

    <title>Ofertas e Promoções em Teófilo Otoni</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/simple-sidebar.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blackprice.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-79233750-1', 'auto');
      ga('send', 'pageview');

    </script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '217974368734378');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=217974368734378&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- share Facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- share Facebook   -->

<script src="js/jquery.js" type="text/javascript"></script>
    
</head>

<script>
  fbq('track', 'ViewContent');
</script>

<body>
    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Item Row -->
        <div class="row">
            <div class="col-md-3">
                <img class="img-responsive imagemDestaqueProduto" src="<?=str_replace('../','',$imagemProduto); ?>" alt="">
            </div>
            <div class="col-md-3 col-xs-12">
                <h3><?php echo $descricaoProduto; ?></h3>
                <p class="productSupermercado"><?php echo $nomeSupermercado; ?></p>
                <p class="productPrice">R$ <?php echo $precoOferta; ?>
                </p>
                <div class="col-md-4 col-xs-4">
                    <img class="img-responsive imagemDestaqueMarca" src="<?=$imagemSupermercado?>" alt="">
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <?
                    if(strlen($mapa)>0){?>
                        <div class="row">
                             <?=$mapa; ?>
                            <!-- /.row -->
                        </div>
                <? }?>
            </div>            
        </div>
    </div>
    <!-- /.container -->
</body>
</html>
