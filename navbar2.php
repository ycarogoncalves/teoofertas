 <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!--
                <a href="#menu-toggle" class="btn button_menu" aria-label="Left Align" id="menu-toggle">
                    <span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span>
                </a>-->

            <a class="navbar-brand" href="index.php"><img src='images/BLACKPRICE80x80.png'></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="row">
                    <div id="searchBar" class="col-md-7 col-sm-7 col-xs-7">
                        <form class="navbar-form" role="search" method="GET" id="search-form" name="search-form" action="index.php">
                            <div id="containerSearch" class="input-group">
                                <?php
                                if( !isset($_SESSION['pesquisa']) ){
                                    $_SESSION['pesquisa'] = '';
                                }
                                ?>
                                <input autocomplete="off" type="text" class="form-control" placeholder="BUSCAR OFERTAS" list="produto" name="buscar" id="buscar"  value="<?php echo $_SESSION['pesquisa']; ?>">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <div class="input-group-btn add-produto">
                            <button type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-1">
                        <?php 
                        if(!empty($_SESSION['auth'])){
                            ?>
                            <ul class="nav navbar-nav navbar-right login">
                                <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"> </span> <span class="menuAdmin"><?php $arrayNome = explode(" ",  $_SESSION['nomeUsuario']); $nomeUsuario = $arrayNome[0]; echo $nomeUsuario;?></span><span class="caret"></span></a>
                                  <ul class="dropdown-menu" role="menu">
                                    <li><a href="favoritos.php"><span class="glyphicon glyphicon-list"></span>  Meus Favoritos</a></li>
                                    <li><a href="alteraSenha.php"><span class="glyphicon glyphicon-pencil"></span>  Alterar senha</a></li>
                                    <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                                  </ul>
                                </li>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <ul class="nav navbar-nav navbar-right login">
                                <li>
                                    <a href="#" role="button" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-user"></span> Entrar</a>
                                </li>
                            </ul>
                            <?php
                        }
                        ?>
                    </div>
                </div> 
            </div>

            

            
            </div>

        <!-- /.container -->
    </nav>

    <?php
        require_once("conf/confbd.php");

        $conexao = conn_mysql();
		$sql = "select * from categoria where lixeira=0 order by descricaoCategoria asc";
        // $resultado_cats = mysqli_query($conexao,$sql);
		$operacao = $conexao->prepare($sql);
		$pesquisar = $operacao->execute();
		$resultado_cats = $operacao->fetchAll();

        $conexao = null;
    ?>

    <!--
    <div id="wrapper">
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <span>Navegação</span>
                </li>
                <?php
                    // if(count($resultado_cats)>0){
                        // foreach($resultado_cats as $categoria){?>
                            <li>
                                <a href="index.php?cat=<?=$categoria['idcategoria']?>">
                                    <i class="fa <?=$categoria['icone']?>" aria-hidden="true"></i>
                                    <?=$categoria['descricaoCategoria']?>
                                </a>
                            </li>
                    <?  }
                    }?>
            </ul>
        </div>
    </div>
    -->
