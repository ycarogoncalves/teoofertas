<?php
session_start();

if($_SESSION['tipo']==1 ||$_SESSION['tipo']==2){
	
   $_SESSION = array();  //Limpa o vetor de sessão
   session_destroy();		// Destruímos a sessão em si

   header("Location:./acessoNegado.php");
  die();	
}

if(empty($_SESSION['auth'])||($_SESSION['auth']!=true)){

     echo '<script language="javascript">';
     echo utf8_decode('alert("Realize login para acessar o sistema.")');
     echo '</script>';
     $href = '<script language="javascript">location.href="index.php";</script>';
     echo $href;        

  die();
 }
?>