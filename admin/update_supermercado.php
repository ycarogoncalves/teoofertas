<?php 

session_start();
$idUser = $_SESSION['idUser'];

    require_once("../conf/confbd.php");

    $idsupermercado= htmlspecialchars($_POST ["idsupermercado"]);
    $nome= htmlspecialchars($_POST ["nome"]);
    $endereco= htmlspecialchars($_POST ["endereco"]);
    $telefones= htmlspecialchars($_POST ["telefones"]);
    $horarioFuncionamento= htmlspecialchars($_POST ["horarioFuncionamento"]);
    $software= htmlspecialchars($_POST ["software"]);
    $lat= htmlspecialchars($_POST ["lat"]);
    $lgt= htmlspecialchars($_POST ["lgt"]);

        $conexao = conn_mysql();  

      if(!empty($_FILES['imagemSupermercado']['name'])){

        $uploaddir = 'images/supermercados/';

        $Imagem = $_FILES['imagemSupermercado']['name'];

        $tipo = explode(".", $_FILES['imagemSupermercado']['name']);

        $uploadfile = $uploaddir . md5($Imagem) . '.' . $tipo[1];

        if (move_uploaded_file($_FILES['imagemSupermercado']['tmp_name'], $uploadfile)) {
          $SQLUpdate = "UPDATE `supermercado` set nomeSupermercado='$nome',imagemSupermercado='$uploadfile',softwareUtilizado='$software',lat='$lat',lgt='$lgt',telefone='$telefones',endereco='$endereco',horarioFuncionamento='$horarioFuncionamento' where idsupermercado='$idsupermercado';";

                $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
                $operacao = $conexao->prepare($SQLLogs);            
                $inserirLog = $operacao->execute(array($SQLUpdate,$idUser));        
        }

      }
      else{
          $SQLUpdate = "UPDATE `supermercado` set nomeSupermercado='$nome',softwareUtilizado='$software',lat='$lat',lgt='$lgt',telefone='$telefones',endereco='$endereco',horarioFuncionamento='$horarioFuncionamento' where idsupermercado='$idsupermercado';";

                $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
                $operacao = $conexao->prepare($SQLLogs);            
                $inserirLog = $operacao->execute(array($SQLUpdate,$idUser)); 
                      
      }



    try{

    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLUpdate);    
        
    $update = $operacao->execute();

      if ($update){
        echo '<script language="javascript">';
        echo utf8_decode('alert("Supermercado alterado com sucesso.")');
        echo '</script>';
        $href = '<script language="javascript">location.href="index.php";</script>';
        echo $href;
         }
       else {
          echo '<script language="javascript">';
          echo utf8_decode('alert("Erro ao alterar supermercado.")');
          echo '</script>';
        $href = '<script language="javascript">location.href="index.php";</script>';
        echo $href;
       }  

  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  
