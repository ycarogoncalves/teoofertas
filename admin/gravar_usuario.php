<?php

session_start();
$idUser = $_SESSION['idUser'];

require_once("../conf/confbd.php");

try
{
	if(count($_POST) < 4){
		header("Location: acessoNegado.php");
		die();
	}
	else{

	$nome= htmlspecialchars($_POST ["nome"]);
	$email= htmlspecialchars($_POST ["email"]);
	$tipoUser= htmlspecialchars($_POST ["tipoUser"]);
	$senha1= htmlspecialchars($_POST ["senha1"]);
	$senha2= htmlspecialchars($_POST ["senha2"]);
	$supermercado= htmlspecialchars($_POST ["supermercado"]);

	if(($tipoUser==1) && empty($supermercado)){
		echo '<script language="javascript">';
		echo 'alert("Favor escolher a empresa que este usuário pertence.")';
		echo '</script>';
		$href = '<script language="javascript">location.href="cadUsuario.php";</script>';
		echo $href;		
	}
	if(strcmp($senha1, $senha2)!=0){
		echo '<script language="javascript">';
		echo 'alert("As senhas informadas não são iguais.")';
		echo '</script>';
		$href = '<script language="javascript">location.href="cadUsuario.php";</script>';
		echo $href;		
	}	
	elseif(strcmp($senha1, $senha2)==0){
		$senha=md5($senha1);
	}

try{
	// instancia objeto PDO, conectando no mysql
		$conexao = conn_mysql();

		$SQLInsert = 'INSERT INTO `usuario` (`nomeUsuario`,`emailUsuario`,`senhaUsuario`,`tipoUsuario`) VALUES (?,?,?,?)';	
		$operacao = $conexao->prepare($SQLInsert);					  
		$inserir = $operacao->execute(array($nome,$email,$senha,$tipoUser));
		$lastIdUser = $conexao->lastInsertId();

		            $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
		            $operacao = $conexao->prepare($SQLLogs);            
		            $inserirLog = $operacao->execute(array($SQLInsert,$idUser)); 	


		if($tipoUser==1){
			$SQLInsert = 'INSERT INTO `supermercado_tem_usuario` (`supermercado_idsupermercado`,`usuario_idusuario`) VALUES (?,?)';	
			$operacao = $conexao->prepare($SQLInsert);					  
			$inserir = $operacao->execute(array($supermercado,$lastIdUser));
			
		            $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
		            $operacao = $conexao->prepare($SQLLogs);            
		            $inserirLog = $operacao->execute(array($SQLInsert,$idUser)); 		
		}

			if ($inserir){

				echo '<script language="javascript">';
				echo 'alert("Usuário cadastrado com sucesso.")';
				echo '</script>';
				$href = '<script language="javascript">location.href="index.php";</script>';
				echo $href;
   			 }
			 else {
	  			echo '<script language="javascript">';
	   			echo 'alert("Erro cadastrar usuário.")';
	   			echo '</script>';
				$href = '<script language="javascript">location.href="cadUsuario.php";</script>';
				echo $href;
			 }			
		
	} //try
		catch (PDOException $e)
		{
		    echo "Erro!: " . $e->getMessage() . "<br>";
		    die();
		}

	}

} //try
catch (PDOException $e)
{
	// caso ocorra uma exceÃ§Ã£o, exibe na tela
	echo "Erro!: " . $e->getMessage() . "<br>";
	die();
}

?>

