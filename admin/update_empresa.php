<?php 

session_start();
$idUser = $_SESSION['idUser'];

    require_once("../conf/confbd.php");

    $idsupermercado= htmlspecialchars($_POST ["idsupermercado"]);
    $nome= htmlspecialchars($_POST ["nome"]);
    $endereco= htmlspecialchars($_POST ["endereco"]);
    $telefones= htmlspecialchars($_POST ["telefones"]);
    $horarioFuncionamento= htmlspecialchars($_POST ["horarioFuncionamento"]);
    $software= htmlspecialchars($_POST ["software"]);
    $lat= htmlspecialchars($_POST ["lat"]);
    $lgt= htmlspecialchars($_POST ["lgt"]);
    $mapa= $_POST ["mapa"];

    if(count($_POST['idcategoria'])==0){
			echo '<script language="javascript">';
			echo utf8_decode('alert("Selecione, ao menos, uma categoria.")');
			echo '</script>';
			$href = '<script language="javascript">location.href="listaEmpresa.php";</script>';
			echo $href;
		}
    else{

        $conexao = conn_mysql();  

      if(!empty($_FILES['imagemSupermercado']['name'])){

        $uploaddir = '../images/supermercados/';

        $Imagem = $_FILES['imagemSupermercado']['name'];

        $tipo = explode(".", $_FILES['imagemSupermercado']['name']);

        $uploadfile = $uploaddir . md5($Imagem) . '.' . $tipo[1];
        
        if (move_uploaded_file($_FILES['imagemSupermercado']['tmp_name'], $uploadfile)) {
          
          $SQLUpdate = "UPDATE `supermercado` set nomeSupermercado='$nome',imagemSupermercado='$uploadfile',softwareUtilizado='$software',lat='$lat',lgt='$lgt',telefone='$telefones',endereco='$endereco',horarioFuncionamento='$horarioFuncionamento',mapa='$mapa' where idsupermercado='$idsupermercado';";

                $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
                $operacao = $conexao->prepare($SQLLogs);            
                $inserirLog = $operacao->execute(array($SQLUpdate,$idUser));        

          
          $ultimo_id = $idsupermercado;


          $SQLDelete = "delete from `rel_supermercado_categoria` where `id_supermercado`=?;";	
          $operacao = $conexao->prepare($SQLDelete);					  
          $operacao->bindParam(1,$ultimo_id );
          $delete = $operacao->execute();
					
					#inserindo as categorias da empresa
					foreach($_POST['idcategoria'] as $valor){
						$SQLInsert = "INSERT INTO `rel_supermercado_categoria` (`id_supermercado`,`id_categoria`) VALUES (?,?);";	
						$operacao = $conexao->prepare($SQLInsert);					  
						$operacao->bindParam(1,$ultimo_id );
						$operacao->bindParam(2,$valor );
						$insert = $operacao->execute();
					}
        }

      }
      else{
          $SQLUpdate = "UPDATE `supermercado` set nomeSupermercado='$nome',softwareUtilizado='$software',lat='$lat',lgt='$lgt',telefone='$telefones',endereco='$endereco',horarioFuncionamento='$horarioFuncionamento',mapa='$mapa' where idsupermercado='$idsupermercado';";

                $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
                $operacao = $conexao->prepare($SQLLogs);            
                $inserirLog = $operacao->execute(array($SQLUpdate,$idUser)); 

        
                $ultimo_id = $idsupermercado;

                $SQLDelete = "delete from `rel_supermercado_categoria` where `id_supermercado`=?;";	
                $operacao = $conexao->prepare($SQLDelete);					  
                $operacao->bindParam(1,$ultimo_id );
                $delete = $operacao->execute();
      					
      					#inserindo as categorias da empresa
      					foreach($_POST['idcategoria'] as $valor){
      						$SQLInsert = "INSERT INTO `rel_supermercado_categoria` (`id_supermercado`,`id_categoria`) VALUES (?,?);";	
      						$operacao = $conexao->prepare($SQLInsert);					  
      						$operacao->bindParam(1,$ultimo_id );
      						$operacao->bindParam(2,$valor );
      						$insert = $operacao->execute();
					}
                      
      }



    try{

    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLUpdate);    
        
    $update = $operacao->execute();

    echo $SQLUpdate;
    exit;
    
      if ($update){
        echo '<script language="javascript">';
        echo utf8_decode('alert("Empresa alterada com sucesso.")');
        echo '</script>';
        $href = '<script language="javascript">location.href="listaEmpresa.php";</script>';
        echo $href;
         }
       else {
          echo '<script language="javascript">';
          echo utf8_decode('alert("Erro ao alterar empresa.")');
          echo '</script>';
        $href = '<script language="javascript">location.href="listaEmpresa.php";</script>';
        echo $href;
       }  

  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
    }
?>  
