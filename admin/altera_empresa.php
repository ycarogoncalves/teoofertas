<?php

require_once("authSession.php");

require_once ("../conf/confbd.php");

// instancia objeto PDO, conectando no mysql
$conexao = conn_mysql();

if(isset($_POST['Ativar'])){
    $lixeira = 0;
    $id_empresa = $_POST['Ativar'];
}
if(isset($_POST['Desativar'])){
    $lixeira = 1;
    $id_empresa = $_POST['Desativar'];
}

if(isset($_POST['Ativar']) || isset($_POST['Desativar'])){

  try{
      $SQLUpdate = "UPDATE supermercado set lixeira=".$lixeira." WHERE idsupermercado='".$id_empresa."';";
      $operacao = $conexao->prepare($SQLUpdate);            
      $update = $operacao->execute();

      if($update){
              echo '<script type="text/javascript">alert("Empresa alterada com sucesso.")</script>';
              echo "<script>location.href='listaEmpresa.php';</script>";      
      }
      else{
              echo '<script type="text/javascript">alert("Erro ao alterar a empresa.")</script>';
              echo "<script>location.href='listaEmpresa.php';</script>";         
      }      
  } //try
  catch (PDOException $e)
  {
      echo "Erro!: " . $e->getMessage() . "<br>";
      die();
  }

}
    $idsupermercado = $_POST["editar"];
                  
    try{
    // instrução SQL básica 
    $SQLSelect = "SELECT * from supermercado where idsupermercado = '".$idsupermercado."';";
  
    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if (count($resultados)>0){  
        foreach($resultados as $dadosEncontrados){ 
           $nomeSupermercado=$dadosEncontrados['nomeSupermercado'];                     
           $imagemSupermercado=$dadosEncontrados['imagemSupermercado'];
           $tipoSupermercado=$dadosEncontrados['tipoSupermercado'];
           $softwareUtilizado=$dadosEncontrados['softwareUtilizado'];
           $lat=$dadosEncontrados['lat'];
           $lgt=$dadosEncontrados['lgt'];
           $telefone=$dadosEncontrados['telefone'];
           $endereco=$dadosEncontrados['endereco'];
           $mapa=$dadosEncontrados['mapa'];
           $horarioFuncionamento=$dadosEncontrados['horarioFuncionamento'];
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  


<?php require_once("head.php"); ?>

<body>

<style>
    .divs_checks{
        display:inline-block;
        margin-bottom:10px;
    }
    .divs_checks label{
        margin-right:7px;
    }
</style>

<div class="container">
    
<?php require_once("navbar.php"); ?>

<h2 class="page-header">Editando Empresas</h2>

  <form name="form" id="form"  method="post" action="update_empresa.php"  enctype="multipart/form-data">

       <input type="hidden" name="MAX_FILE_SIZE" value="20971520" />

      <input type="hidden" name="idsupermercado" value="<?php echo $idsupermercado; ?>" />

      <div class="form-group">
        <label for="nome">Nome da Empresa</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome da Empresa" value="<?php echo $nomeSupermercado; ?>" >
      </div>

    <div class="form-group">
        <label for="endereco">Endereço</label>
        <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Endereço" value="<?php echo $endereco; ?>">
    </div>

    <div class="form-group">
        <label for="telefones">Telefones</label>
        <input type="text" class="form-control" name="telefones" id="telefones" placeholder="Telefones" value="<?php echo $telefone; ?>">
    </div>

      <div class="form-group">
        <label for="horarioFuncionamento">Horário de Funcionamento</label>
        <input type="text" class="form-control" name="horarioFuncionamento" id="horarioFuncionamento" value="<?php echo $horarioFuncionamento; ?>" placeholder="Horário de Funcionamento">
      </div>

      <!--<div class="form-group">
        <label for="software">Software Caixa</label>
        <input type="text" class="form-control" name="software" id="software" value="<?php echo $softwareUtilizado; ?>" placeholder="Software Caixa" >
      </div>-->

    <div class="form-group">
       <label for="lat">Latitude</label>
       <input type="text" class="form-control" name="lat" id="lat" value="<?php echo $lat; ?>" placeholder="Latitude">
    </div>
      
    <div class="form-group">
        <label for="lgt">Longitude</label>
        <input type="text" class="form-control" name="lgt" id="lgt" value="<?php echo $lgt; ?>" placeholder="Longitude">
    </div>  

    <div class="form-group">
   <label for="mapa">Mapa</label>
   <input type="text" class="form-control" name="mapa" id="mapa" value="<?=$mapa?>" placeholder="Mapa">
</div>

    <div class="form-group">
        <label class="control-label" style="color:#F00;">Marque a(s) categoria(s) em que se enquadra esta empresa</label><br /><br />
        <?php
            $conexao = conn_mysql();
            $sql = "select *from categoria order by descricaoCategoria asc";	
            $operacao = $conexao->prepare($sql);					  
            $pesquisar = $operacao->execute();
            $resultado = $operacao->fetchAll();

            $sql = "select *from rel_supermercado_categoria where id_supermercado=?";	
            $operacao = $conexao->prepare($sql);
            $operacao->bindParam(1,$idsupermercado );					  
            $pesquisar = $operacao->execute();
            $result_cats = $operacao->fetchAll();
            $categorias = array();
            foreach($result_cats as $todos)
                $categorias[] = $todos['id_categoria'];

            if(count($resultado)>0){
                foreach($resultado as $categoria){
                    $checked = '';
                    if(in_array($categoria['idcategoria'],$categorias))
                        $checked = 'checked="checked"';
                    ?>
                    <div class="divs_checks">
                        <input type="checkbox" <?=$checked?> name="idcategoria[]" id="cat_<?=$categoria['idcategoria']?>" value="<?=$categoria['idcategoria']?>" />
                        <label for="cat_<?=$categoria['idcategoria']?>"><?=$categoria['descricaoCategoria']?></label>
                    </div>
        <?      }
            }?>
    </div>

    <img src="<?php echo "$imagemSupermercado"; ?>" alt="" />
     <div class="form-group">
            <label class="control-label">Imagem Empresa</label>
          <input id="file-0a" name="imagemSupermercado" class="file" type="file">
    </div>   


    <div class="form-group">
        <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
    </div>


  </form>
</div>
   
<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
         
       }
    		});

});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
