<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Cadastro de Supermercado</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="gravar_supermercado.php" enctype="multipart/form-data">

  <input type="hidden" name="MAX_FILE_SIZE" value="20971520" />

  <div class="form-group">
    <label for="nome">Nome do Supermercado</label>
    <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do Supermercado">
  </div>

<div class="form-group">
    <label for="endereco">Endereço</label>
    <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Endereço">
</div>

<div class="form-group">
    <label for="telefones">Telefones</label>
    <input type="text" class="form-control" name="telefones" id="telefones" placeholder="Telefones">
</div>

  <div class="form-group">
    <label for="horarioFuncionamento">Horário de Funcionamento</label>
    <input type="text" class="form-control" name="horarioFuncionamento" id="horarioFuncionamento" placeholder="Horário de Funcionamento">
  </div>

  <div class="form-group">
    <label for="software">Software Caixa</label>
    <input type="text" class="form-control" name="software" id="software" placeholder="Software Caixa">
  </div>

<div class="form-group">
   <label for="lat">Latitude</label>
   <input type="text" class="form-control" name="lat" id="lat" placeholder="Latitude">
</div>
  
<div class="form-group">
    <label for="lgt">Longitude</label>
    <input type="text" class="form-control" name="lgt" id="lgt" placeholder="Longitude">
</div>  

 <div class="form-group">
	  		<label class="control-label">Imagem Supermercado</label>
			<input id="file-0a" name="imagemSupermercado" class="file" type="file">
</div>	 
 

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>
</div>



<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            endereco: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                              
            telefones: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
             horarioFuncionamento: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },  
            imagemSupermercado: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                       
          
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
