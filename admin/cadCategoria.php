<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Cadastro de Categoria</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="gravar_categoria.php">


    <div class="form-group">
        <label for="descricaoCategoria">Descrição</label>
        <input type="text" class="form-control" name="descricaoCategoria" id="descricaoCategoria" placeholder="Descrição da categoria">
    </div>

    <div class="form-group">
        <label for="icone">Ícone</label>
        <input type="text" class="form-control" name="icone" id="icone" placeholder="Ex: fa-tags">
    </div>


    <div class="form-group">
        <button type="submit" class="btn btn-info">Cadastrar</button> 
    </div>

</form>

</div>
</div>



<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            endereco: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                              
            telefones: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
             horarioFuncionamento: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },  
            imagemSupermercado: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                       
          
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
