<?php

require_once("authSession.php");

require_once ("../conf/confbd.php");

// instancia objeto PDO, conectando no mysql
$conexao = conn_mysql();

if(isset($_POST['Ativar'])){
    $lixeira = 0;
    $id_user = $_POST['Ativar'];
}
if(isset($_POST['Desativar'])){
    $lixeira = 1;
    $id_user = $_POST['Desativar'];
}

if(isset($_POST['Ativar']) || isset($_POST['Desativar'])){

  try{
      $SQLUpdate = "UPDATE usuario set lixeira=".$lixeira." WHERE idusuario='".$id_user."';";
      $operacao = $conexao->prepare($SQLUpdate);            
      $update = $operacao->execute();

      if($update){
              echo '<script type="text/javascript">alert("Usuário alterado com sucesso.")</script>';
              echo "<script>location.href='listaUsuario.php';</script>";      
      }
      else{
              echo '<script type="text/javascript">alert("Erro ao alterar usuário.")</script>';
              echo "<script>location.href='listaUsuario.php';</script>";         
      }      
  } //try
  catch (PDOException $e)
  {
      echo "Erro!: " . $e->getMessage() . "<br>";
      die();
  }

}

if(isset($_POST['resetSenha'])){

  try{
      $SQLUpdate = "UPDATE usuario set senhaUsuario=md5('1234') WHERE idusuario='".$_POST['resetSenha']."';";
      $operacao = $conexao->prepare($SQLUpdate);            
      $update = $operacao->execute();

      if($update){
              echo '<script type="text/javascript">alert("Senha alterada para: 1234. Informe ao usuário.")</script>';
              echo "<script>location.href='index.php';</script>";      
      }
      else{
              echo '<script type="text/javascript">alert("Erro ao alterada senha.")</script>';
              echo "<script>location.href='index.php';</script>";         
      }      
  } //try
  catch (PDOException $e)
  {
      echo "Erro!: " . $e->getMessage() . "<br>";
      die();
  }

}

    $idusuario = $_POST["editar"];
                  
    try{
    // instrução SQL básica 
    $SQLSelect = "SELECT * from usuario where idusuario = '".$idusuario."';";
  
    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if (count($resultados)>0){  
        foreach($resultados as $dadosEncontrados){ 
           $nome=$dadosEncontrados['nomeUsuario'];                     
           $emailUsuario=$dadosEncontrados['emailUsuario'];
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  


<?php require_once("head.php"); ?>

<body>

<div class="container">
    
<?php require_once("navbar.php"); ?>

<h2 class="page-header">Editando Usuário</h2>

  <form name="form" id="form"  method="post" action="update_usuario.php">

      <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>" />

      <input type="hidden" name="dadosPessoais" value="1" />

      <div class="form-group">
        <label for="nome">Nome Usuário</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do Supermercado" value="<?php echo $nome; ?>" >
      </div>

    <div class="form-group">
        <label for="endereco">E-mail</label>
        <input type="text" class="form-control" name="email" id="email" placeholder="E-mail" value="<?php echo $emailUsuario; ?>">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-info" name="signup">Atualizar Dados</button> 
    </div>


  </form>
</div>
   
<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
         
       }
    		});

});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
