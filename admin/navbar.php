<!-- Navigation -->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
       <div class="container">
           <!-- Brand and toggle get grouped for better mobile display -->
           <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                   <span class="sr-only">Toggle navigation</span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
               </button>
               <a href="#menu-toggle" class="btn button_menu" aria-label="Left Align"id="menu-toggle">
                   <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
               </a>
               <a class="navbar-brand" href="index.php"><img src='../images/BLACKPRICE80x80.png'></a>
           </div>

           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

             <!--<div id="searchBar" class="col-md-4">
               <form class="navbar-form" role="search" method="post" id="search-form" name="search-form" action="resultadoBusca.php">
                 <div id="containerSearch" class="input-group">
                     <input autocomplete="off" onkeyup="loadProduto(this.value);" type="text" class="form-control" placeholder="Digite o nome do produto" list="produto" name="query" id="query">

                      <datalist name="produto" id="produto"></datalist>

                      <div class="input-group-btn">
                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                     </div>
                 </div>
                </form>
              </div>-->

            <?php if(!empty($_SESSION['auth'])){
                ?>
                <ul class="nav navbar-nav navbar-right">
                   <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"> </span> <span class="menuAdmin"><?php $arrayNome = explode(" ",  $_SESSION['nomeUsuario']); $nomeUsuario = $arrayNome[0]; echo $nomeUsuario;?></span><span class="caret"></span></a>
                     <ul class="dropdown-menu" role="menu">
                       <li><a href="alteraDadosPessoais.php"><span class="glyphicon glyphicon-pencil"></span>  Editar dados</a></li>
                       <li><a href="alteraSenha.php"><span class="glyphicon glyphicon-pencil"></span>  Alterar senha</a></li>
                       <li><a href="logs.php"><span class="glyphicon glyphicon-search"></span>  Logs do sistema</a></li>
                       <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                     </ul>
                   </li>
                 </ul>
                <?php
            } else {
                ?>
                <ul class="nav navbar-nav navbar-right">
                   <li>
                       <a href="#" role="button" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-user"></span> Entrar</a>
                   </li>
                </ul>
                <?php
            }
            ?>

             <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"> </span> <span class="menuAdmin">Usuários</span><span class="caret"></span></a>
                 <ul class="dropdown-menu" role="menu">
                   <li><a href="cadUsuario.php"><span class="glyphicon glyphicon-plus"></span>  Cadastrar</a></li>
                   <li><a href="listaUsuario.php"><span class="glyphicon glyphicon-search"></span> Pesquisar</a></li>
                 </ul>
               </li>
             </ul>

             <ul class="nav navbar-nav navbar-right">
               <li>
                 <a href="listaPromocoes.php" style="outline:none;"><i class="fa fa-bullhorn" aria-hidden="true"></i> <span class="menuAdmin">Promoções</span></a>
               </li>
             </ul>


             <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-university" aria-hidden="true"></i> <span class="menuAdmin">Empresas</span><span class="caret"></span></a>
                 <ul class="dropdown-menu" role="menu">
                   <li><a href="cadEmpresa.php"><span class="glyphicon glyphicon-plus"></span>  Cadastrar</a></li>
                   <li><a href="listaEmpresa.php"><span class="glyphicon glyphicon-search"></span> Pesquisar</a></li>
                 </ul>
               </li>
             </ul>

             <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-tags" aria-hidden="true"></i> <span class="menuAdmin">Categorias</span><span class="caret"></span></a>
                 <ul class="dropdown-menu" role="menu">
                   <li><a href="cadCategoria.php"><span class="glyphicon glyphicon-plus"></span>  Cadastrar</a></li>
                   <li><a href="listaCategoria.php"><span class="glyphicon glyphicon-search"></span> Pesquisar</a></li>
                 </ul>
               </li>
             </ul>

           </div>
       <!-- /.container -->
   </nav>

   <?php
       require_once("../conf/confbd.php");

       $conexao = conn_mysql();
       $sql = "select *from categoria order by descricaoCategoria asc";

       // $resultado_cats = mysqli_query($conexao,$sql);

       $operacao = $conexao->prepare($sql);
       $pesquisar = $operacao->execute();
       $resultado_cats = $operacao->fetchAll();
   ?>

    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                   <span>Navegação</span>
                </li>
                <?php
                if( !empty($resultado_cats) ){
                    foreach($resultado_cats as $categoria){
                        ?>
                        <li>
                            <a href="index.php?cat=<?=$categoria['idcategoria']?>">
                                   <i class="fa <?=$categoria['icone']?>" aria-hidden="true"></i>
                                   <?=$categoria['descricaoCategoria']?>
                            </a>
                        </li>
                    <?php  
                    }
                }
               ?>
            </ul>
        </div>
    </div>
