 <?php

session_start();

$idusuario= $_SESSION['idUser'];

require_once("authSession.php");

require_once("../conf/confbd.php");
// require_once("../conf/confBD.php");


if(isset($_POST['Ativar'])){
    $status = 1;
    $id_oferta = $_POST['Ativar'];
}
if(isset($_POST['Desativar'])){
    $status = 0;
    $id_oferta = $_POST['Desativar'];
}

if(isset($_POST['Ativar']) || isset($_POST['Desativar'])){
    $conexao = conn_mysql();    
    try{
        $SQLUpdate = "UPDATE publicaOferta set status=".$status." WHERE idpublicaOferta=".$id_oferta.";";
        // $update = mysqli_query($conexao,$SQLUpdate);

        $operacao = $conexao->prepare($SQLUpdate);            
        $update = $operacao->execute();
        $conexao = null;
    } //try
    catch (PDOException $e)
    {
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
    }
}

$conexao = conn_mysql();

if (isset($_POST['buscar'])){
    $nome = $_POST['buscar'];

    $sql = "SELECT idpublicaOferta,status,desc_promocao,precoOferta,date_format(dataInicial,'%Y/%m/%d') as dataInicial,date_format(dataFinal,'%Y/%m/%d') as dataTermino,nomeSupermercado FROM `publicaOferta`,`supermercado` WHERE supermercado_idsupermercado=supermercado.idsupermercado and (status=0 or status=1) and desc_promocao like '%".$nome."%' order by dataFinal;";
}	
else {
    $sql= "SELECT idpublicaOferta,status,desc_promocao,precoOferta,date_format(dataInicial,'%Y/%m/%d') as dataInicial,date_format(dataFinal,'%Y/%m/%d') as dataTermino,nomeSupermercado FROM `publicaOferta`,`supermercado` WHERE supermercado_idsupermercado=supermercado.idsupermercado and (status=0 or status=1)  order by dataFinal;";
}

// $resultados = mysqli_query($conexao,$sql);

$operacao = $conexao->prepare($sql);      
$pesquisar = $operacao->execute();
$resultados = $operacao->fetchAll();

$conexao = null;

//if ( $rows > 0 ){
?>

<?php require_once("head.php"); ?>

<body>

<div class="container">
    
<?php require_once("navbar.php"); ?>

<h2 class="page-header">Promoções a serem ativadas</h2>

	<br>
	
<form id="formCad" method="post" action="">
  
  <div class="row">   
  <div class="col-md-6">
    <input type="text" class="form-control" name="buscar" id="buscar" placeholder="Buscar promoções">
  </div>

  <div class="col-md-2">
      <button type="submit" class="btn btn-warning" name="signup">Buscar</button>
  </div>
  </div>
</form>
	
	<center>

	<br>
	<div class="table-responsive">
	<table class="table">

 <tr>
    <td height="5"><b>Empresa</b></td>
 	<td height="5"><b>Descrição</b></td>
	<td height="5"><b>Data de Início</b></td>
	<td height="5"><b>Data de Término</b></td>
	<td height="5"><b>Preço</b></td>
	<td height="5"><b>Ação</b></td>
 </tr>
	<form id="cadastro" name="form1" method="post" action=""  >
	<?php
	if ( !empty($resultados) ){
        foreach($resultados as $linha){ 
            if($linha['status']==1){
                $classe = "btn-success";
                $text = "Desativar";
            }
            else{
                $classe = "btn-danger";
                $text = "Ativar";
            }
            ?>

            <tr>
            <td><?php echo $linha['nomeSupermercado']; ?></td>
            <td><?php echo $linha['desc_promocao']; ?></td>
            <td><?php echo $linha['dataInicial']; ?></td>
            <td><?php echo $linha['dataTermino']; ?></td>
            <td>R$ <?php echo number_format($linha['precoOferta'],2,",","."); ?></td>
            <td><button name="<?=$text?>" onclick="return confirm('Deseja <?=strtolower($text)?> esta oferta?');" class="btn <?=$classe?>" type="submit" id="excluir" size="10" value="<?php echo $linha['idpublicaOferta']; ?>"><?=$text?></button></td>

            </tr>
            <?php 
        }
    } else { 
        ?>
        <tr><td colspan="6">Nenhuma promoção encontrada.</td></tr>
        <?php  
    }
    ?>
	</form>

	</table>
</div>
	</center>
	<?php
	/*}
	else{

	echo '<script language="javascript">';
	echo 'alert("Não há promoções registradas no momento.")';
	echo '</script>';
	 $href = '<script language="javascript">location.href="listaPromocoes.php";</script>';
	 echo $href;*/
 			
	?>
	
	<?php
	//}
	?>
</div>
 </body>
</html>
