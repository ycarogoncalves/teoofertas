<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Black Price</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link href="../css/simple-sidebar.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/blackprice.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="../css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script src="../js/fileinput.js" type="text/javascript"></script>
    <script src="../js/fileinput_locale_pt.js" type="text/javascript"></script>   
    <script src="../js/jquery.js" type="text/javascript"></script>     
    <script src="../js/jquery.countdown.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
</head>