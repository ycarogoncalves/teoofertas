<?php

session_start();

include('../conf/confbd.php');

include('authSession.php');

include('head.php');

?>


<body>
	<?php include('navbar.php'); ?>

    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Promoções</h2>
            </div>

        <?php 

            $datahoje = date('Y-m-d');
                // instancia objeto PDO, conectando no mysql
                $conexao = conn_mysql();    


            try{
                $SQLDelete = "update `publicaOferta` set status=0 where dataFinal < '$datahoje';";
                // $delete = mysqli_query($conexao,$SQLDelete);
                
                $operacao = $conexao->prepare($SQLDelete);                    
                $delete = $operacao->execute();
            }
            catch (PDOException $e)
            {
                echo "Erro!: " . $e->getMessage() . "<br>";
                die();
            }

            try{

                // instrução SQL básica 
                $SQLSelect = "SELECT status,idpublicaOferta,desc_promocao,banner_promocao,precoOferta,dataInicial,dataFinal,date_format(dataFinal,'%Y/%m/%d') as dataTermino,nomeSupermercado FROM `publicaOferta`,`supermercado` WHERE supermercado_idsupermercado=supermercado.idsupermercado and dataFinal >= '$datahoje' and (status = 0 or status = 1);";

                // $resultados = mysqli_query($conexao,$SQLSelect);

                //prepara a execução da sentença
                $operacao = $conexao->prepare($SQLSelect);      
                        
                $pesquisar = $operacao->execute();
                
                //captura TODOS os resultados obtidos
                $resultados = $operacao->fetchAll();

                // se há resultados, os escreve em uma tabela
                if ( !empty($resultados) ){  
                    foreach($resultados as $dadosEncontrados){

                        // $id = $dadosEncontrados['idproduto'];
                        $idOferta = $dadosEncontrados['idpublicaOferta'];
                        // $barras = $dadosEncontrados['barras'];
                        $descricaoProduto = $dadosEncontrados['desc_promocao'];
                        $imagemProduto = $dadosEncontrados['banner_promocao'];
                        $precoOferta = $dadosEncontrados['precoOferta'];
                        $dataFinal = $dadosEncontrados['dataTermino'];
                        $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                        $precoOferta = number_format($precoOferta,2,",",".");
                        $status = $dadosEncontrados['status'];
                        $banner_promocao = $dadosEncontrados['banner_promocao'];

                        if($status==1){
                            $eye="fa-eye";
                        }
                        else{
                            $eye="fa-eye-slash";
                        }
                        /*
    					if (!getimagesize($imagemProduto)) {
    					    $imagemProduto = 'images/notfound.png';
    					} 
    					*/
                            echo "
                            <div class='portfolio-item col-md-4 col-sm-6'>
                            <span onclick='cgPromo(\"$idOferta\")'; class='addProduto'><i id='item_$idOferta' class='fa $eye'></i></span>
                                <img class='blackItem' src='../images/BLACKPRICE80x80.png' alt=''>
                                <a href='#'>
                                    <img class='img-responsive img-portfolio img-hover' src='$banner_promocao' alt=''>
                                    <span class='productDescription'>".substr($descricaoProduto, 0,38)."</span>
                                    <p class='productPrice'>R$ $precoOferta</p>
                                    <p class='productSupermercado'>$nomeSupermercado</p>

                                    <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante </p>

                                    <p>
                                        <div id='contador' class='contador_$idOferta'></div>
                                        <script type='text/javascript'>
                                          $('.contador_$idOferta').countdown('$dataFinal', function(event) {
                                            $(this).html(event.strftime('%D dias %H:%M:%S'));
                                          });
                                        </script>                    
                                    </p>

                                </a>
                            </div>
                           ";       
                    }
                } else {
                    echo'<div class="starter-template">';
                    echo"\n<p class=\sub-header\>Nenhuma promoção encontrada.</p>";
                    echo'</div>';
                }
            } //try
            catch (PDOException $e)
            {
                // caso ocorra uma exceção, exibe na tela
                echo "Erro!: " . $e->getMessage() . "<br>";
                die();
            }

            $conexao = NULL;
        ?> 
        </div>
        <!-- /.row -->

    
       <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Empresas participantes</h2>
            </div>

            <?php

           // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            $SQLSelect="SELECT * from supermercado where lixeira = 0;";

            // $resultados = mysqli_query($conexao,$SQLSelect);

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if ( !empty($resultados) ){  
                foreach($resultados as $dadosEncontrados){
                    $idsupermercado = $dadosEncontrados['idsupermercado'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $imagemSupermercado = $dadosEncontrados['imagemSupermercado'];
                    $lat = $dadosEncontrados['lat'];
                    $lgt = $dadosEncontrados['lgt'];
                    $telefone = $dadosEncontrados['telefone'];
                    $endereco = $dadosEncontrados['endereco'];
                    $horarioFuncionamento = $dadosEncontrados['horarioFuncionamento'];         

               echo    "<div class='portfolio-supermercado col-md-4 col-sm-6'>
                        <a href='supermercado.php?id=$idsupermercado'>
                            <img class='img-responsive img-portfolio img-hover' src='$imagemSupermercado' alt=''>
                        </a>
                        </div>";

                }

            }
            else{
                echo'<div class="starter-template">';
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
?>

         </div>

        <hr>


        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Jeito Fácil 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <script type="text/javascript">


function cgPromo(id){
                        
                    var promo = document.getElementById('item_'+id);

                    if ($(promo).hasClass( "fa-eye-slash" )){   
                                $(promo).fadeOut();    
                                        $(promo).removeClass( "fa-eye-slash" );
                                        $(promo).fadeIn();     
                                        $(promo).addClass( "fa-eye" );
        
                            $.post("cgPromo.php",
                            {
                                liberar: "1",
                                idOferta: id
                            },
                            function(data, status){
                              //  alert("Data: " + data + "\nStatus: " + status);
                            });         
                                                            
                    }
                    else{
                        
                                if (confirm("Deseja bloquear esta promoção?")) {
                                         $(promo).fadeOut();                              
                                                    $(promo).removeClass( "fa-eye" );  
                                                    $(promo).fadeIn();     
                                                    $(promo).addClass( "fa-eye-slash" );
                
                                        $.post("cgPromo.php",
                                        {
                                            remover: "1",
                                            idOferta: id
                                        },
                                        function(data, status){
                                            //alert("Data: " + data + "\nStatus: " + status);
                                        });
                                     }
                    
                        }
}

    </script>

    <?php  include('footer.php'); ?>

</body>

</html>
