<?php

session_start();

include('../conf/confbd.php');

include('authSession.php');

$produto = $_POST['query'];

$_SESSION['pesquisa'] = $produto;
            
?>

    <!DOCTYPE html>
<html lang="en">

<?php include('head.php'); ?>

<body>
    <?php include('navbar.php'); ?>

    <!-- Page Content -->
    <div class="container">
        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Produtos em destaque</h2>
            </div>


        <?php 



            // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            // instrução SQL básica 
            $SQLSelect = "SELECT * from produto where descricaoProduto like '$produto%' order by barras;";

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();


            // ALGORITMO COMPARAR E CALCULAR MENOR PREÇO
            $i=1;
            $j=0;
            $menorPreco=100000;
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $barras[$i] = $dadosEncontrados['barras'];
                    $preco[$i] = $dadosEncontrados['precoProduto'];

                    if($barras[$i]==$barras[$i-1]){
                    	if($preco[$i-1]<$menorPreco){
                    		$menorPreco=$preco[$i-1];
                    		if($preco[$i]<$menorPreco){
                    			$menorPreco=$preco[$i];
                    			$menorPrecoPos[$j]=$i;
                    		}
                    		else{
                    			$menorPrecoPos[$j]=$i-1;
                    		}
                    	}
                    }else{
                    	$j++;
                    	$menorPreco=100000;
                    }
                    $i++;
                }
             }
            // se há resultados, os escreve em uma tabela
            $i=1;
            $j=1;
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $barras = $dadosEncontrados['barras'];
                    $descricaoProduto = $dadosEncontrados['descricaoProduto'];
                    $imagemProduto = $dadosEncontrados['imagemProduto'];

                    /*
                    if (!getimagesize($imagemProduto)) {
                        $imagemProduto = 'images/notfound.png';
                    } 
                    */


                    if(!empty($_SESSION['auth'])){
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6'>
                            <span class='addProduto'><i class='fa fa-pencil'></i></span>
                            <img class='blackItem' src='../images/BLACKPRICE80x80.png' alt=''>
                            <a href='editarProduto.php?barras=$barras'>
                                <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                <span class='productDescription'>".substr($descricaoProduto, 0,38)."</span>
                                <p class='productPrice'> $barras</p>
                                <p class='productSupermercado'></p>
                            </a>
                        </div>
                       ";   

                     }else{
                        echo "
                        <div class='portfolio-item col-md-4 col-sm-6'>
                            <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                            <a href='portfolio-item.php?id=$id'>
                                <img class='img-responsive img-portfolio img-hover' src='$imagemProduto' alt=''>
                                <span class='productDescription'>".substr($descricaoProduto, 0,36)."</span>
                                <p class='productPrice'>R$ $precoProduto</p>
                                <p class='productSupermercado'>$nomeSupermercado</p>
                            </a>
                        </div>
                       ";
                     }    
                     $i++;          
                }
            }
            else{
                echo'<div class="starter-template">';
                echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
    ?>  
                                        
        </div>
        <!-- /.row -->

        <hr>

       <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Supermercados participantes</h2>
            </div>

<?php

           // instancia objeto PDO, conectando no mysql
            $conexao = conn_mysql();    

            try{

            $SQLSelect="SELECT * from supermercado where lixeira = 0;";

            //prepara a execução da sentença
            $operacao = $conexao->prepare($SQLSelect);      
                    
            $pesquisar = $operacao->execute();
            
            //captura TODOS os resultados obtidos
            $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if (count($resultados)>0){  
                foreach($resultados as $dadosEncontrados){
                    $idsupermercado = $dadosEncontrados['idsupermercado'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $imagemSupermercado = $dadosEncontrados['imagemSupermercado'];
                    $lat = $dadosEncontrados['lat'];
                    $lgt = $dadosEncontrados['lgt'];
                    $telefone = $dadosEncontrados['telefone'];
                    $endereco = $dadosEncontrados['endereco'];
                    $horarioFuncionamento = $dadosEncontrados['horarioFuncionamento'];         

               echo    "<div class='portfolio-supermercado col-md-4 col-sm-6'>
                        <a href='supermercado.php?id=$idsupermercado'>
                            <img class='img-responsive img-portfolio img-hover' src='../$imagemSupermercado' alt=''>
                        </a>
                        </div>";

                }

            }
            else{
                echo'<div class="starter-template">';
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
?>

         </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Black Price 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <?php include('footer.php'); ?>    
</body>

</html>
