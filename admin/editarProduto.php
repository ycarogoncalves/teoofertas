<?php

session_start();
require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Editar Produto</h2>

<?php require_once("navbar.php"); ?>
 
   <?php 
      // instancia objeto PDO, conectando no mysql

    $barras=$_GET['barras'];
    $conexao = conn_mysql();                    
    try{
    // instrução SQL básica 
    $SQLSelect = "SELECT * FROM `produto` where barras=$barras";
  
    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if (count($resultados)>0){  
        foreach($resultados as $dadosEncontrados){ 
           $descricaoProduto=$dadosEncontrados['descricaoProduto'];                     
           $imagemProduto=$dadosEncontrados['imagemProduto'];
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  

<form id="formCad" method="post" action="update_produto.php">

  <div class="form-group">
    <label for="nome">Código de barras</label>
    <input type="text" class="form-control" name="barras" id="barras" placeholder="Código de barras" value="<?php echo $barras; ?>" readonly>
  </div>

  <div class="form-group">
    <label for="nome">Descrição</label>
    <input type="text" class="form-control" name="descricao" id="descricao" placeholder="Descrição" value="<?php echo $descricaoProduto; ?>">
  </div>

    <div class="form-group">
    <label for="nome">URL da Imagem</label>
    <input type="text" class="form-control" name="imagem" id="imagem" placeholder="URL da Imagem" value="<?php echo $imagemProduto; ?>">
  </div>

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Atualizar</button> 
  </div>

</form>

</div>
</div>


<script type="text/javascript">

jQuery(function($){
          
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            imagem: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            descricao: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                                              
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
    
  </body>
</html>
