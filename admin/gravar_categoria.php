<?php

session_start();
$idUser = $_SESSION['idUser'];

require_once("authSession.php");
require_once("../conf/confbd.php");

try
{
	if(count($_POST) < 2){
		header("Location: acessoNegado.php");
		die();
	}
	else{
        $tem_vazio = 0;
        foreach($_POST as $chave=>$valor){
            $$chave = htmlspecialchars($valor);
            
            if(strlen($$chave)==0)
                $tem_vazio = 1;
        }
        
        if($tem_vazio==1){
            echo '<script language="javascript">';
            echo utf8_decode('alert("Todos os campos devem ser preenchidos.")');
            echo '</script>';
            $href = '<script language="javascript">location.href="cadCategoria.php";</script>';
            echo $href;
        }
        else{

            try{
                // instancia objeto PDO, conectando no mysql
                $conexao = conn_mysql();
                $SQLInsert = "INSERT INTO `categoria` (`descricaoCategoria`,`icone`) VALUES (?,?);";	
                $operacao = $conexao->prepare($SQLInsert);					  
                $operacao->bindParam(1,$descricaoCategoria );
                $operacao->bindParam(2,$icone );
                $inserir = $operacao->execute();

                if ($inserir){
                    $SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
                    $operacao = $conexao->prepare($SQLLogs);            
                    $inserirLog = $operacao->execute(array($SQLInsert,$idUser));  
                                    
                    echo '<script language="javascript">';
                    echo utf8_decode('alert("Categoria cadastrada com sucesso.")');
                    echo '</script>';
                    $href = '<script language="javascript">location.href="cadCategoria.php";</script>';
                    echo $href;
                }
                else {
                    echo '<script language="javascript">';
                    echo utf8_decode('alert("Erro cadastrar categoria.")');
                    echo '</script>';
                    $href = '<script language="javascript">location.href="cadCategoria.php";</script>';
                    echo $href;
                }			
            
            } //try
            catch (PDOException $e)
            {
                echo "Erro!: " . $e->getMessage() . "<br>";
                die();
            }
        }
	}

} //try
catch (PDOException $e)
{
	// caso ocorra uma exceÃ§Ã£o, exibe na tela
	echo "Erro!: " . $e->getMessage() . "<br>";
	die();
}

?>

