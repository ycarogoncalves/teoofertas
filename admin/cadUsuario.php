<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Cadastro de Usuário</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="gravar_usuario.php">

<div class="form-group">
  <label for="nome">Nome</label>
  <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome">
</div>

<div class="form-group">
    <label for="email">E-mail</label>
    <input type="text" class="form-control" name="email" id="email" placeholder="E-mail">
</div>

<div class="form-group">
<label for="user">Tipo de Usuário</label>
    <div class="radio">
      <label><input type="radio" class="tipoUser" value="2" name="tipoUser">Administrador</label>
    </div>
    <div class="radio">
      <label><input type="radio" class="tipoUser" value="1" name="tipoUser">Usuário de Empresa</label>
    </div>
    <div class="radio">
      <label><input type="radio" class="tipoUser" value="0" name="tipoUser">Usuário do site</label>
    </div>
</div>


<div class="form-group" id="supermercadoHidden">
    <label for="supermercado">Empresa</label>
  <?php 
      // instancia objeto PDO, conectando no mysql
    $conexao = conn_mysql();                    
    try{
    // instrução SQL básica 
    $SQLSelect = "SELECT * FROM `supermercado` where lixeira=0;";
  
    //prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // fecha a conexão (os resultados já estão capturados)
    //$conexao = null;

    // se há resultados, os escreve em uma tabela
    if (count($resultados)>0){  
        foreach($resultados as $dadosEncontrados){    //para cada elemento do vetor de resultados...    
              echo "<div class='radio'><label><input name='supermercado' type='radio' value=".$dadosEncontrados['idsupermercado'].">".$dadosEncontrados['nomeSupermercado']."</label></div>";                                
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  
</div>


<div class="form-group">
    <label for="senha1">Senha</label>
    <input type="password" class="form-control" name="senha1" id="senha1" placeholder="Senha">
</div>

<div class="form-group">
    <label for="senha2">Confirmar Senha</label>
    <input type="password" class="form-control" name="senha2" id="senha2" placeholder="Confirmar Senha">
</div>


  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>
</div>

<script type="text/javascript">
$('.tipoUser').click(function() {
    if ($(this).val() === '1') {
      $("#supermercadoHidden").fadeIn();
    }
    else{
      $("#supermercadoHidden").fadeOut();
    }
});
</script>

<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            endereco: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                              
            telefones: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
             horarioFuncionamento: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },  
            imagemSupermercado: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                       
          
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
