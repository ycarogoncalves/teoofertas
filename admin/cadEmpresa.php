<?php

session_start();

require_once("authSession.php");

require_once("../conf/confbd.php");

?>
<html>

<?php require_once("head.php"); ?>

<body>

<style>
    .divs_checks{
        display:inline-block;
        margin-bottom:10px;
    }
    .divs_checks label{
        margin-right:7px;
    }
</style>

<div class="container">
     
<div class="col-md-6">

<h2 class="titleH2">Cadastro de Empresa</h2>

<?php require_once("navbar.php"); ?>
 
<form id="formCad" method="post" action="gravar_empresa.php" enctype="multipart/form-data">

  <input type="hidden" name="MAX_FILE_SIZE" value="20971520" />

  <div class="form-group">
    <label for="nome">Nome da Empresa</label>
    <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome da Empresa">
  </div>

<div class="form-group">
    <label for="endereco">Endereço</label>
    <input type="text" class="form-control" name="endereco" id="endereco" placeholder="Endereço">
</div>

<div class="form-group">
    <label for="telefones">Telefones</label>
    <input type="text" class="form-control" name="telefones" id="telefones" placeholder="Telefones">
</div>

  <div class="form-group">
    <label for="horarioFuncionamento">Horário de Funcionamento</label>
    <input type="text" class="form-control" name="horarioFuncionamento" id="horarioFuncionamento" placeholder="Horário de Funcionamento">
  </div>

  <!--<div class="form-group">
    <label for="software">Software Caixa</label>
    <input type="text" class="form-control" name="software" id="software" placeholder="Software Caixa">
  </div>-->

<div class="form-group">
   <label for="lat">Latitude</label>
   <input type="text" class="form-control" name="lat" id="lat" placeholder="Latitude">
</div>
  
<div class="form-group">
    <label for="lgt">Longitude</label>
    <input type="text" class="form-control" name="lgt" id="lgt" placeholder="Longitude">
</div>  

<div class="form-group">
   <label for="mapa">Mapa</label>
   <input type="text" class="form-control" name="mapa" id="mapa" placeholder="Mapa">
</div>

 <div class="form-group">
    <label class="control-label">Logomarca</label>
    <input id="file-0a" name="imagemSupermercado" class="file" type="file">
</div>

<div class="form-group">
    <label class="control-label" style="color:#F00;">Marque a(s) categoria(s) em que se enquadra esta empresa</label><br /><br />
    <?php
        $conexao = conn_mysql();
        $sql = "select * from categoria where lixeira=0 order by descricaoCategoria asc";	
        // $resultados = mysqli_query($conexao,$sql);

        $operacao = $conexao->prepare($sql);					  
        $pesquisar = $operacao->execute();
        $resultado = $operacao->fetchAll();

        if( !empty($resultado) ){
            foreach($resultado as $categoria){
                ?>
                <div class="divs_checks">
                    <input type="checkbox" name="idcategoria[]" id="cat_<?=$categoria['idcategoria']?>" value="<?=$categoria['idcategoria']?>" />
                    <label for="cat_<?=$categoria['idcategoria']?>"><?=$categoria['descricaoCategoria']?></label>
                </div>
                <?php      
            }
        }
        ?>
</div>
 

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
  </div>

</form>

</div>
</div>



<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            endereco: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                              
            telefones: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
             horarioFuncionamento: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },  
            imagemSupermercado: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                       
          
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
