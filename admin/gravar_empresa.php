<?php

session_start();
$idUser = $_SESSION['idUser'];

require_once("authSession.php");
require_once("../conf/confbd.php");


try
{
	if(count($_POST) < 8){
		header("Location: acessoNegado.php");
		die();
	}
	else{

		$nome= htmlspecialchars($_POST ["nome"]);
		$endereco= htmlspecialchars($_POST ["endereco"]);
		$telefones= htmlspecialchars($_POST ["telefones"]);
		$horarioFuncionamento= htmlspecialchars($_POST ["horarioFuncionamento"]);
		//$software= htmlspecialchars($_POST ["software"]);
		$lat= htmlspecialchars($_POST ["lat"]);
		$lgt= htmlspecialchars($_POST ["lgt"]);
		$mapa= $_POST ["mapa"];

		if(count($_POST['idcategoria'])==0){
			echo '<script language="javascript">';
			echo utf8_decode('alert("Selecione, ao menos, uma categoria.")');
			echo '</script>';
			$href = '<script language="javascript">location.href="cadEmpresa.php";</script>';
			echo $href;
		}
		else{

			try{
				// instancia objeto PDO, conectando no mysql
				$conexao = conn_mysql();

				$uploaddir = '../images/supermercados/';

				$Imagem = $_FILES['imagemSupermercado']['name'];

				$tipo = explode(".", $_FILES['imagemSupermercado']['name']);

				$uploadfile = $uploaddir . md5($Imagem) . '.' . $tipo[1];

				if (move_uploaded_file($_FILES['imagemSupermercado']['tmp_name'], $uploadfile)) {

					$SQLInsert = "INSERT INTO `supermercado` (`nomeSupermercado`,`imagemSupermercado`,`lat`,`lgt`,`telefone`,`endereco`,`horarioFuncionamento`,mapa) VALUES ('$nome','$uploadfile','$lat','$lgt','$telefones','$endereco','$horarioFuncionamento','$mapa');";	

					$operacao = $conexao->prepare($SQLInsert);					  
					$inserir = $operacao->execute();

					$ultimo_id = $conexao->lastInsertId();
					
					#inserindo as categorias da empresa
					foreach($_POST['idcategoria'] as $valor){
						$SQLInsert = "INSERT INTO `rel_supermercado_categoria` (`id_supermercado`,`id_categoria`) VALUES (?,?);";	
						$operacao = $conexao->prepare($SQLInsert);					  
						$operacao->bindParam(1,$ultimo_id );
						$operacao->bindParam(2,$valor );
						$insert = $operacao->execute();
					}

				}
				
				if ($inserir){

						$SQLLogs = 'INSERT INTO `logs` (`sql`,`usuario_idusuario`) VALUES (?,?)';  
						$operacao = $conexao->prepare($SQLLogs);            
						$inserirLog = $operacao->execute(array($SQLInsert,$idUser));  
									
					echo '<script language="javascript">';
					echo utf8_decode('alert("Empresa cadastrada com sucesso.")');
					echo '</script>';
					$href = '<script language="javascript">location.href="cadEmpresa.php";</script>';
					echo $href;
				}
				else {
					echo '<script language="javascript">';
					echo utf8_decode('alert("Erro cadastrar empresa.")');
					echo '</script>';
					$href = '<script language="javascript">location.href="cadEmpresa.php";</script>';
					echo $href;
				}			
			
			} //try
			catch (PDOException $e)
			{
				echo "Erro!: " . $e->getMessage() . "<br>";
				die();
			}
	 	}
	}

} //try
catch (PDOException $e)
{
	// caso ocorra uma exceÃ§Ã£o, exibe na tela
	echo "Erro!: " . $e->getMessage() . "<br>";
	die();
}

?>

