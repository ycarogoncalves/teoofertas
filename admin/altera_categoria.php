<?php

require_once("authSession.php");

require_once ("../conf/confbd.php");

// instancia objeto PDO, conectando no mysql
$conexao = conn_mysql();

if(isset($_POST['Ativar'])){
    $lixeira = 0;
    $id_categoria = $_POST['Ativar'];
}
if(isset($_POST['Desativar'])){
    $lixeira = 1;
    $id_categoria = $_POST['Desativar'];
}

if(isset($_POST['Ativar']) || isset($_POST['Desativar'])){

  try{
      $SQLUpdate = "UPDATE categoria set lixeira=".$lixeira." WHERE idcategoria='".$id_categoria."';";
      // $update = mysqli_query($conexao,$SQLUpdate);

      $operacao = $conexao->prepare($SQLUpdate);            
      $update = $operacao->execute();

      if($update){
              echo '<script type="text/javascript">alert("Categoria alterada com sucesso.")</script>';
              echo "<script>location.href='listaCategoria.php';</script>";      
      }
      else{
              echo '<script type="text/javascript">alert("Erro ao excluir categoria.")</script>';
              echo "<script>location.href='listaCategoria.php';</script>";         
      }      
  } //try
  catch (PDOException $e)
  {
      echo "Erro!: " . $e->getMessage() . "<br>";
      die();
  }

}
    $idcategoria = $_POST["editar"];
                  
    try{
    // instrução SQL básica 
    $SQLSelect = "SELECT * from categoria where idcategoria = '".$idcategoria."';";
    
    // $resultados = mysqli_query($conexao,$SQLSelect);
    // prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if ( !empty($resultados) ){  
        foreach($resultados as $dadosEncontrados){ 
           $idcategoria=$dadosEncontrados['idcategoria'];        
           $descricaoCategoria=$dadosEncontrados['descricaoCategoria'];                     
           $icone=$dadosEncontrados['icone'];
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  


<?php require_once("head.php"); ?>

<body>

<style>
    .divs_checks{
        display:inline-block;
        margin-bottom:10px;
    }
    .divs_checks label{
        margin-right:7px;
    }
</style>

<div class="container">
    
<?php require_once("navbar.php"); ?>

<h2 class="page-header">Editando Categoria</h2>

  <form name="form" id="form"  method="post" action="update_categoria.php">


      <input type="hidden" name="idcategoria" value="<?php echo $idcategoria; ?>" />

      <div class="form-group">
        <label for="descricaoCategoria">Descrição</label>
        <input type="text" class="form-control" name="descricaoCategoria" id="descricaoCategoria" placeholder="Nome da Categoria" value="<?php echo $descricaoCategoria; ?>" >
      </div>

    <div class="form-group">
        <label for="icone">Ícone</label>
        <input type="text" class="form-control" name="icone" id="icone" placeholder="Ícone" value="<?php echo $icone; ?>">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-info" name="signup">Cadastrar</button> 
    </div>


  </form>
</div>
   
<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
         
       }
    		});

});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>
