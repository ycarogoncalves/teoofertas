<?php

session_start();

require_once("authSession.php");

require_once ("../conf/confbd.php");

// instancia objeto PDO, conectando no mysql
$conexao = conn_mysql();

    $idusuario = $_SESSION['idUser'];
                  
    try{
    // instrução SQL básica 
    $SQLSelect = "SELECT * from usuario where idusuario = '".$idusuario."';";
    
    // $resultados = mysqli_query($conexao,$SQLSelect);

    // prepara a execução da sentença
    $operacao = $conexao->prepare($SQLSelect);    
        
    $pesquisar = $operacao->execute();
    
    //captura TODOS os resultados obtidos
    $resultados = $operacao->fetchAll();

    // se há resultados, os escreve em uma tabela
    if ( !empty($resultados) ){  
        foreach($resultados as $dadosEncontrados){ 
           $nome=$dadosEncontrados['nomeUsuario'];                     
           $emailUsuario=$dadosEncontrados['emailUsuario'];
        }
    }

    else{
      echo'<div class="starter-template">';
      echo"\n<h3 class=\sub-header\>Dados não encontrados.</h3>";
      echo'</div>';
    }
  } //try
  catch (PDOException $e)
  {
    // caso ocorra uma exceção, exibe na tela
    echo "Erro!: " . $e->getMessage() . "<br>";
    die();
  }
?>  


<?php require_once("head.php"); ?>

<body>

<div class="container">
    
<?php require_once("navbar.php"); ?>

<h2 class="page-header">Editando Dados Pessoais</h2>

  <form name="form" id="form"  method="post" action="update_usuario.php">

      <input type="hidden" name="dadosPessoais" value="1" />
      <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>" />

      <div class="form-group">
        <label for="nome">Nome Usuário</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome do Supermercado" value="<?php echo $nome; ?>" >
      </div>

    <div class="form-group">
        <label for="endereco">E-mail</label>
        <input type="text" class="form-control" name="email" id="email" placeholder="E-mail" value="<?php echo $emailUsuario; ?>">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-info" name="signup">Atualizar Dados</button> 
    </div>


  </form>
</div>
   
<script type="text/javascript">

jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
         
       }
    		});

});


</script>

    <?php  include('footer.php'); ?>
    
  </body>
</html>
