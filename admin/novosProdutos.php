<?php 

session_start();

if(isset($_GET['carregar'])){

unset($_SESSION['totalProdutosAfter']);

include('../conf/confbd.php');


		// inicio
	 $i = 7890000000000;
	 $f = 7900000000010;
	 //fim
	 //$f = 7900000000000;


        // instancia objeto PDO, conectando no mysql
        $conexao = conn_mysql();    

       	while($i < $f) { 	

		    try{

			    // instru��o SQL b�sica 
			    $SQLSelect = "SELECT * from produto where barras='$i'";

			    //prepara a execu��o da senten�a
			    $operacao = $conexao->prepare($SQLSelect);      
			            
			    $pesquisar = $operacao->execute();
			    
			    //captura TODOS os resultados obtidos
			    $resultados = $operacao->fetchAll();

			    // se h� resultados, os escreve em uma tabela
			    if (count($resultados)==0){  

						$url = "http://cosmos.bluesoft.com.br/produto/".$i."";

						$site = file_get_contents($url);

						//Separa a pagina pela tag h1
						@$arraySite = explode("h1", $site);
					
						if(!empty($arraySite[0])){

							@$arrayURL= explode('img', $arraySite[0]);
							@$url= explode('"', $arrayURL[2]);
							$url = $url[7];

							//Captura dados do produto usando o delimitador '>'
							@$filter = explode(">", $arraySite[1]);

							@$produto = explode("<", $filter[1]);
						
							// captura a descri��o do produto
							@$desc = $produto[0];

							if ((isset($desc)) || (empty($desc)) || ($desc == "") || ($desc == " ")) {
					 
								try
								{
							   		// cria instru��o SQL parametrizada
									$SQLInsert = 'INSERT INTO produto (barras,descricaoProduto,imagemProduto) VALUES (?,?,?)';
												  
									//prepara a execu��o
									$operacao = $conexao->prepare($SQLInsert);					  
									
									//executa a senten��o SQL com os parametros passados por um vetor
									$inserir = $operacao->execute(array($i,trim($desc),$url));
								} //try
								catch (PDOException $e)
								{
								    // caso ocorra uma exce��o, exibe na tela
								    echo "Erro!: " . $e->getMessage() . "<br>";
								    die();
								}
							

							}

						}
				}

			}//try
		    catch (PDOException $e)
		    {
		    // caso ocorra uma exce��o, exibe na tela
		    echo "Erro!: " . $e->getMessage() . "<br>";
		    die();
		    }	

		    $i++;
		    
		}

		try{

			$SQLSelect = "SELECT count(*) as totalProdutos FROM `produto`;";

			$operacao = $conexao->prepare($SQLSelect);		
			$pesquisar = $operacao->execute();
			$resultados = $operacao->fetchAll();
			if (count($resultados)>0){	
				foreach($resultados as $dadosEncontrados){
					$totalProdutos = $dadosEncontrados['totalProdutos'];
				}
			}
			else{

			}
		} //try
		catch (PDOException $e)
		{
	    // caso ocorra uma exce��o, exibe na tela
	    echo "Erro!: " . $e->getMessage() . "<br>";
	    die();
		}

	    $conexao = NULL;

	    $_SESSION['totalProdutosAfter']=$totalProdutos;

	    unset($_GET['carregar']);

		echo '<script language="javascript">';
		echo utf8_decode('alert("Banco de dados atualizado com sucesso.")');
		echo '</script>';
		$href = '<script language="javascript">location.href="novosProdutos.php";</script>';
		echo $href;	
}
else{

session_start();

require_once("../conf/confBD.php");

		$conexao = conn_mysql();	

		try{

			$SQLSelect = "SELECT count(*) as totalProdutos FROM `produto`;";

			$operacao = $conexao->prepare($SQLSelect);		
			$pesquisar = $operacao->execute();
			$resultados = $operacao->fetchAll();
			if (count($resultados)>0){	
				foreach($resultados as $dadosEncontrados){
					$totalProdutos = $dadosEncontrados['totalProdutos'];
				}
			}
		} //try
		catch (PDOException $e)
		{
	    // caso ocorra uma exce��o, exibe na tela
	    echo "Erro!: " . $e->getMessage() . "<br>";
	    die();
		}

	    $conexao = NULL;

	    $_SESSION['totalProdutosBefore']=$totalProdutos;


?>
<html>

<?php require_once("head.php"); ?>

<body>

<div class="container">
    
<div class="col-md-12">


<h2 class="titleH2"><?php echo utf8_encode("Importa��o de produtos"); ?></h2>

<br>

<?php 

if(isset($_SESSION['totalProdutosAfter'])){

	$totalAfter=$_SESSION['totalProdutosAfter']-$_SESSION['totalProdutosBefore'];
	echo utf8_encode("<h3>Total de produtos inseridos ap�s a �ltima importa��o: $totalAfter produtos</h3>");
}

?>
<p>Este procedimento pode demorar alguns minutos</p>

<div id="area"></div>

<?php require_once("navbar.php"); ?>

<div id="hideDiv">
 
<form id="formCad" method="get" action="novosProdutos.php" enctype="multipart/form-data">

  <div class="form-group">
      <button type="submit" class="btn btn-info" name="carregar" id="carregar">Importar novos produtos</button> 
  </div>

</form>

</div>

</div>
</div>


<script type="text/javascript">


$( "#carregar" ).click(function() {
  
  $("#hideDiv").css("display", "none");

  $("#area").html("<img src='../images/loading.gif' width='200' height='200' />");
});


jQuery(function($){
   				
    $('#formCad').bootstrapValidator({
//      live: 'disabled',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
            endereco: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                              
            telefones: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },
             horarioFuncionamento: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },  
            imagemSupermercado: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, preencha este campo.'
                    }
                }
            },                                       
          
       }
    });
});


</script>

    <?php  include('footer.php'); ?>
  </body>
</html>

<?php
}
?>
