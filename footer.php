<!-- BEGIN # MODAL LOGIN -->

  <div class="modal fade" id="modalProdutos" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div id="bodyModal" class="modal-body">
          <p>Carregando... <img src='images/ajax-loader.gif' alt='' /></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" align="center">
					<img class="img-circle" id="img_logo" src="images/BLACKPRICE100x100.png">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</button>
				</div>
                
                <!-- Begin # DIV Form -->
                <div id="div-forms">
                
                    <!-- Begin # Login Form -->
                    <form id="login-form" action="login.php" method="post">
		                <div class="modal-body">
				    		<div id="div-login-msg">
                                <div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-login-msg">Digite seu e-mail e senha.</span>
                            </div>
				    		<input id="login_username" class="form-control" type="text" name="email" placeholder="E-mail" required>
				    		<input id="login_password" class="form-control" type="password" name="senha" placeholder="Senha" required>
                            <div class="checkbox">
                                <!--<label>
                                    <input type="checkbox"> Remember me
                                </label>-->
                            </div>
        		    	</div>
				        <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
                            </div>
				    	    <div>
                                <button id="login_lost_btn" type="button" class="btn btn-link">Esqueceu a senha?</button>
                                <button id="login_register_btn" type="button" class="btn btn-link">Registrar</button>
                            </div>
				        </div>
                    </form>
                    <!-- End # Login Form -->
                    
                    <!-- Begin | Lost Password Form -->
                    <form id="lost-form" style="display:none;">
    	    		    <div class="modal-body">
		    				<div id="div-lost-msg">
                                <div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-lost-msg">Digite seu e-mail.</span>
                            </div>
		    				<input id="lost_email" class="form-control" type="text" placeholder="E-mail" required>
            			</div>
		    		    <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
                            </div>
                            <div>
                                <button id="lost_login_btn" type="button" class="btn btn-link">Log In</button>
                                <button id="lost_register_btn" type="button" class="btn btn-link">Registrar</button>
                            </div>
		    		    </div>
                    </form>
                    <!-- End | Lost Password Form -->
                    
                    <!-- Begin | Register Form -->
                    <form id="register-form" style="display:none;" action="gravar_usuario.php" method="post">
            		    <div class="modal-body">
		    				<div id="div-register-msg">
                                <div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
                                <span id="text-register-msg">Crie uma nova conta.</span>
                            </div>
		    				<input id="register_username" class="form-control" type="text" name="nome" placeholder="Nome" required>
                            <input id="register_email" class="form-control" type="text" name="email" placeholder="E-mail" required>
                            <input class="form-control" type="text" name="confemail" placeholder="Confirmar e-mail" required>
                            <input id="register_password" class="form-control" type="password" name="senha" placeholder="Senha">
                            <input class="form-control" type="password" name="confsenha" placeholder="Confirmar senha">
                            <meter max="4" id="password-strength-meter"></meter>
                            <p id="password-strength-text"></p>                            

            			</div>
		    		    <div class="modal-footer">
                            <div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Registrar</button>
                            </div>
                            <div>
                                <button id="register_login_btn" type="button" class="btn btn-link">Log In</button>
                                <button id="register_lost_btn" type="button" class="btn btn-link">Esqueceu a senha?</button>
                            </div>
		    		    </div>
                    </form>
                    <!-- End | Register Form -->
                    
                </div>
                <!-- End # DIV Form -->
                
			</div>
		</div>
	</div>
    <!-- END # MODAL LOGIN -->

    <script type="text/javascript">



var strength = {
        0: "Péssima ☹",
        1: "Ruim ☹",
        2: "Fraca ☹",
        3: "Boa ☺",
        4: "Forte ☻"
}

var password = document.getElementById('register_password');
var meter = document.getElementById('password-strength-meter');
var text = document.getElementById('password-strength-text');

password.addEventListener('input', function()
{
    var val = password.value;
    var result = zxcvbn(val);
    
    // Update the password strength meter
    meter.value = result.score;
   
    // Update the text indicator
    if(val !== "") {
        text.innerHTML = "Senha: " + "<strong>" + strength[result.score] + "</strong>"; 
    }
    else {
        text.innerHTML = "";
    }
});    


        function loadOfertas(produto) {

            var produto = $('#query').val();

            produto = produto.replace(/ /gi, "%20");

            var tam = produto.length;
            if(tam > 3){
                $('#produtosOferta').load('loadOfertas.php?produto='+produto);
            }

            if(produto==''){
                $('#produtosOferta').load('loadOfertas.php?produto='+produto);
            }

        }
    </script>


    <script type="text/javascript">


function addtoCart(id){
    var logado ='<?=$_SESSION['auth']?>';

    if(logado){
        var promo = document.getElementById('item_'+id);

        if ($(promo).hasClass( "fa-star-o" )){   
                    $(promo).fadeOut();    
                            $(promo).removeClass( "fa-star-o" );
                            $(promo).fadeIn('slow');     
                            $(promo).addClass( "fa-star" );

                $.post("addtoCart.php",
                {
                    add: "1",
                    idOferta: id
                },
                function(data, status){
                    // alert("Data: " + data + "\nStatus: " + status);
                });         
                                                
        }
        else{
            
                    if (confirm("Deseja remover este produto do carrinho?")) {
                                $(promo).fadeOut();                              
                                        $(promo).removeClass( "fa-star" );  
                                        $(promo).fadeIn('slow');     
                                        $(promo).addClass( "fa-star-o" );

                            $.post("addtoCart.php",
                            {
                                remover: "1",
                                idOferta: id
                            },
                            function(data, status){
                                //  alert("Data: " + data + "\nStatus: " + status);
                            });
                            }

            }
    }
    else{
        alert('Adicione promoções aos favoritos (Em breve).');
    }
}

    </script>



 <script type="text/javascript">
   
$(function() {
        var $formLogin = $('#login-form');
    var $formLost = $('#lost-form');
    var $formRegister = $('#register-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 300;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;

    $("#login-form").submit(function () {
        switch(this.id) {
            case "login-form":
                var $lg_username=$('#login_username').val();
                var $lg_password=$('#login_password').val();
                if ($lg_username == "ERROR") {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");
                } else {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Login OK");
                }
                break;
            case "lost-form":
                var $ls_email=$('#lost_email').val();
                if ($ls_email == "ERROR") {
                    msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
                } else {
                    msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
                }
                break;
            case "register-form":
                var $rg_username=$('#register_username').val();
                var $rg_email=$('#register_email').val();
                var $rg_password=$('#register_password').val();
                if ($rg_username == "ERROR") {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", "Register error");
                } else {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "success", "glyphicon-ok", "Register OK");
                }
                break;
            default:
                return true;
        }
        return true;
    });
    
    $('#login_register_btn').click( function () { modalAnimate($formLogin, $formRegister) });
    $('#register_login_btn').click( function () { modalAnimate($formRegister, $formLogin); });
    $('#login_lost_btn').click( function () { modalAnimate($formLogin, $formLost); });
    $('#lost_login_btn').click( function () { modalAnimate($formLost, $formLogin); });
    $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); });
    $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); });
    
    function modalAnimate ($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height",$oldH);
        $oldForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $newH}, $modalAnimateTime, function(){
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }
    
    function msgFade ($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function() {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

});


 </script>
