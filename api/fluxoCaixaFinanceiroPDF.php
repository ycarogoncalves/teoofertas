<?php
require_once("authSession.php");
require_once("../loadClasses.php");

$siglaMoeda = $_SESSION['siglaMoeda'];
function centroCusto( $idcentroCusto ){

    $centroCustoObj = new CentroCusto('centroCusto');
    $centroCustoObj->setDados(array($idcentroCusto, 1));
    $resultado = $centroCustoObj->getListar("AND idcentroCusto = ? AND status = ?");
    $centroCustoObj->fechar_conexao();
    return $resultado[0]['descricaoCentroCusto'];
}

$ano = date('Y');
$mes = date('m');
$dia = date('d');

if(isset($_GET['format'])){
    $format = $_GET['format'];
}

$dataHoje = date('d/m/Y');

if(isset($_POST['ano'])){
    $ano = $_POST['ano'];
}else{
    $ano = date('Y');
}

function converteData($data, $se, $ss){
    return implode($ss, array_reverse(explode($se, $data)));
}


if( $_GET['datepicker_final']=='null' || !isset($_GET['datepicker_final']) ) {

    $dataInicial = converteData($_GET['datepicker_initial'],'/','-');
    $consulta = "= '".$dataInicial."'";

}else{

    $dataInicial = converteData($_GET['datepicker_initial'],'/','-');
    $dataFinal = converteData($_GET['datepicker_final'],'/','-');
    $consulta = "BETWEEN '".$dataInicial."' and '".$dataFinal."' ";

}

switch ($mes) {
    case "01":    $month = "JANEIRO";     break;
    case "02":    $month = "FEVEREIRO";   break;
    case "03":    $month = "MARÇO";       break;
    case "04":    $month = "ABRIL";       break;
    case "05":    $month = "MAIO";        break;
    case "06":    $month = "JUNHO";       break;
    case "07":    $month = "JULHO";       break;
    case "08":    $month = "AGOSTO";      break;
    case "09":    $month = "SETEMBRO";    break;
    case "10":    $month = "OUTUBRO";     break;
    case "11":    $month = "NOVEMBRO";    break;
    case "12":    $month = "DEZEMBRO";    break;
}


$linhaEmpresa =empresa()[0];
$linhaConf = configuracoes()[0];


if( $siglaMoeda == '€' ){
    $siglaMoeda = chr(128);
}


require_once("../fpdf/fpdf.php");
$pdf=new FPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetMargins(12,12,18);

//Logo
$width = getWidthImageForPdf($linhaConf['dir_logomarca']);

$pdf->Image($linhaConf['dir_logomarca'],22,8,$width);


$pdf->SetFont('Arial', '', 8);
$pdf->Cell(0, 4,$linhaEmpresa['email'], 0,0,'C');
$pdf->SetFont('Arial', 'B', 8);

$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(0, 4, "Telefone: " . $linhaEmpresa['telefone'] . ' ' . $linhaEmpresa['celular'], 0,0,'C');
$pdf->Cell(0, 4,utf8_decode("Data de Emissão: $dataHoje"), 0,0,'R');
$pdf->Ln();

$pdf->Cell(0, 4,utf8_decode(utf8_encode($linhaEmpresa['rua'])) . ', ' . $linhaEmpresa['n_estabelecimento'] . ' - ' . $linhaEmpresa['bairro'], 0,0,'C');
$pdf->Ln();
$pdf->Cell(0, 4,$linhaEmpresa['nome_cidade'], 0,0,'C');
$pdf->Ln(8);


$pdf->SetFont('Arial', 'B', 20);
$pdf->Cell(0, 4, utf8_decode("Fluxo Financeiro Caixa - $ano"), 0,0,'C');

$pdf->Ln(12);
$pdf->SetX(8);
$pdf->SetFont('Arial', '', 12);
$pdf->SetFillColor(19, 165, 85);
$pdf->SetTextColor(255,255,255);
$pdf->Cell(50, 5, "Cliente", 1,0,'C',true);
$pdf->Cell(30, 5, "Status", 1,0,'C',true);
$pdf->Cell(45, 5, "Centro Custo", 1,0,'C',true);
$pdf->Cell(50, 5, utf8_decode("Descrição"), 1,0,'C',true);
$pdf->Cell(28, 5, "Data", 1,0,'C',true);
$pdf->Cell(39, 5, "Forma Pagamento", 1,0,'C',true);
$pdf->Cell(37, 5, "Valor", 1,0,'C',true);
$pdf->Ln();

$html=utf8_decode("
			    <table width='100%' border='1'>
			      <tr>
			    	 <td bgcolor='#13a555'><b>Cliente</b></td>     
			         <td bgcolor='#13a555'><b>Status</b></td>
                     <td bgcolor='#13a555'><b>Centro custo</b></td>
					 <td bgcolor='#13a555'><b>Descrição</b></td>
					 <td bgcolor='#13a555'><b>Data</b></td>
					 <td bgcolor='#13a555'><b>Forma de Pagamento</b></td>
			       	 <td bgcolor='#13a555'><b>Valor</b></td>
			      </tr>
			");

$totalRecebimentoslRealizados = 0;

//RECEBIMENTOS MÊS ATUAL REALIZADO
$movimentacao_obj = new Movimentacao('movimentacao');
$movimentacao_obj->setDados(array(1,1));
$linhaTotal = $movimentacao_obj->getListarComTabelas("and movimentacao=? and 
    cliente_idcliente=cliente.idcliente and usuario_login=usuario.login and pago=? and 
    dataMovimentacao $consulta",array('cliente','usuario'),
    'sum(valor) as total');
$movimentacao_obj->fechar_conexao();
$totalRecebimentoslRealizados+=$linhaTotal[0]['total'];


$aluguel_obj = new Aluguel('aluguel_tem_formaPagamento');
$aluguel_obj->setDados(array(1));
$linhaTotal = $aluguel_obj->getListarComTabelas("and aluguel_tem_formaPagamento.confirmado=? and 
      aluguel_tem_formaPagamento.aluguel_idaluguel=aluguel.idaluguel and 
      aluguel_tem_formaPagamento.parcelas_idparcelas=parcelas.idparcelas and 
      aluguel_tem_formaPagamento.formaPagamento_idformaPagamento=formaPagamento.idformaPagamento and 
      aluguel.cliente_idcliente=cliente.idcliente and data_entrada $consulta",
    array('parcelas','formaPagamento','aluguel','cliente'),
    'sum(valor) as total');
$aluguel_obj->fechar_conexao();
$totalRecebimentoslRealizados+=$linhaTotal[0]['total'];

//DESPESAS REALIZADAS

$movimentacao_obj = new Movimentacao('movimentacao');
$movimentacao_obj->setDados(array(0,1));
$linhaTotal = $movimentacao_obj->getListarComTabelas("and movimentacao=? and 
        cliente_idcliente=cliente.idcliente and usuario_login=usuario.login and pago=? and 
        dataMovimentacao $consulta",array('cliente','usuario'),'sum(valor) as total');
$movimentacao_obj->fechar_conexao();
$totalDespesasRealizadas=$linhaTotal[0]['total'];


//Recebimentos DataTable

$movimentacao_obj = new Movimentacao('movimentacao');
$movimentacao_obj->setDados(array(1,1));
$resultadoTimeline = $movimentacao_obj->query_listar("SELECT movimentacao,idmovimentacao,nome,valor,
    dataMovimentacao as dataOrdenacao,date_format(dataMovimentacao,'%d/%m/%Y') as data,centroCusto_idcentroCusto,  descMovimentacao, NULL as formaPagamento
    FROM movimentacao,cliente,`usuario` WHERE pago=? and cliente_idcliente=cliente.idcliente and 
    usuario_login=usuario.login and dataMovimentacao $consulta
    UNION
    SELECT 1,idaluguel_tem_formaPagamento,nome,valor,data_entrada as dataOrdenacao,
    date_format(data_entrada,'%d/%m/%Y') as data,confirmado,NULL,descricao
    FROM aluguel_tem_formaPagamento,parcelas,
    formaPagamento,aluguel,cliente WHERE aluguel_tem_formaPagamento.confirmado=? and 
    aluguel_tem_formaPagamento.aluguel_idaluguel=aluguel.idaluguel and 
    aluguel_tem_formaPagamento.parcelas_idparcelas=parcelas.idparcelas and 
    aluguel_tem_formaPagamento.formaPagamento_idformaPagamento=formaPagamento.idformaPagamento and 
    aluguel.cliente_idcliente=cliente.idcliente and data_entrada $consulta order by dataOrdenacao ASC");
$movimentacao_obj->fechar_conexao();
$numrows = $movimentacao_obj->getQtde();

foreach ($resultadoTimeline as $linha){

    $descricao = utf8_decode($linha['descMovimentacao']);
    if($linha['descMovimentacao']==NULL) {
        $descricao=utf8_decode("Parcela de Locação");
    }

    if($linha['movimentacao']==0){
        $descTimeline="Despesa";

    }else{
        $descTimeline="Recebimento";
    }

    $centroCusto = '-------';
    if( !empty($linha['centroCusto_idcentroCusto']) ){
        $centroCusto = centroCusto( $linha['centroCusto_idcentroCusto'] );
    } 
    

    //Totais
    $pdf->SetX(8);
    $pdf->SetFont('Arial', '', 6);
    $pdf->SetTextColor(0,0,0);
    $pdf->Cell(50, 5, utf8_decode($linha['nome']), 1,0,'C');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 5, utf8_decode($descTimeline), 1,0,'C');
    $pdf->Cell(45, 5, utf8_decode($centroCusto), 1,0,'C');
    $pdf->Cell(50, 5, $descricao, 1,0,'C');
    $pdf->Cell(28, 5, $linha['data'], 1,0,'C');
    $pdf->Cell(39, 5, utf8_decode(isset($linha['formaPagamento']) ? $linha['formaPagamento'] : '------------'), 1,0,'C');
    $pdf->Cell(37, 5, $siglaMoeda." " . number_format($linha['valor'], 2, ',', '.'), 1,0,'C');
    $pdf->Ln();

    $html.="
			      <tr>
			         <td><b>".utf8_decode($linha['nome'])."</b></td>
			    	 <td><b>" .utf8_decode($descTimeline). "</b></td>
                     <td><b>" .utf8_decode($centroCusto). "</b></td>
			    	 <td><b>". $descricao. "</b></td>
					 <td><b>" . $linha['data'] ."</b></td>
					 <td><b>" . utf8_decode($linha['formaPagamento']) ."</b></td>
			    	 <td><b>" .$siglaMoeda." ".number_format($linha['valor'], 2, ',', '.')."</b></td>
			      </tr>
			";



}
$balanco=$totalRecebimentoslRealizados-$totalDespesasRealizadas;
$pdf->SetFont('Arial', 'B', 14);
$pdf->Ln();
$pdf->Cell(20, 4,"", 0,0,'L');
$pdf->Cell(85, 4,utf8_decode("TOTAL DOS RECEBIMENTOS"), 0,0,'L');
$pdf->Cell(80, 4,utf8_decode("TOTAL DAS DESPESAS"), 0,0,'L');
$pdf->Cell(80, 4,utf8_decode("FECHAMENTO DO CAIXA"), 0,0,'L');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Ln(1);
$pdf->Cell(20, 4,"", 0,0,'L');
$pdf->Cell(85, 4,$siglaMoeda." ".number_format($totalRecebimentoslRealizados, 2, ',', '.'), 0,0,'L');
$pdf->Cell(80, 4,$siglaMoeda." ".number_format($totalDespesasRealizadas, 2, ',', '.'), 0,0,'L');
$pdf->Cell(80, 4,$siglaMoeda." ".number_format($balanco, 2, ',', '.'), 0,0,'L');

$html.="
			<tr>
			 <td></td>
			 <td></td>
			 <td></td>
	         <td><b> TOTAL DOS RECEBIMENTOS: ".$siglaMoeda." ".number_format($totalRecebimentoslRealizados, 2, ',', '.')."</b></td>
	    	 <td><b>TOTAL DAS DESPESAS: ".$siglaMoeda." ".number_format($totalDespesasRealizadas, 2, ',', '.'). "</b></td>
	       	 <td><b>FECHAMENTO DO CAIXA: ".$siglaMoeda." ". number_format($balanco, 2, ',', '.')."</b></td>
		    </tr>
		    </table>";

if($format==1){
    $pdf->Output("FluxoDeCaixa-$ano.pdf",'I');
}else{
    // Determina que o arquivo é uma planilha do Excel
    header("Content-type: application/vnd.ms-excel");

    // Força o download do arquivo
    header("Content-type: application/force-download");

    // Seta o nome do arquivo
    header("Content-Disposition: attachment; filename=fluxoDeCaixa-$ano.xls");

    header("Pragma: no-cache");
    // Imprime o conteúdo da nossa tabela no arquivo que será gerado
    echo $html;
}

