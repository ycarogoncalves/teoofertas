<?php
header("Access-Control-Allow-Origin: *");

include('../conf/confbd.php');

$datahoje=date('Y-m-d');

if( isset($_GET['idSupermecado']) ){

} elseif ( isset($_GET['idProduto']) ) {
	
	$idProduto = $_GET['idProduto'];

	$conexao = conn_mysql();

	$SQLSelect = "
	    SELECT *,
	        if(precoOferta is not null,'1','0') as tipo,
	        date_format(dataFinal,'%Y/%m/%d') as dataTermino,
	        nomeSupermercado,
	        DATEDIFF('$datahoje', dataFinal) as datediff
	    from 
	        publicaOferta 
	        inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
	        inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
	    where
	        dataFinal >= '$datahoje' 
	        and status= 1 
	        and publicaOferta.idpublicaOferta = $idProduto 
	        and dataInicial <= '$datahoje'
	        group by idpublicaOferta
	        order by qtde_cliques,precoOferta
	        ";   
	$query = $conexao->prepare($SQLSelect);
    $result = $query->execute();
    $resultados = $query->fetchAll();

    $produtos = array();

    foreach( $resultados as $key => $r ){
    	$produtos[$key]['idProduto'] = $r['idpublicaOferta']; 
    	$descricao =  utf8_encode($r['desc_promocao']);
    	$produtos[$key]['descricao'] =  $descricao;
    	$produtos[$key]['preco'] = $r['precoOferta']; 
    	$produtos[$key]['dataInicial'] = $r['dataInicial']; 
    	$produtos[$key]['dataFinal'] = $r['dataFinal']; 
    	$produtos[$key]['id_supermercado'] = $r['supermercado_idsupermercado']; 
    	$produtos[$key]['imagem'] = $r['banner_promocao']; 
    	$produtos[$key]['imagemSupermercado'] = $r['imagemSupermercado']; 
    }

    $array_produtos = array();

    $array_produtos = json_encode($produtos);

    // echo "<pre>";
    // echo $array_produtos;
    // echo "</pre>";
    print_r($array_produtos);

} else {

	$conexao = conn_mysql();

	$SQLSelect = "
	    SELECT *,
	        if(precoOferta is not null,'1','0') as tipo,
	        date_format(dataFinal,'%Y/%m/%d') as dataTermino,
	        nomeSupermercado,
	        DATEDIFF('$datahoje', dataFinal) as datediff
	    from 
	        publicaOferta 
	        inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
	        inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
	    where
	        dataFinal >= '$datahoje' 
	        and status= 1 
	        and dataInicial <= '$datahoje'
	        group by idpublicaOferta
	        order by qtde_cliques,precoOferta
	        ";   
	$query = $conexao->prepare($SQLSelect);
    $result = $query->execute();
    $resultados = $query->fetchAll();

    $produtos = array();

    foreach( $resultados as $key => $r ){
    	$produtos[$key]['idProduto'] = $r['idpublicaOferta']; 
    	$descricao = utf8_encode($r['desc_promocao']);
    	$produtos[$key]['descricao'] =  $descricao;
    	$produtos[$key]['preco'] = $r['precoOferta']; 
    	$produtos[$key]['dataInicial'] = $r['dataInicial']; 
    	$produtos[$key]['dataFinal'] = $r['dataFinal']; 
    	$produtos[$key]['id_supermercado'] = $r['supermercado_idsupermercado']; 
    	$produtos[$key]['nomeSupermercado'] = $r['nomeSupermercado']; 

    	if( strpos($r['banner_promocao'], 'cdn-cosmos') > 0 ){
            $file_headers = @get_headers($r['banner_promocao']);
            if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
               $produtos[$key]['imagem'] = 'http://appteoofertas.klouro.com.br/images/promocoes/produto_defalt.png';
            } else {
            	$produtos[$key]['imagem'] = $r['banner_promocao'];
            }
        }  else {
        	$produtos[$key]['imagem'] = 'http://appteoofertas.klouro.com.br/images/promocoes/'.$r['banner_promocao'];
        }

        $produtos[$key]['imagemSupermercado'] = "http://appteoofertas.klouro.com.br/".$r['imagemSupermercado']; 

    }

    $array_produtos = array();

    $array_produtos = json_encode($produtos);
    // echo "<pre>";
    // print_r($resultados);
    // // echo $array_produtos;
    // echo "</pre>";
    print_r($array_produtos);

}

