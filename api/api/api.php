<?php

    require_once('../vendor/autoload.php');
    require_once('./includes/confBD.php');
    // require_once('./includes/JWTWrapper.php');
    

    use Siler\GraphQL;
    use Siler\Http\Request;
    use Siler\Http\Response;

    // Enable CORS
    Response\header('Access-Control-Allow-Origin', '*');
    Response\header('Access-Control-Allow-Headers', '*');
    // Response\header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');

    // Respond only for POST requests
    if (Request\method_is('post')) {
        // Retrive the Schema
        $schema = include __DIR__.'/schema.php';

        // Give it to siler
        GraphQL\init($schema);
    }

?>