<?php

    use Siler\GraphQL;

    GraphQL\subscriptions_at('ws://127.0.0.1:3000');

    $queryType = [
        'allOfertas' => $allOfertas      
    ];

    $mutationType = [
        // 'cadastrarNota' => $cadastrarNota
    //     'editCliente' => $editCliente,
    //     'desativarProduto' => $desativarProduto,
    //     'deleteProduto' => $deleteProduto,
    //     'createProduto' => $createProduto,
    //     'editProduto' => $editProduto,
    //     'confirmarRetirada' => $confirmarRetirada,
    //     'createPedido' => $createPedido,
    //     'editPedido' => $editPedido,
    //     'devolverPedido' => $devolverPedido,
    //     'setRegistrarDispositivo' => $setRegistrarDispositivo
    ];

    $subscriptionType = [
    //     'getClienteSub' => $getClienteSub,
        // 'getProdutoSub' => $getProdutoSub
    //     'getPedidoSub' => $getPedidoSub,
    //     'getDevolucaoSub' => $getDevolucaoSub
    ];

    return [
        // 'Oferta' => $ofertasType,
        'Query'    => $queryType,
        'Mutation' => $mutationType,
        'Subscription' => $subscriptionType
    ];
?>
