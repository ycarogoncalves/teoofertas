<?php
    use \Firebase\JWT\JWT;
    
    /**
     * Gerenciamento de tokens JWT
     */
    class JWTWrapper {
        const KEY = '}>GxH*3M?e_T';
    
        // Geracao de um novo token jwt
        public static function encode($options) {
            $issuedAt = time();
            // $expire = $issuedAt + 3600; // tempo de expiracao do token
    
            $token = [
                'iat'  => $issuedAt,            // timestamp de geracao do token
                'iss'  => 'http://www.klouro.com.br',      // dominio, pode ser usado para descartar tokens de outros dominios
                // 'exp'  => $expire,              // expiracao do token
                'nbf'  => $issuedAt - 1,        // token nao eh valido Antes de
                'id' => $options, // Dados do usuario logado
            ];
    
            return JWT::encode($token, self::KEY);
        }
    
        // Decodifica token jwt
        public static function decode($jwt){
            return JWT::decode($jwt, self::KEY, ['HS256']);
        }

        public function extractTokenHeaders() {
            try {
                $header = apache_request_headers(); 
                $extract = '';
                foreach ($header as $headers => $value) {
                    if($headers=='Authorization') {
                        $extract = explode(' ', $value)[1];
                    }
                } 
                return $extract;
            }
            catch(Exception $e) {
                return false;
            }
        }

        public function validateTokenForResolvers() {
            try {
                $token = self::extractTokenHeaders();

                $resp = self::decode($token);
                $options = $resp->id;
                /*return [
                    'id' => $resp->id, 
                    'isValid' => true
                ];*/
                return true;
            }
            catch(Exception $e) {
                return false;
            }
        }
    }
?>