<?php

    use Siler\GraphQL;
 
    include __DIR__.'/produtos/produtos-resolvers.php';
    // include __DIR__.'/notas/notas-resolvers.php';
    
    $typeDefs = file_get_contents(__DIR__.'/schema.graphql');
    $resolvers = include __DIR__.'/resolvers.php';

    return GraphQL\schema($typeDefs, $resolvers);

?>