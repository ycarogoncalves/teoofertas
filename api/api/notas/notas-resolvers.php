<?php
    
    use Siler\GraphQL;

    $cadastrarNota = function( $root, $args ){

        // $generateTokenObj = new JWTWrapper();
        // $canShow = $generateTokenObj->validateTokenForResolvers();
        
        // if($canShow){

            $inserts = 0;
            $j=1;
            $i=1;

            $url_nota = $args['url'];
            $url = str_replace('https','http',$url_nota);
            
            $arrayChaveAcesso=explode('=', $url);
            $arrayChaveAcesso=explode('|', $arrayChaveAcesso[1]);
            $chaveAcesso=$arrayChaveAcesso[0];

            $conexao = conn_mysql();

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );  

            $html = file_get_contents($url, false, stream_context_create($arrContextOptions));
            preg_match_all("/<td(.*)([^<]+)/", $html, $matches);
            preg_match_all("/Código(.*)/", $html, $matchesCodigos);

            preg_match("/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/", $html, $dataCompra);
            $dataCompra=explode(' ', $dataCompra[0]);
            $horaCompra=$dataCompra[1];
            $dataCompra=$dataCompra[0];
            $invertaData=array_reverse(explode('/', $dataCompra));
            $dataCompra=implode("-", $invertaData) . ' ' . $horaCompra;

            foreach ($matchesCodigos[0] as $key => $value) {
                $arrayCode = explode(")", $value);
                $arrayCode = explode(":",$arrayCode[0]);		
                $codes[$j] = trim($arrayCode[1]);
                $j++;
            }

            foreach ($matches[0] as $key => $value) {
                $value = strip_tags($value);
                if($i==1){
                    $empresa = explode(",", $value);
                    $empresa = explode(":",$empresa[0]);
                    $cnpj =  trim($empresa[1]);
                    $sql = " INSERT INTO notas_inseridas (idnotas_inseridas, cnpj, chaveAcesso,dataCompra) VALUES (NULL, '$cnpj', '$chaveAcesso','$dataCompra');";
                    $operacao = $conexao->prepare($sql);
                    $inserted = $operacao->execute();
                    if($inserted){
                        $fkChaveAcesso = $conexao->lastInsertId();
                        
                    }else{
                        return false;
                        break;
                    }
                }
                if($i>2){
                    $value = trim($value);
                    if(strstr($value, ':')){
                        $value = explode(":",$value); $value = $value[1];
                    }
                    $produtos[$i-2]=trim($value);
                    if(strlen($value)<1) {
                        break;
                    }
                }
                $i++;
            }

            //Verificando se CNPJ ja esta cadastrado e pega o id da empresa
            $sql = "SELECT * from empresas where cnpj = '$cnpj'";
                
            $query = $conexao->prepare($sql);
            $result = $query->execute();    
            $resultado = $query->fetchAll(); 

            if( !empty($resultado) ){
                $id_empresa = $resultado[0]['id_empresa'];
            } else {
                $cnpj_trado = str_replace("/","",$cnpj);
                $cnpj_trado = str_replace(".","",$cnpj_trado);
                $cnpj_trado = str_replace("-","",$cnpj_trado);

                $url_cnpj = 'https://www.receitaws.com.br/v1/cnpj/'.$cnpj_trado;

                $data = json_decode(file_get_contents($url_cnpj));

                $nome_empresa = $data->nome;
                $nome_fantasia = $data->fantasia;
                $cidade = $data->municipio;
            

                $sql = "INSERT INTO empresas (descricao,nome_fantasia, cnpj) VALUES ('$nome_empresa','$nome_fantasia', '$cnpj');";
                $operacao = $conexao->prepare($sql);
                $inserted = $operacao->execute();

                $id_empresa = $conexao->lastInsertId();
            }
            
            $j=1;
            for($i=1;$i<sizeof($produtos);$i++){
                $codigoProduto = $codes[$j];
                $nomeProduto = $produtos[$i];
                $qtdeProduto = $produtos[$i+1];
                $medidaProduto = $produtos[$i+2];
                $valorTotal = str_replace("R$ ", "", $produtos[$i+3]);
                $valorTotal = str_replace(",", ".", $valorTotal);
                
                $url_imagem ="http://appteoofertas.klouro.com.br/images/promocoes/produto_defalt.png";
                // $urlImagem = "http://cosmos.bluesoft.com.br/produtos/".$codigoProduto;

                // $file_headers = @get_headers($urlImagem);
                // if( !$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                //     $url_imagem = 'http://appteoofertas.klouro.com.br/images/promocoes/produto_defalt.png';
                    
                // } else {
                //     $site = file_get_contents($urlImagem);

                //     //Separa a pagina pela tag h1
                //     @$arraySite = explode("h1", $site);
                
                //     if( !empty( $arraySite[0] ) ){
                    
                //         @$arrayURL= explode('img', $arraySite[0]);
                //         @$urlImagem= explode('"', $arrayURL[2]);
                //         $urlImagem = $urlImagem[7];

                //         //Captura dados do produto usando o delimitador '>'
                //         @$filter = explode(">", $arraySite[1]);

                //         @$produto = explode("<", $filter[1]);
                    
                //         // captura a descrição do produto
                //         @$desc = $produto[0];

                        
                //         $url_imagem = 'https://cdn-cosmos.bluesoft.com.br/products/'.$codigoProduto;
                //     }
                    
                // }
                $valorOferta = $valorTotal;
                if( $medidaProduto == "UN" ){
                    $valorOferta =  ( (float) $valorTotal / (int)$qtdeProduto );

                    $sql ="INSERT INTO ofertas (id_oferta, descricao_oferta, qtde_oferta, medida_oferta, valor_total, codigo_oferta, nota_id, empresa_id, valor_oferta, imagem_oferta, status) 
                    VALUES (NULL, '$nomeProduto','$qtdeProduto','$medidaProduto','$valorTotal','$codigoProduto',$fkChaveAcesso,$id_empresa,'$valorOferta','$url_imagem',1);";
            
                } else {
                    $sql ="INSERT INTO ofertas (id_oferta, descricao_oferta, qtde_oferta, medida_oferta, valor_total, codigo_oferta, nota_id, empresa_id, valor_oferta, imagem_oferta, status) 
                    VALUES (NULL, '$nomeProduto','$qtdeProduto','$medidaProduto','$valorTotal','$codigoProduto',$fkChaveAcesso,$id_empresa,'$valorOferta','$url_imagem',0);";
                }

                
                $operacao = $conexao->prepare($sql);
                $returnOp = $operacao->execute();
                if($returnOp){
                    $inserts++;
                }
                $i=$i+3;
                $j++;
            }

            $conexao = null;

            return true;
        // }
    }

?>