<?php

  session_start();

  include('conf/confbd.php');

$com_categoria = '';
$outra_categoria='';
if(isset($_GET['cat'])){
    $categoria = $_GET['cat'];
    if(!is_numeric($categoria))
        header('Location:index.php');

    $conexao = conn_mysql();    

    $SQLSelect = "select * from categoria where idcategoria=? and lixeira=0";

    // $categoria_escolhida = mysqli_query($conexao,$SQLSelect);

    $query = $conexao->prepare($SQLSelect);
    $result = $query->execute();
    $categoria_escolhida = $query->fetchAll();


    $conexao = null;

    $com_categoria = 'and id_categoria='.$categoria;
    $outra_categoria = 'and id_categoria<>'.$categoria;
} else {
    $categoria_escolhida = array(
        'idcategoria' => 25,
        'descricaoCategoria' => 'Supermercados',
        'icone' => 'fa-shopping-basket'
    );
}

if( !empty($_SESSION['idUser']) ) {
    $idUser = $_SESSION['idUser'];
} else {
    $idUser = '';
}

$datahoje=date('Y-m-d');

?>

<!DOCTYPE html>
<html lang="pt-br">

<?php include('head.php'); ?>
<link href="css/jquery-ui.min.css" rel="stylesheet">
<body>
    <style>
        .msg{
            text-align:center;
            font-size:13px;
            color:#F00;
            display:inline-block;
            margin-top:30px;
            width:100%;
        }


        #loadingGif{
            display: none;
        }
        .overlay__content {
            top: 50%;
            position: absolute;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 98%;
            /*max-height: 90%;*/
            height: 98%;
            z-index: 1052!important;
        }
        .overlay__close {
            position: absolute;
            right: 0;
            padding: 0.5rem;
            width: 2rem;
            height: 2rem;
            line-height: 2rem;
            text-align: center;
            background-color: white;
            cursor: pointer;
            border: 3px solid #9e9e9e;
            font-size: 1.5rem;
            margin: -1rem;
            border-radius: 2rem;
            z-index: 100;
            box-sizing: content-box;
        }
        .overlay__content video {
            width: 100%;
            height: 100%;
        }
        ul#ui-id-1 {
            z-index: 9999;
            max-height: 160px;
            overflow-y: auto;
        }

    </style>
	<?php 
    include('navbar.php'); 
    ?>

    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('images/banner/banner01.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('images/banner/banner02.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('images/banner/banner03.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('images/banner/banner04.jpg');"></div>
                <div class="carousel-caption">
                    <h3></h3>
                </div>
            </div>            
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-md-6 col-xs-6">
                <div style="margin-top: 20px;">
                    <!-- <label for="supermercados">Selecione</label> -->
                    <select class="btn btn-cinza" id="sup" style="width: 100%;border-radius: 5px; height: 43px;"> 
                    <option value="">Filtrar</option>
                        <?php

                            $conexao = conn_mysql();  

                            $SQLEmpresa = "SELECT idsupermercado,nomeSupermercado from supermercado,publicaOferta where publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado and supermercado.lixeira=0 group by idsupermercado"; 

                            // $resultadoEmpresa = mysqli_query($conexao,$SQLEmpresa);
                           
                            $query = $conexao->prepare($SQLEmpresa);
                            $result = $query->execute();
                            $resultadoEmpresa = $query->fetchAll();


                            // $pesquisarEmpresa = $operacaoEmpresa->execute();
                            // $resultadoEmpresa = $operacaoEmpresa->fetchAll();
                            foreach($resultadoEmpresa as $empresa){
                                $idsupermercado=$empresa['idsupermercado'];
                                $nomeSupermercado=$empresa['nomeSupermercado'];
                                echo "<option value='$idsupermercado'>$nomeSupermercado</option>";
                            }          
                        ?>                      
                    </select>
                    <?php
                    $conexao = null;
                    ?>
                </div>
            </div>
            <div class="col-md-6 col-xs-6">
                <div class="input-group-btn add-produto">
                    <button type="button" class="btn btn-danger button scan"  data-toggle="modal" data-target="#cadPromocao">
                        <i class="fas fa-barcode"></i> Cadastrar oferta
                    </button>
                </div>
            </div>
        </div>
        

        <spam id='loadingGif'>Carregando ofertas, aguarde...<img src='images/ajax-loader.gif' alt='' /></spam>

        <?php
        if(!isset($_GET['buscar'])){
            ?>
            <!--top 6 promocoes encerradas-->
            <div id='topProduto' class='row'>
                <?php
                    
                    $conexao = conn_mysql();    

                    try{
                        $SQLSelect = "
                            SELECT *,
                                if(precoOferta is not null,'1','0') as tipo,
                                date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                                nomeSupermercado,
                                idpublicaOferta,
                                imagemSupermercado
                            from 
                                publicaOferta 
                                inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                                inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                            where 
                                DATEDIFF(dataFinal,'$datahoje') >= 0
                               	and DATEDIFF(dataFinal,dataInicial) <= 3
                                and status= 1 
                                group by idpublicaOferta
                                $com_categoria
                                order by qtde_cliques desc
                                ;";   
                        
                        // $resultados = mysqli_query($conexao,$SQLSelect);

                        $query = $conexao->prepare($SQLSelect);
                        $result = $query->execute();
                        $resultados = $query->fetchAll();

                        // $operacao = $conexao->prepare($SQLSelect);
                        // $pesquisar = $operacao->execute(array($categoria));
                        // $resultados = $operacao->fetchAll();


                        if (!empty($resultados)){
                            ?>
                            <div class='col-lg-12'>
                                <h3 class='page-header'>Ofertas do dia (<?=date('d/m/Y')?>)</h3>
                            </div>
                            <?php
                            foreach($resultados as $promocoes){
                                if( strpos($promocoes['banner_promocao'], 'cdn-cosmos') > 0 ){
                                    $file_headers = @get_headers($promocoes['banner_promocao']);
                                    if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                       $promocoes['banner_promocao'] = 'images/promocoes/produto_defalt.png';
                                    }
                                }   
                               
                                ?>
                           	    <div class='col-md-4 col-xs-12'>
                                    <div class="portfolio-item todos">
                                        <img class='blackItem' src='<?=str_replace('../','',$promocoes['imagemSupermercado']);?>' alt=''>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <img onclick="loadModal('<?=$promocoes['idpublicaOferta']?>')" class='img-responsive img-portfolio img-hover' data-toggle="modal" data-target="#modalProdutos" src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt=''>
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="descricaoProduto">
                                                    <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                                    <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                                    <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div> 
                            <?php      
                            }
                        }
                    }
                    catch (PDOException $e)
                    {
                        // caso ocorra uma exceção, exibe na tela
                        echo "Erro!: " . $e->getMessage() . "<br>";
                        die();
                    }

                    $conexao = NULL;
                    
                ?>
            </div>
            <?php
        }
        ?>

        <div id='produtosOferta' class='row'>
            <?php
                


                if(isset($_GET['buscar'])){
                   $query=$_GET['buscar'];
                   $buscarTotal = "and desc_promocao like '%$query%'";
                   $buscarNome = "desc_promocao like '%$query%' and";
                }else{
                    $buscarNome = "";
                    $buscarTotal = "";
                }

                try{

                    $conexao = conn_mysql();  

                    $total = "SELECT sum(status) as totalPromoAtivas from  publicaOferta where status = 1 $buscarTotal
                            and dataFinal >= '$datahoje' 
                            and status= 1 
                            and dataInicial <= '$datahoje'
                            ";   
                    // $resultadoTotal = mysqli_query($conexao,$total);

                    $query = $conexao->prepare($total);
                    $result = $query->execute();
                    $resultadoTotal = $query->fetchAll();

                    // $operacaototal = $conexao->prepare($total);
                    // $pesquisarTotal = $operacaototal->execute();
                    // $resultadoTotal = $operacaototal->fetchAll();
                    if( !empty($resultadoTotal) ) {
                        foreach($resultadoTotal as $total){
                            $totalPromoAtivas=$total['totalPromoAtivas'];
                        }
                    }
                    $SQLSelect = "
                        SELECT *,
                            if(precoOferta is not null,'1','0') as tipo,
                            date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                            nomeSupermercado,
                            DATEDIFF('$datahoje', dataFinal) as datediff
                        from 
                            publicaOferta 
                            inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                            inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                        where 
                            $buscarNome
                            dataFinal >= '$datahoje' 
                            and status= 1 
                            and dataInicial <= '$datahoje'
                            $com_categoria
                            group by idpublicaOferta
                            order by qtde_cliques,precoOferta
                            ";   
                    // $resultados = mysqli_query($conexao,$SQLSelect);

                    $query = $conexao->prepare($SQLSelect);
                    $result = $query->execute();
                    $resultados = $query->fetchAll();

                    // $operacao = $conexao->prepare($SQLSelect);
                    // $pesquisar = $operacao->execute();
                    // $resultados = $operacao->fetchAll();

                     $SQLSelect = "
                        SELECT 
                            idlistaDeCompras,
                            id_publicaoferta 
                        FROM 
                            `listaDeCompras`
                            inner join rel_listaDeCompras_publicaOferta on id_listadecompras=idlistaDeCompras
                        WHERE 
                            usuario_idusuario=$idUser;";   

                    // $resultadoslista = mysqli_query($conexao,$SQLSelect);

                    $query = $conexao->prepare($SQLSelect);
                    $result = $query->execute();
                    $resultadoslista = $query->fetchAll();

                    $minhaListaDeCompras = array();
                    
                    if ( !empty($resultadoslista) ){  
                        foreach($resultadoslista as $listaCompras){
                            $minhaListaDeCompras[]=$listaCompras['id_publicaoferta'];
                        }
                    }
                    $promocoes_listadas = array();

                    if ( !empty($resultados) ){
                        ?>

                        <div class='col-lg-12'>
                              <h3 class='page-header'>Promoções <?=$categoria_escolhida['descricaoCategoria']?> (<?=$totalPromoAtivas?>) </h3>
                        </div>

                        <?php
                        
                        //if(!empty($_SESSION['auth'])){
                        foreach($resultados as $promocoes){
                            array_push($promocoes_listadas,$promocoes['idpublicaOferta']);
                            if (in_array($promocoes['idpublicaOferta'], $minhaListaDeCompras)) {
                                $classLista = "fa-star";
                            }else{
                                $classLista = "fa-star-o";
                            }
                            if( strpos($promocoes['banner_promocao'], 'cdn-cosmos') > 0 ){
                                $file_headers = @get_headers($promocoes['banner_promocao']);
                                if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                   $promocoes['banner_promocao'] = 'images/promocoes/produto_defalt.png';
                                }
                            } 
                            ?>
                            <div class='col-md-4 col-xs-12' >
                                <!--<span title="Adicionar aos meus favoritos" onclick="addtoCart('<?=$promocoes['idpublicaOferta']?>');" class='addProduto'><i id="item_<?=$promocoes['idpublicaOferta']?>" class="fa <?=$classLista?>"></i></span>-->
                                <div class="portfolio-item todos ">
                                    <?php
                                    if(isset($_GET['buscar'])){
                                    ?>
                                        <div class="fb-share-button" data-href="https://www.teoofertas.com.br/share.php?id=<?=$promocoes['idpublicaOferta']?>" data-layout="button" data-size="small" data-mobile-iframe="false" style="position: absolute; bottom: 10px;"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.teoofertas.com.br/"></a></div>
                                    <?php
                                    }
                                    ?>
                                    </span>

                                    <img class='blackItem' src='<?=str_replace('../','',$promocoes['imagemSupermercado']);?>' alt=''>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <span class='addProduto'> 
                                                
                                                <!--<img class='marcaDagua' src='images/BLACKPRICE80x80.png' alt=''>-->


                                                <img onclick="loadModal('<?=$promocoes['idpublicaOferta']?>')" class='img-responsive img-portfolio img-hover' data-toggle="modal" data-target="#modalProdutos" src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt='<?=$promocoes['desc_promocao']?>'>
                                            </span>
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="descricaoProduto">
                                                <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                                <?php
                                                    if(!empty($_SESSION['auth'])){ 
                                                        ?>
                                                        <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?>
                                                        </p>
                                                        <?php  
                                                    }
                                                    else{
                                                        ?>
                                                        <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                                        <?php  
                                                    }
                                                    ?>
                                                <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>
                                            </div>
                                        </div>
                                        <span class="tempo">
                                            <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>
                                            <?php 
                                            if($promocoes['datediff']==0){ 
                                                ?>
                                                <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'>ACABA HOJE!</div>
                                                <?php 
                                            }else{ 
                                                ?>
                                                <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'></div>
                                                <script type='text/javascript'>
                                                $('.contador_<?=$promocoes['idpublicaOferta']?>').countdown('<?=$promocoes['dataFinal']?>', function(event) {
                                                    $(this).html(event.strftime('%D dias %H:%M:%S'));
                                                });
                                                </script>        
                                                <?php 
                                            } 
                                            ?>
                                        </span>
                                    </div>
                                    

                                                
                                    </p>
                                </div>
                                
                            </div>
                            <?php      
                        }
                    }
                    else{
                        ?>
                        <div>
                            <p class="msg">Nenhuma oferta encontrada!</p>
                        </div>
                        <?php  
                    }
                }
                catch (PDOException $e)
                {
                    echo "Erro!: " . $e->getMessage() . "<br>";
                    die();
                }

                $conexao = NULL;
            ?>
        </div>

        <!--top 3 de promoções de outras categorias-->
        <?php


            /*if(isset($_GET['cat'])){?>
                <div id='topProduto' class='row'>
                    <div class='col-lg-12'>
                        <h3 class='page-header'>Promoções ativas de outras categorias</h3>
                    </div>
                    <?php
                        //if(!empty($_SESSION['auth'])){
                        $conexao = conn_mysql();    
                        try{
                            $promocoes_in = '';
                            $promocoes_in = implode($promocoes_listadas,',');
                            if(strlen($promocoes_in)>0)
                                $promocoes_in = "and idpublicaOferta not in($promocoes_in)";
                        
                            $SQLSelect = "
                                SELECT *,
                                    if(precoOferta is not null,'1','0') as tipo,
                                    date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                                    nomeSupermercado
                                from 
                                    publicaOferta 
                                    inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                                    inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                                where 
                                    dataFinal > '$datahoje' 
                                    and status= 1 
                                    and dataInicial <= '$datahoje'
                                    $promocoes_in
                                    group by idpublicaOferta
                                    limit 3;";   

                            $operacao = $conexao->prepare($SQLSelect);
                            $pesquisar = $operacao->execute();
                            $resultados = $operacao->fetchAll();
                            
                            if (count($resultados)>0){
                                foreach($resultados as $promocoes){
                                    if (in_array($promocoes['idpublicaOferta'], $minhaListaDeCompras)) {
                                        $classLista = "fa-star";
                                    }else
                                        $classLista = "fa-star-o";
                                ?>
                                    <div class='portfolio-item col-md-4 col-xs-6 $hidden' >
                                        <span title="Adicionar aos meus favoritos" onclick="addtoCart('<?=$promocoes['idpublicaOferta']?>');" class='addProduto'><i id="item_<?=$promocoes['idpublicaOferta']?>" class="fa <?=$classLista?>"></i></span>
                                        <img class='blackItem' src='images/BLACKPRICE80x80.png' alt=''>
                                        <a href="share.php?cat=<?=$promocoes['id_categoria']?>&id=<?=$promocoes['idpublicaOferta']?>" style="outline:none;">
                                            <img class='img-responsive img-portfolio img-hover' src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt=''>
                                            <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                            <?php
                                                if(!empty($_SESSION['auth'])){?>
                                                    <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                            <?  }
                                                else{?>
                                                    <p class='productPrice'>Faça seu login.</p>
                                            <?  }?>
                                            <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>

                                            <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>

                                            <p>
                                                <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'></div>
                                                <script type='text/javascript'>
                                                $('.contador_<?=$promocoes['idpublicaOferta']?>').countdown('<?=$promocoes['dataFinal']?>', function(event) {
                                                    $(this).html(event.strftime('%D dias %H:%M:%S'));
                                                });
                                                </script>                    
                                            </p>

                                        </a>
                                    </div>
                        <?      
                                }
                            }
                            else{?>
                                <div>
                                    <p class="msg">No momento, não há promoções ativas para a categoria escolhida.</p>
                                </div>
                        <?  }
                        }
                        catch (PDOException $e)
                        {
                            // caso ocorra uma exceção, exibe na tela
                            echo "Erro!: " . $e->getMessage() . "<br>";
                            die();
                        }

                        $conexao = NULL;*/
                        /*}
                        else{?>
                            <div>
                                <p class="msg">É necessário efetuar o login para visualizar as promoções.</p>
                            </div>
                    <?  }*/
                   /* ?>
                </div>
        <? }*/?>

            <div>
                <p class="msg">Atenção! O Teó Ofertas foi desenvolvido para ajudar você consumidor a encontrar mais facilmente as orfertas disponíveis nos supermercados de Teófilo Otoni e região. As ofertas possuem data de expiração ou até durar o estoque, as imagens contidas no sistema são meramente ilustrativas. Em caso de dúvidas entre em contato conosco: siamelsoftwares@gmail.com</p>
                <p><a href="http://www.siamelsoftwares.com.br/contato/"><img src="images/logo.png" alt="SiamelSoftware"></a></p>
            </div>

        <hr>
        <button class="btn btn btn-secondary" id="scrollTop"><i class="fas fa-angle-up"></i></button>
    </div>

    <div class="modal fade" id="modalProdutos" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div id="bodyModal" class="modal-body">
              <p>Carregando... <img src='images/ajax-loader.gif' alt='' /></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
            </div>
          </div>
          
        </div>
    </div>
<div class="modal fade" id="cadPromocao" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cadastro de Promoção</h4>
            </div>
            <div id="bodyModal" class="modal-body">
                   
                <div id="cadPromocao-form">
                    <form class="navbar-form" role="cadPromocao" method="POST" id="cadPromocao-form" name="cadPromocao-form" action="cadPromocao.php">
                        <div class="row">
                            <!-- <div class="col-md-12">
                                <div class="button-codBarra">
                                    <button type="button" class="btn btn-info button scan">   
                                        Ler código de barras
                                    </button> 
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <label for="from">Tipo de identificação</label>
                                <div class="custom-control custom-radio" style="margin-bottom: 20px;">    
                                    <input type="radio" id="tipoEntrada1" name="tipoEntrada" class="custom-control-input" style="width: auto;"> 
                                    <label class="custom-control-label" for="customRadio1"  style="margin-right: 20px;">Nome do produto</label>
                                    <input type="radio" id="tipoEntrada2" name="tipoEntrada" class="custom-control-input button scan" style="width: auto;" checked="true">
                                    <label class="custom-control-label" for="customRadio2">Código de barras</label>
                                </div>

                            </div>
                            <div class="col-md-12 cod-barra">
                                <label for="from">Codigo de Barras</label>
                                <input autocomplete="off" type="text" class="form-control cod_barra" placeholder="Codigo de Barras" name="cod_barra" id="cod_barra">
                            </div>
                            <div class="col-md-12 nome-produto">
                                <label for="from">Nome do Produto</label>
                                <input autocomplete="off" type="text" class="form-control cod_barra" placeholder="Nome do Produto" name="descricaoProduto" id="nome_produto">
                            </div>
                            <div class="col-md-12">
                                <label for="from">Supermecado</label>
                                <select class="form-control" name="supermercado"> 
                                    <option value="">Selecione o supermercado</option>
                                    <?php

                                        $conexao = conn_mysql();  
                                        $SQLEmpresa = "SELECT idsupermercado,nomeSupermercado from supermercado,publicaOferta where publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado and supermercado.lixeira=0 group by idsupermercado"; 
                                        // $resultadoEmpresa = mysqli_query($conexao,$SQLEmpresa);

                                        $query = $conexao->prepare($SQLEmpresa);
                                        $result = $query->execute();
                                        $resultadoEmpresa = $query->fetchAll();

                                        foreach($resultadoEmpresa as $empresa){
                                            $idsupermercado=$empresa['idsupermercado'];
                                            $nomeSupermercado=$empresa['nomeSupermercado'];
                                            echo "<option value='$idsupermercado'>$nomeSupermercado</option>";
                                        }          
                                    ?>                      
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="from">Data de Término</label>
                                <input type="date" class="form-control" name="fim" id="to" placeholder="Data de Término" required>
                            </div>
                            <div class="col-md-12">
                                <label for="endereco">Preço</label>
                                <input type="number" class="form-control" name="preco" id="preco" placeholder="Preço" onKeyPress="return(MascaraMoeda(this,'','.',event))">
                            </div>
                            <br>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success" name="signup">Cadastrar</button> 
                            </div>
                        </div>
                    </form>
                </div>                                    


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
            </div>
      </div>
      
    </div>
</div>
    <?php
    $conexao = null;

    $conexao = conn_mysql();    

    $SQLSelect = "select descricaoProduto from produto where ativo=1";

    // $categoria_escolhida = mysqli_query($conexao,$SQLSelect);

    $query = $conexao->prepare($SQLSelect);
    $result = $query->execute();
    $arrayHortfruti = $query->fetchAll();

    

    $array_tratado = array();

    foreach ($arrayHortfruti as $key => $produto) {
        $array_tratado[$key] = $produto['descricaoProduto'];
    }


    function js_str($s) {
        return '"' . addcslashes($s, "\0..\37\"\\") . '"';
    }
    function js_array($array) {
        $temp = array_map('js_str', $array);
        return '[' . implode(',', $temp) . ']';
    }

    $js_arrayhortfruti = js_array($array_tratado);

    $conexao = null;
    ?>
    </div>
    <!-- /.container -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>    
    <script src="js/prism.js" type="text/javascript"></script>
    <script src="js/quagga.js" type="text/javascript"></script>
    <script src="js/jquery.maskMoney.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Script to Activate the Carousel -->
    <script>

        var hortfruti = <?php echo $js_arrayhortfruti; ?>;
 
        var accentMap = {
            "á": "a",
            "à": "a",
            "ã": "a",
            "â": "a",
            "é": "e",
            "é": "e",
            "è": "e",
            "ê": "e",
            "ö": "o",
            "ó": "o",
            "ò": "o",
            "ô": "o",
            "ú": "u",
            "ù": "u",
            "ú": "u",
            "ç": "c"
        };
        var normalize = function( term ) {
            var ret = "";
            for ( var i = 0; i < term.length; i++ ) {
                ret += accentMap[ term.charAt(i) ] || term.charAt(i);
            }
            return ret;
        };
         
        $( "#nome_produto" ).autocomplete({
            source: function( request, response ) {
                var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
                response( $.grep( hortfruti, function( value ) {
                    value = value.label || value.value || value;
                    return matcher.test( value ) || matcher.test( normalize( value ) );
                }) );
              },
            minLength: 1
        });

        $(document).ready(function()
        {
            if ( $('#tipoEntrada2').is(":checked") ) {
                $(".nome-produto").css({display:"none"});
                $(".cod-barra").css({display:"block"});
                $(".nome_produto").val("");
            }
            if ( $('#tipoEntrada1').is(":checked")  ) {
                $(".cod-barra").css({display:"none"});
                $(".nome-produto").css({display:"block"});
                $(".cod_barra").val("");
            }

            $('#tipoEntrada2').on('click',function(){
                $(".nome-produto").css({display:"none"});
                $(".cod-barra").css({display:"block"});
                var windowWidth = $(window).width();

                if(windowWidth < 767) {
                    App.activateScanner();
                }
                
                $(".nome_produto").val("");
            });
            $('#tipoEntrada1').on('click',function(){
                $(".cod-barra").css({display:"none"});
                $(".nome-produto").css({display:"block"});
                $(".cod_barra").val("");
            });

             $("#preco").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

              $(window).scroll(function(){ 
                   if ($(this).scrollTop() > 100) { 
                       $('#scrollTop').fadeIn(); 
                   } else { 
                       $('#scrollTop').fadeOut(); 
                   } 
               }); 
               $('#scrollTop').click(function(){ 
                   $("html, body").animate({ scrollTop: 0 }, 600); 
                   return false; 
               });
        });
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })

        $( "#sup" ).change(function() {
          var sup = $( "#sup" ).val();

          $('#loadingGif').fadeIn();
          $('#topProduto').fadeOut();
          //$('#produtosOferta').load('loadOfertas.php?sup='+sup);

            $( "#produtosOferta" ).load('loadOfertas.php?sup='+sup, function() {
              $('#loadingGif').fadeOut();
            });
        });

        function loadModal(id){
            $("#bodyModal").html('');
            $("#bodyModal").load('verProdutos.php?id='+id);
        }

        var Quagga = window.Quagga;

        var backCamID;
       
        navigator.mediaDevices.enumerateDevices()
        .then(function(devices) {
            devices.forEach(function(device) {
                
                if( device.kind == "videoinput" ){
                    
                    backCamID = device.deviceId || device.id;
                }
            });
           
        })
        .catch(function(err) {
          console.log(err.name + ": " + err.message);
        });

        

        if(typeof(backCamID)=="undefined"){
          console.log("back camera not found.");
        }
var App = {
    _scanner: null,
    init: function() {
        this.attachListeners();
    },
    activateScanner: function() {
        var scanner = this.configureScanner('.overlay__content'),
            onDetected = function (result) {
                document.querySelector('input.cod_barra').value = result.codeResult.code;
                stop();
            }.bind(this),
            stop = function() {
                scanner.stop();  // should also clear all event-listeners?
                scanner.removeEventListener('detected', onDetected);
                this.hideOverlay();
                this.attachListeners();
            }.bind(this);

        this.showOverlay(stop);
        scanner.addEventListener('detected', onDetected).start();
    },
    attachListeners: function() {
        var self = this,
            button = document.querySelector('.button.scan');

        button.addEventListener("click", function onClick(e) {
            e.preventDefault();
            button.removeEventListener("click", onClick);
            self.activateScanner();
        });
    },
    showOverlay: function(cancelCb) {
        if (!this._overlay) {
            var content = document.createElement('div'),
                closeButton = document.createElement('div');

            closeButton.appendChild(document.createTextNode('X'));
            content.className = 'overlay__content';
            closeButton.className = 'overlay__close';
            this._overlay = document.createElement('div');
            this._overlay.className = 'overlay';
            this._overlay.appendChild(content);
            content.appendChild(closeButton);
            closeButton.addEventListener('click', function closeClick() {
                closeButton.removeEventListener('click', closeClick);
                cancelCb();
            });
            document.body.appendChild(this._overlay);
        } else {
            var closeButton = document.querySelector('.overlay__close');
            closeButton.addEventListener('click', function closeClick() {
                closeButton.removeEventListener('click', closeClick);
                cancelCb();
            });
        }
        this._overlay.style.display = "block";
    },
    hideOverlay: function() {
        if (this._overlay) {
            this._overlay.style.display = "none";
        }
    },
    configureScanner: function(selector) {
        
        if (!this._scanner) {
            this._scanner = Quagga
                .decoder({readers: ['ean_reader']})
                .locator({patchSize: 'medium',halfSample: true})
                .fromSource({
                    capture: true,
                    capacity: 20,
                    target: selector,
                    constraints: {
                        width: 640,
                        height: 480,
                        facingMode: "environment",
                        deviceId: backCamID
                        // aspectRatio: {min: 1, max: 2}
                    },
                    area: { // defines rectangle of the detection/localization area
                        top: "30%",    // top offset
                        right: "0%",  // right offset
                        left: "0%",   // left offset
                        bottom: "30%"  // bottom offset
                    },
                    locate: false,
                    singleChannel: true
                });
        }
        return this._scanner;
    }
};

    $(document).ready(function(){
        var windowWidth = $(window).width();

        if(windowWidth < 767) {
            App.init();
        }

    });



    </script>
    
</div>

</body>

</html>
