<?php
    session_start();

    include('conf/confbd.php');

    if( !isset($_SESSION['idUser']) ){
        $idUser = '';
    } else {
        $idUser = $_SESSION['idUser'];
    }
    
    if( empty($categoria_escolhida) ){
        $categoria_escolhida = array(
            'idcategoria' => 25,
            'descricaoCategoria' => 'Supermercados',
            'icone' => 'fa-shopping-basket'
        );
    }

    $datahoje=date('Y-m-d');

    $sup = $_GET['sup'];

    if( !isset($_GET['produto']) ){
        $oferta= '';
    } else {
        $oferta=$_GET['produto'];
    }
    

    if(!empty($_GET['sup'])){
       $supTotap = "and supermercado_idsupermercado=$sup";
       $idsup = "idsupermercado=$sup and";
    }elseif(isset($_GET['produto'])){
       $supTotap = "and desc_promocao like '%$oferta%'";
       $idsup = "desc_promocao like '%$oferta%' and";
    }else{
         unset($_GET['sup']);
         unset($_GET['produto']);
         $idsup="";
         $supTotap ="";      
    }

    $conexao = conn_mysql();  
?>
        
    <?php

        try{

            $total = "SELECT sum(status) as totalPromoAtivas from publicaOferta where status = 1 $supTotap";

            // $resultadoTotal = mysqli_query($conexao,$total);

            $operacaototal = $conexao->prepare($total);
            $pesquisarTotal = $operacaototal->execute();
            $resultadoTotal = $operacaototal->fetchAll();
                foreach($resultadoTotal as $total){
                    $totalPromoAtivas=$total['totalPromoAtivas'];
                }

                

            $SQLSelect = "
                SELECT *,
                    if(precoOferta is not null,'1','0') as tipo,
                    date_format(dataFinal,'%Y/%m/%d') as dataTermino,
                    nomeSupermercado,
                    DATEDIFF('$datahoje', dataFinal) as datediff
                from 
                    publicaOferta 
                    inner join supermercado on publicaOferta.supermercado_idsupermercado=supermercado.idsupermercado
                    inner join rel_supermercado_categoria on id_supermercado=supermercado.idsupermercado
                where 
                    $idsup
                    dataFinal >= '$datahoje' 
                    and status= 1 
                    and dataInicial <= '$datahoje'
                    group by idpublicaOferta
                    order by qtde_cliques,precoOferta
                    ";  
            // $resultados = mysqli_query($conexao,$SQLSelect); 

            $operacao = $conexao->prepare($SQLSelect);
            $pesquisar = $operacao->execute();
            $resultados = $operacao->fetchAll();

             $SQLLista = "
                SELECT 
                    idlistaDeCompras,
                    id_publicaoferta 
                FROM 
                    `listaDeCompras`
                    inner join rel_listaDeCompras_publicaOferta on id_listadecompras=idlistaDeCompras
                WHERE 
                    usuario_idusuario=$idUser;";   

            // $resultadoslista = mysqli_query($conexao,$SQLLista); 

            $operacaoLista = $conexao->prepare($SQLLista);      
            $pesquisarLista = $operacaoLista->execute();
            $resultadoslista = $operacaoLista->fetchAll();

            $minhaListaDeCompras = array();
            
            if ( !empty($resultadoslista) ){  
                foreach($resultadoslista as $listaCompras){
                    $minhaListaDeCompras[]=$listaCompras['id_publicaoferta'];
                }
            }
            $promocoes_listadas = array();

            if ( !empty($resultados) ){ 
                ?>

                <div class='col-lg-12'>
                    <h3 class='page-header'>Promoções <?=$categoria_escolhida['descricaoCategoria']?> (<?=$totalPromoAtivas?>) </h3>
                </div>

                <?php
                

                //if(!empty($_SESSION['auth'])){
                foreach($resultados as $promocoes){

                    array_push($promocoes_listadas,$promocoes['idpublicaOferta']);
                    if (in_array($promocoes['idpublicaOferta'], $minhaListaDeCompras)) {
                        $classLista = "fa-star";
                    }else{
                        $classLista = "fa-star-o";
                    }
                    if( strpos($promocoes['banner_promocao'], 'cdn-cosmos') > 0 ){
                        $file_headers = @get_headers($promocoes['banner_promocao']);
                        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                           $promocoes['banner_promocao'] = 'images/promocoes/produto_defalt.png';
                        }
                    } 
                    ?>
                    <div class='col-md-4 col-xs-12'>
                        <div class="portfolio-item todos">
                            <!-- <span title="Adicionar aos meus favoritos" onclick="addtoCart('<?=$promocoes['idpublicaOferta']?>');" class='addProduto'><i id="item_<?=$promocoes['idpublicaOferta']?>" class="fa <?=$classLista?>"></i></span> -->
                            <img class='blackItem' src='<?=str_replace('../','',$promocoes['imagemSupermercado']);?>' alt=''>
                            <div class="row">
                                <div class="col-xs-4">
                                    <!--<img class='marcaDagua' src='images/BLACKPRICE80x80.png' alt=''>-->
                                    <img onclick="loadModal('<?=$promocoes['idpublicaOferta']?>')" class='img-responsive img-portfolio img-hover' data-toggle="modal" data-target="#modalProdutos" src='<?=str_replace('../','',$promocoes['banner_promocao'])?>' alt='<?=$promocoes['desc_promocao']?>'>
                                </div>
                                <div class="col-xs-8">
                                    <div class="descricaoProduto">
                                        <span class='productDescription'><?=$promocoes['desc_promocao']?></span>
                                        <?php
                                            if(!empty($_SESSION['auth'])){
                                                ?>
                                                    <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                                    <?php  
                                            } else {
                                                ?>
                                                <p class='productPrice'>R$ <?=number_format($promocoes['precoOferta'],2,",",".")?></p>
                                            <?php  
                                            }
                                        ?>
                                        <p class='productSupermercado'><?=$promocoes['nomeSupermercado']?></p>
                                    </div>
                                    
                                </div>
                            </div>
                            <span class="tempo">
                                <p class='tempoRestante'><i class='fa fa-clock-o'></i> Tempo restante  </p>
                                <?php 
                                if($promocoes['datediff']==0){ 
                                    ?>
                                    <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'>ACABA HOJE!</div>
                                    <?php 
                                }else{ 
                                    ?>
                                    <div id='contador' class='contador_<?=$promocoes['idpublicaOferta']?>'></div>
                                    <script type='text/javascript'>
                                    $('.contador_<?=$promocoes['idpublicaOferta']?>').countdown('<?=$promocoes['dataFinal']?>', function(event) {
                                        $(this).html(event.strftime('%D dias %H:%M:%S'));
                                    });
                                    </script>        
                                    <?php 
                                } 
                                ?>            
                            </span>
                        </div>                       
                            
                    </div>
                <?php    
               
                }
            } else {
                ?>
                <div>
                    <p class="msg">Nenhuma oferta encontrada!</p>
                </div>
                <?php  
            }
        } catch (PDOException $e) {
            $conexao = null;
            echo "Erro!: " . $e->getMessage() . "<br>";
            die();
        }

        $conexao = NULL;
    ?>

