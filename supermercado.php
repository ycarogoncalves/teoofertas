<?php

session_start();

include('conf/confbd.php');


include('authSession.php');

            
?>
<!DOCTYPE html>
<html lang="pt-br">

<?php



            $idSup=$_GET['id'];

           // instancia objeto PDO, conectando no mysql
            // $conexao = conn_mysql();    

            try{

            $SQLSelect="SELECT * from supermercado where idsupermercado=$idSup;";

            $resultados = mysqli_query($conexao,$SQLSelect);

            // //prepara a execução da sentença
            // $operacao = $conexao->prepare($SQLSelect);      
                    
            // $pesquisar = $operacao->execute();
            
            // //captura TODOS os resultados obtidos
            // $resultados = $operacao->fetchAll();

            // se há resultados, os escreve em uma tabela
            if ( !empty($resultados) ){  
                foreach($resultados as $dadosEncontrados){
                    $idsupermercado = $dadosEncontrados['idsupermercado'];
                    $nomeSupermercado = $dadosEncontrados['nomeSupermercado'];
                    $imagemSupermercado = $dadosEncontrados['imagemSupermercado'];
                    $lat = $dadosEncontrados['lat'];
                    $lgt = $dadosEncontrados['lgt'];
                    $telefone = $dadosEncontrados['telefone'];
                    $endereco = $dadosEncontrados['endereco'];
                    $horarioFuncionamento = $dadosEncontrados['horarioFuncionamento'];         
                }

            }
            else{
                echo'<div class="starter-template">';
                echo'</div>';
            }
        } //try
        catch (PDOException $e)
        {
        // caso ocorra uma exceção, exibe na tela
        echo "Erro!: " . $e->getMessage() . "<br>";
        die();
        }

        $conexao = NULL;
?>

<?php include('head.php'); ?>

<body>
    <?php include('navbar.php'); ?>
    
    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo $nomeSupermercado; ?>
                    <small></small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Página Inicial</a>
                    </li>
                    <li class="active">Supermercado</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Map Column -->
            <div class="col-md-8">
                <!-- Embedded Google Map -->
                <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?hl=pt-br&amp;ie=UTF8&amp;q=<?php echo $lat; ?>,<?php echo $lgt; ?>&amp;q=<?php echo $endereco; ?>&amp;t=m&amp;z=18&amp;output=embed"></iframe>
            </div>
            <!-- Contact Details Column -->
            <div class="col-md-4">
                <h3><?php echo $nomeSupermercado; ?></h3>
                <p>
                    <?php echo $endereco; ?><br>
                </p>
                <p><i class="fa fa-phone"></i> 
                    <abbr title="Phone">Telefone</abbr>:  <?php echo $telefone; ?></p>
                <p><i class="fa fa-clock-o"></i> 
                    <abbr title="Hours">Aberto</abbr>: <?php echo $horarioFuncionamento; ?></p>
            </div>
        </div>
        <!-- /.row -->

        <hr>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <?php include('footer.php'); ?> 


</body>

</html>
